package org.jhipster.blog.service.impl;

import org.jhipster.blog.service.InternshipService;
import org.jhipster.blog.domain.Company;
import org.jhipster.blog.domain.Internship;
import org.jhipster.blog.domain.Student;
import org.jhipster.blog.repository.CompanyRepository;
import org.jhipster.blog.repository.InternshipRepository;
import org.jhipster.blog.repository.StudentRepository;
import org.jhipster.blog.repository.UserRepository;
import org.jhipster.blog.security.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Internship}.
 */
@Service
@Transactional
public class InternshipServiceImpl implements InternshipService {

    private final Logger log = LoggerFactory.getLogger(InternshipServiceImpl.class);

    private final InternshipRepository internshipRepository;
    private final UserRepository userRepository;
    private final CompanyRepository companyRepository;
    private final StudentRepository studentRepository;
    private Company company;
    private Student student;
    public InternshipServiceImpl(InternshipRepository internshipRepository,UserRepository userRepository,CompanyRepository companyRepository,StudentRepository studentRepository) {
        this.internshipRepository = internshipRepository;
        this.companyRepository= companyRepository ;
        this.userRepository = userRepository;
        this.studentRepository = studentRepository;
    }

    /**
     * Save a internship.
     *
     * @param internship the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Internship save(Internship internship) {
        SecurityUtils.getCurrentUserLogin()
		.flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			company = companyRepository.findCompanyByUser(user);
			internship.setCompany(company);
			 log.debug("Request to save Internship : {}", internship);
		});
        return internshipRepository.save(internship);
    }

    /**
     * Get all the internships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Internship> findAll(Pageable pageable) {
        log.debug("Request to get all Internships");
        return internshipRepository.findAll(pageable);
    }

    /**
     * Get one internship by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Internship> findOne(Long id) {
        log.debug("Request to get Internship : {}", id);
        return internshipRepository.findById(id);
    }

    /**
     * Delete the internship by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Internship : {}", id);
        internshipRepository.deleteById(id);
    }

	@Override
	public Page<Internship> findByCompanyLogin(Pageable pageable) {

		SecurityUtils.getCurrentUserLogin()
		.flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			company = companyRepository.findCompanyByUser(user);
			log.debug("Request to get all Internships by company : {}", company);
		});
        return internshipRepository.findByCompany(company,pageable);
	}

	@Override
	public Long findNumberInternshipsByLoginStudentDomain() {
		SecurityUtils.getCurrentUserLogin()
		.flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			student = studentRepository.findStudentByUser(user);
			log.debug("Request to get number of Internships by StudentDomain");
		});
        return internshipRepository.findNumberInternshipsByLoginStudentDomain(student.getDepartment().getName());

	}

	@Override
	public Page<Internship> findByLoginStudentDomain(Pageable pageable) {
		SecurityUtils.getCurrentUserLogin()
		.flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			student = studentRepository.findStudentByUser(user);
			log.debug("Request to get all Internships by StudentDomain");
		});
        return internshipRepository.findByLoginStudentDomain(student.getDepartment().getName(),pageable);


	}

    @Override
    public Long findNumberInternshipsByLoginCompany() {
        SecurityUtils.getCurrentUserLogin()
		.flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			company = companyRepository.findCompanyByUser(user);
			log.debug("Request to get all Internships by company : {}", company);
        });
        return internshipRepository.findNumberOfInternshipByLoginCompany(company.getId());
    }





}
