package org.jhipster.blog.repository;

import java.util.List;
import java.util.Optional;

import org.jhipster.blog.domain.Company;
import org.jhipster.blog.domain.Interview;
import org.jhipster.blog.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Interview entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InterviewRepository extends JpaRepository<Interview, Long> {
    Page<Interview> findByCompany(Company company,Pageable pageable);
}
