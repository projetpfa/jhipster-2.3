package org.jhipster.blog.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;

/**
 * A Affectation.
 */
@Entity
@Table(name = "affectation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Affectation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "date_affectation", nullable = false)
    private Instant dateAffectation;

    @NotNull
    @Column(name = "start_date", nullable = false)
    private Instant startDate;

    @NotNull
    @Column(name = "end_date", nullable = false)
    private Instant endDate;

    @OneToOne
    @JoinColumn(unique = true)
    private StudentEvaluation studentEvaluation;

    @OneToOne
    @JoinColumn(unique = true)
    private CompanyEvaluation companyEvaluation;

    @ManyToOne
    @JsonIgnoreProperties("affectations")
    private Student student;

    @ManyToOne
    @JsonIgnoreProperties("affectations")
    private Internship internship;

    

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDateAffectation() {
        return dateAffectation;
    }

    public Affectation dateAffectation(Instant dateAffectation) {
        this.dateAffectation = dateAffectation;
        return this;
    }

    public void setDateAffectation(Instant dateAffectation) {
        this.dateAffectation = dateAffectation;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public Affectation startDate(Instant startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public Affectation endDate(Instant endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public StudentEvaluation getStudentEvaluation() {
        return studentEvaluation;
    }

    public Affectation studentEvaluation(StudentEvaluation studentEvaluation) {
        this.studentEvaluation = studentEvaluation;
        return this;
    }

    public void setStudentEvaluation(StudentEvaluation studentEvaluation) {
        this.studentEvaluation = studentEvaluation;
    }

    public CompanyEvaluation getCompanyEvaluation() {
        return companyEvaluation;
    }

    public Affectation companyEvaluation(CompanyEvaluation companyEvaluation) {
        this.companyEvaluation = companyEvaluation;
        return this;
    }

    public void setCompanyEvaluation(CompanyEvaluation companyEvaluation) {
        this.companyEvaluation = companyEvaluation;
    }

    public Student getStudent() {
        return student;
    }

    public Affectation student(Student student) {
        this.student = student;
        return this;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Internship getInternship() {
        return internship;
    }

    public Affectation internship(Internship internship) {
        this.internship = internship;
        return this;
    }

    public void setInternship(Internship internship) {
        this.internship = internship;
    }

  

    
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Affectation)) {
            return false;
        }
        return id != null && id.equals(((Affectation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Affectation{" +
            "id=" + getId() +
            ", dateAffectation='" + getDateAffectation() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            "}";
    }
}
