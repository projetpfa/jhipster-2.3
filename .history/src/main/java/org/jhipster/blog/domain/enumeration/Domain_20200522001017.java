package org.jhipster.blog.domain.enumeration;

/**
 * The Domain enumeration.
 */
public enum Domain {
    ELECTRONIC, BIOLOGY, COMPUTERSCIENCE, CIVIL, ELECTROMECHANIC, MATERIAL, GEOLOGY
}
