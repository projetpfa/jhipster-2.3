import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IViews, Views } from 'app/shared/model/views.model';
import { ViewsService } from './views.service';
import { ViewsComponent } from './views.component';
import { ViewsDetailComponent } from './views-detail.component';
import { ViewsUpdateComponent } from './views-update.component';
import { DetailsInternshipComponent } from './details-internship/details-internship.component';
import { ApplicationInternshipComponent } from './application-internship/application-internship.component';
import { CreateInternshipComponent } from './create-internship/create-internship.component';
import { InternshipResolve } from '../internship/internship.route';
import { ListCondidateComponent } from './list-condidate/list-condidate.component';
import { ListInternshipCompanyComponent } from './list-internship-company/list-internship-company.component';
import { ProfilesocietyComponent } from './profilesociety/profilesociety.component';
import { CompanyResolve } from '../company/company.route';
import { StudInternshipComponent } from './stud-internship/stud-internship.component';
import { StudentProfileComponent } from './student-profile/student-profile.component';
import { StudentResolve } from '../student/student.route';
import { HomeStudentComponent } from './home-student/home-student.component';
import { HomeCompanyComponent } from './home-company/home-company.component';
import { ListInvitationInterviewComponent } from './list-invitation-interview/list-invitation-interview.component';
import { InvitationInterviewResolve } from '../invitation-interview/invitation-interview.route';
import { ListApplicationComponent } from './list-application/list-application.component';
import { ListInterviewsComponent } from './list-interviews/list-interviews.component';
import { ListInternsComponent } from './list-interns/list-interns.component';
import { StudInterviewComponent } from './stud-interview/stud-interview.component';

@Injectable({ providedIn: 'root' })
export class ViewsResolve implements Resolve<IViews> {
  constructor(private service: ViewsService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IViews> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((views: HttpResponse<Views>) => {
          if (views.body) {
            return of(views.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Views());
  }
}

export const viewsRoute: Routes = [
  {
    path: '',
    component: ViewsComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'enisIntershipApp.views.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'stud-internship',
    component: StudInternshipComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'enisIntershipApp.views.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'listeOffreseditprofile',
    component: ListInternshipCompanyComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'enisIntershipApp.views.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ViewsDetailComponent,
    resolve: {
      views: ViewsResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisStagePlatformeApp.views.home.title'
    },
    canActivate: [UserRouteAccessService]
  },

  {
    path: 'newInternship',
    component: CreateInternshipComponent,
    resolve: {
      internship: InternshipResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.views.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/editerOffre',
    component: CreateInternshipComponent,
    resolve: {
      internship: InternshipResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.views.home.title'
    },
    canActivate: [UserRouteAccessService]
  },

  /*
 {
    path: 'listeCondidatures',
    component: ListCondidateComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'enisIntershipApp.views.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  */
  {
    path: ':id/listeCondidatures',
    component: ListCondidateComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams,
      internship: InternshipResolve
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'enisIntershipApp.application.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'profilesociety',
    component: ProfilesocietyComponent,
    resolve: {
      company: CompanyResolve
    },
    data: {
      authorities: [Authority.USER],

      pageTitle: 'enisIntershipApp.views.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/profilesociety',
    component: ProfilesocietyComponent,
    resolve: {
      company: CompanyResolve
    },
    data: {
      authorities: [Authority.USER],

      pageTitle: 'enisIntershipApp.views.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ViewsDetailComponent,
    resolve: {
      views: ViewsResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.views.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ViewsUpdateComponent,
    resolve: {
      views: ViewsResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.views.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ViewsUpdateComponent,
    resolve: {
      views: ViewsResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.views.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'listeOffres',
    component: ListInternshipCompanyComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'enisIntershipApp.views.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/viewInternship',
    component: DetailsInternshipComponent,
    resolve: {
      internship: InternshipResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.internship.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/newApplication',
    component: ApplicationInternshipComponent,
    resolve: {
      internship: InternshipResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.application.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/student-profile',
    component: StudentProfileComponent,
    resolve: {
      student: StudentResolve,
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.internship.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'home-student',
    component: HomeStudentComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'enisIntershipApp.views.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'home-company',
    component: HomeCompanyComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'enisIntershipApp.views.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'listInvitationInterview',
    component: ListInvitationInterviewComponent,
    resolve: {
      invitationInterview: InvitationInterviewResolve,
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.application.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'listApplication',
    component: ListApplicationComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.application.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'list-confirmation-interview',
    component: ListInterviewsComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.invitationInterview.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/listeInterns',
    component: ListInternsComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams,
      internship: InternshipResolve
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'enisIntershipApp.application.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'stud-interview',
    component: StudInterviewComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'enisIntershipApp.interview.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
];
