import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EnisIntershipSharedModule } from 'app/shared/shared.module';
import { ViewsComponent } from './views.component';
import { ViewsDetailComponent } from './views-detail.component';
import { ViewsUpdateComponent } from './views-update.component';
import { ViewsDeleteDialogComponent } from './views-delete-dialog.component';
import { viewsRoute } from './views.route';
import { StudInternshipComponent } from './stud-internship/stud-internship.component';
import { ListInternshipCompanyComponent } from './list-internship-company/list-internship-company.component';
import { DetailsInternshipComponent } from './details-internship/details-internship.component';
import { ApplicationInternshipComponent } from './application-internship/application-internship.component';
import { CreateInternshipComponent } from './create-internship/create-internship.component';
import { ListCondidateComponent } from './list-condidate/list-condidate.component';
import { ProfilesocietyComponent } from './profilesociety/profilesociety.component';
import { StudentProfileComponent } from './student-profile/student-profile.component';
import { HomeStudentComponent } from './home-student/home-student.component';
import { HomeCompanyComponent } from './home-company/home-company.component';
import { EvaluationsComponent } from './evaluations/evaluations.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { ListInvitationInterviewComponent } from './list-invitation-interview/list-invitation-interview.component';
import { InvitationInterviewConfirmationComponent } from './invitation-interview-confirmation/invitation-interview-confirmation.component';
import { InvitationInterviewCancellationComponent } from './invitation-interview-cancellation/invitation-interview-cancellation.component';
import { LoginCompanyComponent } from './login-company/login-company.component';
import { InvitationDialogComponent } from './invitation-dialog/invitation-dialog.component';
import { ListApplicationComponent } from './list-application/list-application.component';
import { ListInterviewsComponent } from './list-interviews/list-interviews.component';
import { ListInternsComponent } from './list-interns/list-interns.component';
import { ListNumberApplicationPerInternshipComponent } from './list-number-application-per-internship/list-number-application-per-internship.component';
import { StudInterviewComponent } from './stud-interview/stud-interview.component';

@NgModule({
  imports: [EnisIntershipSharedModule, RouterModule.forChild(viewsRoute)],
  declarations: [
    ViewsComponent,
    ViewsDetailComponent,
    ViewsUpdateComponent,
    ViewsDeleteDialogComponent,
    StudInternshipComponent,
    ListInternshipCompanyComponent,
    DetailsInternshipComponent,
    ApplicationInternshipComponent,
    CreateInternshipComponent,
    ListCondidateComponent,
    ProfilesocietyComponent,
    StudentProfileComponent,
    HomeStudentComponent,
    HomeCompanyComponent,
    EvaluationsComponent,
    SideBarComponent,
    ListInvitationInterviewComponent,
    InvitationInterviewConfirmationComponent,
    InvitationInterviewCancellationComponent,
    LoginCompanyComponent,
    InvitationDialogComponent,
    ListApplicationComponent,
    ListInterviewsComponent,
    ListInternsComponent,
    ListNumberApplicationPerInternshipComponent,
    StudInterviewComponent
  ],
  entryComponents: [ViewsDeleteDialogComponent]
})
export class EnisIntershipViewsModule {}
