import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IDeadline, Deadline } from 'app/shared/model/deadline.model';
import { DeadlineService } from './deadline.service';

@Component({
  selector: 'jhi-deadline-update',
  templateUrl: './deadline-update.component.html'
})
export class DeadlineUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    start: [null, [Validators.required]],
    end: [null, [Validators.required]],
    type: [null, [Validators.required]]
  });

  constructor(protected deadlineService: DeadlineService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ deadline }) => {
      if (!deadline.id) {
        const today = moment().startOf('day');
        deadline.start = today;
        deadline.end = today;
      }

      this.updateForm(deadline);
    });
  }

  updateForm(deadline: IDeadline): void {
    this.editForm.patchValue({
      id: deadline.id,
      start: deadline.start ? deadline.start.format(DATE_TIME_FORMAT) : null,
      end: deadline.end ? deadline.end.format(DATE_TIME_FORMAT) : null,
      type: deadline.type
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const deadline = this.createFromForm();
    if (deadline.id !== undefined) {
      this.subscribeToSaveResponse(this.deadlineService.update(deadline));
    } else {
      this.subscribeToSaveResponse(this.deadlineService.create(deadline));
    }
  }

  private createFromForm(): IDeadline {
    return {
      ...new Deadline(),
      id: this.editForm.get(['id'])!.value,
      start: this.editForm.get(['start'])!.value ? moment(this.editForm.get(['start'])!.value, DATE_TIME_FORMAT) : undefined,
      end: this.editForm.get(['end'])!.value ? moment(this.editForm.get(['end'])!.value, DATE_TIME_FORMAT) : undefined,
      type: this.editForm.get(['type'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDeadline>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
