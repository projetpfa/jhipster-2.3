import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EnisIntershipSharedModule } from 'app/shared/shared.module';
import { DeadlineComponent } from './deadline.component';
import { DeadlineDetailComponent } from './deadline-detail.component';
import { DeadlineUpdateComponent } from './deadline-update.component';
import { DeadlineDeleteDialogComponent } from './deadline-delete-dialog.component';
import { deadlineRoute } from './deadline.route';

@NgModule({
  imports: [EnisIntershipSharedModule, RouterModule.forChild(deadlineRoute)],
  declarations: [DeadlineComponent, DeadlineDetailComponent, DeadlineUpdateComponent, DeadlineDeleteDialogComponent],
  entryComponents: [DeadlineDeleteDialogComponent]
})
export class EnisIntershipDeadlineModule {}
