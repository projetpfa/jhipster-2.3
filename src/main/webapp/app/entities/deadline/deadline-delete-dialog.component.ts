import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDeadline } from 'app/shared/model/deadline.model';
import { DeadlineService } from './deadline.service';

@Component({
  templateUrl: './deadline-delete-dialog.component.html'
})
export class DeadlineDeleteDialogComponent {
  deadline?: IDeadline;

  constructor(protected deadlineService: DeadlineService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.deadlineService.delete(id).subscribe(() => {
      this.eventManager.broadcast('deadlineListModification');
      this.activeModal.close();
    });
  }
}
