import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IDeadline } from 'app/shared/model/deadline.model';

type EntityResponseType = HttpResponse<IDeadline>;
type EntityArrayResponseType = HttpResponse<IDeadline[]>;

@Injectable({ providedIn: 'root' })
export class DeadlineService {
  public resourceUrl = SERVER_API_URL + 'api/deadlines';

  constructor(protected http: HttpClient) {}

  create(deadline: IDeadline): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(deadline);
    return this.http
      .post<IDeadline>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(deadline: IDeadline): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(deadline);
    return this.http
      .put<IDeadline>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IDeadline>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IDeadline[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(deadline: IDeadline): IDeadline {
    const copy: IDeadline = Object.assign({}, deadline, {
      start: deadline.start && deadline.start.isValid() ? deadline.start.toJSON() : undefined,
      end: deadline.end && deadline.end.isValid() ? deadline.end.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.start = res.body.start ? moment(res.body.start) : undefined;
      res.body.end = res.body.end ? moment(res.body.end) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((deadline: IDeadline) => {
        deadline.start = deadline.start ? moment(deadline.start) : undefined;
        deadline.end = deadline.end ? moment(deadline.end) : undefined;
      });
    }
    return res;
  }
}
