import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IDeadline, Deadline } from 'app/shared/model/deadline.model';
import { DeadlineService } from './deadline.service';
import { DeadlineComponent } from './deadline.component';
import { DeadlineDetailComponent } from './deadline-detail.component';
import { DeadlineUpdateComponent } from './deadline-update.component';

@Injectable({ providedIn: 'root' })
export class DeadlineResolve implements Resolve<IDeadline> {
  constructor(private service: DeadlineService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDeadline> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((deadline: HttpResponse<Deadline>) => {
          if (deadline.body) {
            return of(deadline.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Deadline());
  }
}

export const deadlineRoute: Routes = [
  {
    path: '',
    component: DeadlineComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.deadline.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: DeadlineDetailComponent,
    resolve: {
      deadline: DeadlineResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.deadline.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: DeadlineUpdateComponent,
    resolve: {
      deadline: DeadlineResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.deadline.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: DeadlineUpdateComponent,
    resolve: {
      deadline: DeadlineResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.deadline.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
