import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IDeadline } from 'app/shared/model/deadline.model';
import { DeadlineService } from './deadline.service';
import { DeadlineDeleteDialogComponent } from './deadline-delete-dialog.component';

@Component({
  selector: 'jhi-deadline',
  templateUrl: './deadline.component.html'
})
export class DeadlineComponent implements OnInit, OnDestroy {
  deadlines?: IDeadline[];
  eventSubscriber?: Subscription;

  constructor(protected deadlineService: DeadlineService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.deadlineService.query().subscribe((res: HttpResponse<IDeadline[]>) => (this.deadlines = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInDeadlines();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IDeadline): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInDeadlines(): void {
    this.eventSubscriber = this.eventManager.subscribe('deadlineListModification', () => this.loadAll());
  }

  delete(deadline: IDeadline): void {
    const modalRef = this.modalService.open(DeadlineDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.deadline = deadline;
  }
}
