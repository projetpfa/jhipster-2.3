import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDeadline } from 'app/shared/model/deadline.model';

@Component({
  selector: 'jhi-deadline-detail',
  templateUrl: './deadline-detail.component.html'
})
export class DeadlineDetailComponent implements OnInit {
  deadline: IDeadline | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ deadline }) => (this.deadline = deadline));
  }

  previousState(): void {
    window.history.back();
  }
}
