import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IInvitationInterview } from 'app/shared/model/invitation-interview.model';

@Component({
  selector: 'jhi-invitation-interview-detail',
  templateUrl: './invitation-interview-detail.component.html'
})
export class InvitationInterviewDetailComponent implements OnInit {
  invitationInterview: IInvitationInterview | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ invitationInterview }) => (this.invitationInterview = invitationInterview));
  }

  previousState(): void {
    window.history.back();
  }
}
