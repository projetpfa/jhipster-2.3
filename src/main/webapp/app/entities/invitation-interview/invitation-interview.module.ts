import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EnisIntershipSharedModule } from 'app/shared/shared.module';
import { InvitationInterviewComponent } from './invitation-interview.component';
import { InvitationInterviewDetailComponent } from './invitation-interview-detail.component';
import { InvitationInterviewUpdateComponent } from './invitation-interview-update.component';
import { InvitationInterviewDeleteDialogComponent } from './invitation-interview-delete-dialog.component';
import { invitationInterviewRoute } from './invitation-interview.route';

@NgModule({
  imports: [EnisIntershipSharedModule, RouterModule.forChild(invitationInterviewRoute)],
  declarations: [
    InvitationInterviewComponent,
    InvitationInterviewDetailComponent,
    InvitationInterviewUpdateComponent,
    InvitationInterviewDeleteDialogComponent
  ],
  entryComponents: [InvitationInterviewDeleteDialogComponent]
})
export class EnisIntershipInvitationInterviewModule {}
