import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IInvitationInterview, InvitationInterview } from 'app/shared/model/invitation-interview.model';
import { InvitationInterviewService } from './invitation-interview.service';
import { InvitationInterviewComponent } from './invitation-interview.component';
import { InvitationInterviewDetailComponent } from './invitation-interview-detail.component';
import { InvitationInterviewUpdateComponent } from './invitation-interview-update.component';

@Injectable({ providedIn: 'root' })
export class InvitationInterviewResolve implements Resolve<IInvitationInterview> {
  constructor(private service: InvitationInterviewService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IInvitationInterview> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((invitationInterview: HttpResponse<InvitationInterview>) => {
          if (invitationInterview.body) {
            return of(invitationInterview.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new InvitationInterview());
  }
}

export const invitationInterviewRoute: Routes = [
  {
    path: '',
    component: InvitationInterviewComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'enisIntershipApp.invitationInterview.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: InvitationInterviewDetailComponent,
    resolve: {
      invitationInterview: InvitationInterviewResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.invitationInterview.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: InvitationInterviewUpdateComponent,
    resolve: {
      invitationInterview: InvitationInterviewResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.invitationInterview.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: InvitationInterviewUpdateComponent,
    resolve: {
      invitationInterview: InvitationInterviewResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.invitationInterview.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
