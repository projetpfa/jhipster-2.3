import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IInvitationInterview, InvitationInterview } from 'app/shared/model/invitation-interview.model';
import { InvitationInterviewService } from './invitation-interview.service';
import { IApplication } from 'app/shared/model/application.model';
import { ApplicationService } from 'app/entities/application/application.service';
import { ICompany } from 'app/shared/model/company.model';
import { CompanyService } from 'app/entities/company/company.service';

type SelectableEntity = IApplication | ICompany;

@Component({
  selector: 'jhi-invitation-interview-update',
  templateUrl: './invitation-interview-update.component.html'
})
export class InvitationInterviewUpdateComponent implements OnInit {
  isSaving = false;
  applications: IApplication[] = [];
  companies: ICompany[] = [];

  editForm = this.fb.group({
    id: [],
    confirmation: [],
    dateInterview: [null, [Validators.required]],
    application: [],
    company: []
  });

  constructor(
    protected invitationInterviewService: InvitationInterviewService,
    protected applicationService: ApplicationService,
    protected companyService: CompanyService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ invitationInterview }) => {
      if (!invitationInterview.id) {
        const today = moment().startOf('day');
        invitationInterview.dateInterview = today;
      }

      this.updateForm(invitationInterview);

      this.applicationService
        .query({ filter: 'invitationinterview-is-null' })
        .pipe(
          map((res: HttpResponse<IApplication[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IApplication[]) => {
          if (!invitationInterview.application || !invitationInterview.application.id) {
            this.applications = resBody;
          } else {
            this.applicationService
              .find(invitationInterview.application.id)
              .pipe(
                map((subRes: HttpResponse<IApplication>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IApplication[]) => (this.applications = concatRes));
          }
        });

      this.companyService.query().subscribe((res: HttpResponse<ICompany[]>) => (this.companies = res.body || []));
    });
  }

  updateForm(invitationInterview: IInvitationInterview): void {
    this.editForm.patchValue({
      id: invitationInterview.id,
      confirmation: invitationInterview.confirmation,
      dateInterview: invitationInterview.dateInterview ? invitationInterview.dateInterview.format(DATE_TIME_FORMAT) : null,
      application: invitationInterview.application,
      company: invitationInterview.company
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const invitationInterview = this.createFromForm();
    if (invitationInterview.id !== undefined) {
      this.subscribeToSaveResponse(this.invitationInterviewService.update(invitationInterview));
    } else {
      this.subscribeToSaveResponse(this.invitationInterviewService.create(invitationInterview));
    }
  }

  private createFromForm(): IInvitationInterview {
    return {
      ...new InvitationInterview(),
      id: this.editForm.get(['id'])!.value,
      confirmation: this.editForm.get(['confirmation'])!.value,
      dateInterview: this.editForm.get(['dateInterview'])!.value
        ? moment(this.editForm.get(['dateInterview'])!.value, DATE_TIME_FORMAT)
        : undefined,
      application: this.editForm.get(['application'])!.value,
      company: this.editForm.get(['company'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IInvitationInterview>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
