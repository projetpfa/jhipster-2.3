import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IInvitationInterview } from 'app/shared/model/invitation-interview.model';

type EntityResponseType = HttpResponse<IInvitationInterview>;
type EntityArrayResponseType = HttpResponse<IInvitationInterview[]>;

@Injectable({ providedIn: 'root' })
export class InvitationInterviewService {
  public resourceUrl = SERVER_API_URL + 'api/invitation-interviews';

  constructor(protected http: HttpClient) {}

  create(invitationInterview: IInvitationInterview): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(invitationInterview);
    return this.http
      .post<IInvitationInterview>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(invitationInterview: IInvitationInterview): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(invitationInterview);
    return this.http
      .put<IInvitationInterview>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IInvitationInterview>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IInvitationInterview[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  getInvitationInterviewsByCompanyLoginAndConfirmation(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IInvitationInterview[]>(`${this.resourceUrl}/company`, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(invitationInterview: IInvitationInterview): IInvitationInterview {
    const copy: IInvitationInterview = Object.assign({}, invitationInterview, {
      dateInterview:
        invitationInterview.dateInterview && invitationInterview.dateInterview.isValid()
          ? invitationInterview.dateInterview.toJSON()
          : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateInterview = res.body.dateInterview ? moment(res.body.dateInterview) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((invitationInterview: IInvitationInterview) => {
        invitationInterview.dateInterview = invitationInterview.dateInterview ? moment(invitationInterview.dateInterview) : undefined;
      });
    }
    return res;
  }

  getInvitationInterviewsByStudentLogin(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IInvitationInterview[]>(`${this.resourceUrl}/student`, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  getNumberOfInvitationInterviewsByStudentLogin(): any {
    return this.http.get<Number>(`${this.resourceUrl}/nbrInvit`);
  }
  updateConfirmation(confirmation: String, id?: number): Observable<EntityResponseType> {
    return this.http
      .put<IInvitationInterview>(`${this.resourceUrl}/update/${id}`, confirmation, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }
}
