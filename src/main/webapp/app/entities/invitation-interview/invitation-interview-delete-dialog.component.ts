import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IInvitationInterview } from 'app/shared/model/invitation-interview.model';
import { InvitationInterviewService } from './invitation-interview.service';

@Component({
  templateUrl: './invitation-interview-delete-dialog.component.html'
})
export class InvitationInterviewDeleteDialogComponent {
  invitationInterview?: IInvitationInterview;

  constructor(
    protected invitationInterviewService: InvitationInterviewService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.invitationInterviewService.delete(id).subscribe(() => {
      this.eventManager.broadcast('invitationInterviewListModification');
      this.activeModal.close();
    });
  }
}
