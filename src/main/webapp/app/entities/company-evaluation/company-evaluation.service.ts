import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICompanyEvaluation } from 'app/shared/model/company-evaluation.model';
import { ICompany } from 'app/shared/model/company.model';

type EntityResponseType = HttpResponse<ICompanyEvaluation>;
type EntityArrayResponseType = HttpResponse<ICompanyEvaluation[]>;

@Injectable({ providedIn: 'root' })
export class CompanyEvaluationService {
  public resourceUrl = SERVER_API_URL + 'api/company-evaluations';
  public resourceUrl2 = SERVER_API_URL + 'api/company-evaluation';

  constructor(protected http: HttpClient) {}

  create(companyEvaluation: ICompanyEvaluation): Observable<EntityResponseType> {
    return this.http.post<ICompanyEvaluation>(this.resourceUrl, companyEvaluation, { observe: 'response' });
  }

  update(companyEvaluation: ICompanyEvaluation): Observable<EntityResponseType> {
    return this.http.put<ICompanyEvaluation>(this.resourceUrl, companyEvaluation, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICompanyEvaluation>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICompanyEvaluation[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
  getCompanyEvaluationsByCompany(company: ICompany): Observable<EntityArrayResponseType> {
    return this.http.get<ICompanyEvaluation[]>(`${this.resourceUrl2}/${company.id}`, { observe: 'response' });
  }
}
