import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICompanyEvaluation } from 'app/shared/model/company-evaluation.model';

@Component({
  selector: 'jhi-company-evaluation-detail',
  templateUrl: './company-evaluation-detail.component.html'
})
export class CompanyEvaluationDetailComponent implements OnInit {
  companyEvaluation: ICompanyEvaluation | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ companyEvaluation }) => (this.companyEvaluation = companyEvaluation));
  }

  previousState(): void {
    window.history.back();
  }
}
