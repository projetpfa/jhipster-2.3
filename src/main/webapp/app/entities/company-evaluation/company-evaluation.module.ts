import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EnisIntershipSharedModule } from 'app/shared/shared.module';
import { CompanyEvaluationComponent } from './company-evaluation.component';
import { CompanyEvaluationDetailComponent } from './company-evaluation-detail.component';
import { CompanyEvaluationUpdateComponent } from './company-evaluation-update.component';
import { CompanyEvaluationDeleteDialogComponent } from './company-evaluation-delete-dialog.component';
import { companyEvaluationRoute } from './company-evaluation.route';

@NgModule({
  imports: [EnisIntershipSharedModule, RouterModule.forChild(companyEvaluationRoute)],
  declarations: [
    CompanyEvaluationComponent,
    CompanyEvaluationDetailComponent,
    CompanyEvaluationUpdateComponent,
    CompanyEvaluationDeleteDialogComponent
  ],
  entryComponents: [CompanyEvaluationDeleteDialogComponent]
})
export class EnisIntershipCompanyEvaluationModule {}
