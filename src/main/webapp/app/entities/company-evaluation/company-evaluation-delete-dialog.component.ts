import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICompanyEvaluation } from 'app/shared/model/company-evaluation.model';
import { CompanyEvaluationService } from './company-evaluation.service';

@Component({
  templateUrl: './company-evaluation-delete-dialog.component.html'
})
export class CompanyEvaluationDeleteDialogComponent {
  companyEvaluation?: ICompanyEvaluation;

  constructor(
    protected companyEvaluationService: CompanyEvaluationService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.companyEvaluationService.delete(id).subscribe(() => {
      this.eventManager.broadcast('companyEvaluationListModification');
      this.activeModal.close();
    });
  }
}
