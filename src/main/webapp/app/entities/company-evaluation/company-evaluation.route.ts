import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICompanyEvaluation, CompanyEvaluation } from 'app/shared/model/company-evaluation.model';
import { CompanyEvaluationService } from './company-evaluation.service';
import { CompanyEvaluationComponent } from './company-evaluation.component';
import { CompanyEvaluationDetailComponent } from './company-evaluation-detail.component';
import { CompanyEvaluationUpdateComponent } from './company-evaluation-update.component';

@Injectable({ providedIn: 'root' })
export class CompanyEvaluationResolve implements Resolve<ICompanyEvaluation> {
  constructor(private service: CompanyEvaluationService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICompanyEvaluation> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((companyEvaluation: HttpResponse<CompanyEvaluation>) => {
          if (companyEvaluation.body) {
            return of(companyEvaluation.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CompanyEvaluation());
  }
}

export const companyEvaluationRoute: Routes = [
  {
    path: '',
    component: CompanyEvaluationComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'enisIntershipApp.companyEvaluation.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: CompanyEvaluationDetailComponent,
    resolve: {
      companyEvaluation: CompanyEvaluationResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.companyEvaluation.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: CompanyEvaluationUpdateComponent,
    resolve: {
      companyEvaluation: CompanyEvaluationResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.companyEvaluation.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: CompanyEvaluationUpdateComponent,
    resolve: {
      companyEvaluation: CompanyEvaluationResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.companyEvaluation.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
