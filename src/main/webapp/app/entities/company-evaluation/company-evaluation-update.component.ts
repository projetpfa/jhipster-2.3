import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ICompanyEvaluation, CompanyEvaluation } from 'app/shared/model/company-evaluation.model';
import { CompanyEvaluationService } from './company-evaluation.service';
import { IAffectation } from 'app/shared/model/affectation.model';
import { AffectationService } from 'app/entities/affectation/affectation.service';
import { ICompany } from 'app/shared/model/company.model';
import { CompanyService } from 'app/entities/company/company.service';
import { IStudent } from 'app/shared/model/student.model';
import { StudentService } from 'app/entities/student/student.service';

type SelectableEntity = IAffectation | ICompany | IStudent;

@Component({
  selector: 'jhi-company-evaluation-update',
  templateUrl: './company-evaluation-update.component.html'
})
export class CompanyEvaluationUpdateComponent implements OnInit {
  isSaving = false;
  affectations: IAffectation[] = [];
  companies: ICompany[] = [];
  students: IStudent[] = [];

  editForm = this.fb.group({
    id: [],
    supervision: [null, [Validators.required]],
    acceuil: [null, [Validators.required]],
    affectation: [],
    company: [],
    student: []
  });

  constructor(
    protected companyEvaluationService: CompanyEvaluationService,
    protected affectationService: AffectationService,
    protected companyService: CompanyService,
    protected studentService: StudentService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ companyEvaluation }) => {
      this.updateForm(companyEvaluation);

      this.affectationService
        .query({ filter: 'companyevaluation-is-null' })
        .pipe(
          map((res: HttpResponse<IAffectation[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IAffectation[]) => {
          if (!companyEvaluation.affectation || !companyEvaluation.affectation.id) {
            this.affectations = resBody;
          } else {
            this.affectationService
              .find(companyEvaluation.affectation.id)
              .pipe(
                map((subRes: HttpResponse<IAffectation>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IAffectation[]) => (this.affectations = concatRes));
          }
        });

      this.companyService.query().subscribe((res: HttpResponse<ICompany[]>) => (this.companies = res.body || []));

      this.studentService.query().subscribe((res: HttpResponse<IStudent[]>) => (this.students = res.body || []));
    });
  }

  updateForm(companyEvaluation: ICompanyEvaluation): void {
    this.editForm.patchValue({
      id: companyEvaluation.id,
      supervision: companyEvaluation.supervision,
      acceuil: companyEvaluation.acceuil,
      affectation: companyEvaluation.affectation,
      company: companyEvaluation.company,
      student: companyEvaluation.student
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const companyEvaluation = this.createFromForm();
    if (companyEvaluation.id !== undefined) {
      this.subscribeToSaveResponse(this.companyEvaluationService.update(companyEvaluation));
    } else {
      this.subscribeToSaveResponse(this.companyEvaluationService.create(companyEvaluation));
    }
  }

  private createFromForm(): ICompanyEvaluation {
    return {
      ...new CompanyEvaluation(),
      id: this.editForm.get(['id'])!.value,
      supervision: this.editForm.get(['supervision'])!.value,
      acceuil: this.editForm.get(['acceuil'])!.value,
      affectation: this.editForm.get(['affectation'])!.value,
      company: this.editForm.get(['company'])!.value,
      student: this.editForm.get(['student'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICompanyEvaluation>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
