import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EnisIntershipSharedModule } from 'app/shared/shared.module';
import { InternshipComponent } from './internship.component';
import { InternshipDetailComponent } from './internship-detail.component';
import { InternshipUpdateComponent } from './internship-update.component';
import { InternshipDeleteDialogComponent } from './internship-delete-dialog.component';
import { internshipRoute } from './internship.route';

@NgModule({
  imports: [EnisIntershipSharedModule, RouterModule.forChild(internshipRoute)],
  declarations: [InternshipComponent, InternshipDetailComponent, InternshipUpdateComponent, InternshipDeleteDialogComponent],
  entryComponents: [InternshipDeleteDialogComponent]
})
export class EnisIntershipInternshipModule {}
