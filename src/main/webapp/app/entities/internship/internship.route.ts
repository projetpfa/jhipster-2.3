import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IInternship, Internship } from 'app/shared/model/internship.model';
import { InternshipService } from './internship.service';
import { InternshipComponent } from './internship.component';
import { InternshipDetailComponent } from './internship-detail.component';
import { InternshipUpdateComponent } from './internship-update.component';

@Injectable({ providedIn: 'root' })
export class InternshipResolve implements Resolve<IInternship> {
  constructor(private service: InternshipService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IInternship> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((internship: HttpResponse<Internship>) => {
          if (internship.body) {
            return of(internship.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Internship());
  }
}

export const internshipRoute: Routes = [
  {
    path: '',
    component: InternshipComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'enisIntershipApp.internship.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: InternshipDetailComponent,
    resolve: {
      internship: InternshipResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.internship.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: InternshipUpdateComponent,
    resolve: {
      internship: InternshipResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.internship.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: InternshipUpdateComponent,
    resolve: {
      internship: InternshipResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.internship.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
