import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IInternship } from 'app/shared/model/internship.model';

type EntityResponseType = HttpResponse<IInternship>;
type EntityArrayResponseType = HttpResponse<IInternship[]>;

@Injectable({ providedIn: 'root' })
export class InternshipService {
  public resourceUrl = SERVER_API_URL + 'api/internships';

  constructor(protected http: HttpClient) {}

  create(internship: IInternship): Observable<EntityResponseType> {
    return this.http.post<IInternship>(this.resourceUrl, internship, { observe: 'response' });
  }

  update(internship: IInternship): Observable<EntityResponseType> {
    return this.http.put<IInternship>(this.resourceUrl, internship, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IInternship>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IInternship[]>(this.resourceUrl, { params: options, observe: 'response' });
  }
  getInternshipByCompanyLogin(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IInternship[]>(`${this.resourceUrl}/company`, { params: options, observe: 'response' });
  }
  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
  getNumberOfInternshipByStudentLoginDomain(): any {
    return this.http.get<Number>(`${this.resourceUrl}/NbreInternship/Student`);
  }

  getInternshipByLoginStudent(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IInternship[]>(`${this.resourceUrl}/AllInternships/Student`, { params: options, observe: 'response' });
  }

  getNumberOfInternshipByCompanyLogin(): any {
    return this.http.get<Number>(`${this.resourceUrl}/NbreInternship/Company`);
  }
}
