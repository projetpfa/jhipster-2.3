import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IInternship } from 'app/shared/model/internship.model';
import { InternshipService } from './internship.service';

@Component({
  templateUrl: './internship-delete-dialog.component.html'
})
export class InternshipDeleteDialogComponent {
  internship?: IInternship;

  constructor(
    protected internshipService: InternshipService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.internshipService.delete(id).subscribe(() => {
      this.eventManager.broadcast('internshipListModification');
      this.activeModal.close();
    });
  }
}
