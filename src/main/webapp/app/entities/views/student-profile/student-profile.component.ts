import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { IStudent } from 'app/shared/model/student.model';

import { IAffectation } from 'app/shared/model/affectation.model';
import { AffectationService } from 'app/entities/affectation/affectation.service';
import { EvaluationsComponent } from '../evaluations/evaluations.component';
import { StudentEvaluationService } from 'app/entities/student-evaluation/student-evaluation.service';
import { IStudentEvaluation } from 'app/shared/model/student-evaluation.model';

@Component({
  selector: 'jhi-student-profile',
  templateUrl: './student-profile.component.html',
  styleUrls: ['./student-profile.component.scss']
})
export class StudentProfileComponent implements OnInit, OnDestroy {
  studentEvaluations: IStudentEvaluation[] = [];
  affectations?: IAffectation[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;

  nbOfEvaluation: number | undefined;
  nbOfStarsCommunication: number | undefined;
  nbOfStarsTechnical: number | undefined;
  totalScoreCommunication = 0;
  totalScoreTechnical = 0;

  student!: IStudent;
  today: Date = (Date.now() as unknown) as Date;
  constructor(
    protected affectationService: AffectationService,
    protected studentEvaluationService: StudentEvaluationService,
    protected activatedRoute: ActivatedRoute,
    protected dataUtils: JhiDataUtils,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ student }) => (this.student = student));
    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.ngbPaginationPage = data.pagingParams.page;
      this.loadPage();
    });

    this.loadAll();
    this.registerChangeInAffectations();
    this.registerChangeInStudentEvaluations();
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  previousState(): void {
    window.history.back();
  }

  myFunction1(): void {
    const x1 = document.getElementById('about');
    const x2 = document.getElementById('internship');
    if (x1 != null && x2 != null) {
      x1.style.display = 'block';
      x2.style.display = 'none';
    }
  }

  myFunction2(): void {
    const x1 = document.getElementById('about');
    const x2 = document.getElementById('internship');
    if (x1 != null && x2 != null) {
      x1.style.display = 'none';
      x2.style.display = 'block';
    }
  }

  loadAll(): void {
    this.affectationService
      .getAffectationsByStudent(this.student)
      .subscribe((res: HttpResponse<IAffectation[]>) => (this.affectations = res.body || []));
  }
  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(item: IAffectation): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAffectations(): void {
    this.eventSubscriber = this.eventManager.subscribe('affectationListModification', () => this.loadAll());
  }

  registerChangeInStudentEvaluations(): void {
    this.eventSubscriber = this.eventManager.subscribe('studentEvaluationListModification', () => this.loadPage());
  }

  loadPage(page?: number): void {
    const pageToLoad: number = page || this.page;

    this.studentEvaluationService
      .getStudentEvaluationsByStudent(this.student, {
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IStudentEvaluation[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
        () => this.onError()
      );
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: IStudentEvaluation[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/views', this.student.id, 'student-profile'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.studentEvaluations = data || [];
    this.nbOfEvaluation = this.studentEvaluations.length;
    if (this.nbOfEvaluation !== 0) {
      this.studentEvaluations.forEach(element => {
        if (element.communication) {
          this.totalScoreCommunication = element.communication + this.totalScoreCommunication;
        }
        if (element.technical) {
          this.totalScoreTechnical = element.technical + this.totalScoreTechnical;
        }
      });

      this.nbOfStarsCommunication = Math.round(this.totalScoreCommunication / this.nbOfEvaluation);
      this.nbOfStarsTechnical = Math.round(this.totalScoreTechnical / this.nbOfEvaluation);
    }
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }
  evaluate(affectation: IAffectation): void {
    const modalRef = this.modalService.open(EvaluationsComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.affectation = affectation;
    modalRef.componentInstance.evaluatingStudent = affectation.student;
    modalRef.componentInstance.company = affectation.internship?.company;
  }
}
