import { Component, OnInit } from '@angular/core';
import { IApplication } from 'app/shared/model/application.model';
import { Subscription } from 'rxjs';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ApplicationService } from 'app/entities/application/application.service';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse, HttpHeaders } from '@angular/common/http';
import { ApplicationDeleteDialogComponent } from 'app/entities/application/application-delete-dialog.component';
import { IInvitationInterview } from 'app/shared/model/invitation-interview.model';
import { InvitationInterviewService } from 'app/entities/invitation-interview/invitation-interview.service';
import { InvitationInterviewDeleteDialogComponent } from 'app/entities/invitation-interview/invitation-interview-delete-dialog.component';
import { InternshipService } from 'app/entities/internship/internship.service';
import { AccountService } from 'app/core/auth/account.service';
import { UserService } from 'app/core/user/user.service';
import { StudentService } from 'app/entities/student/student.service';
import { IStudent } from 'app/shared/model/student.model';
import { AffectationService } from 'app/entities/affectation/affectation.service';
import { IAffectation } from 'app/shared/model/affectation.model';

@Component({
  selector: 'jhi-home-student',
  templateUrl: './home-student.component.html',
  styleUrls: ['./home-student.component.scss']
})
export class HomeStudentComponent implements OnInit {
  student?: IStudent | null;
  affectations?: IAffectation[];
  invitationInterviews?: IInvitationInterview[];
  NbreOfinvitationInterviews?: number;
  NbreOfapplications?: number;
  NbreOfInternshipByDomain?: number;
  applications?: IApplication[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  numberIterations = 5;
  date = Date.now();

  constructor(
    protected invitationInterviewService: InvitationInterviewService,
    protected applicationService: ApplicationService,
    protected internshipService: InternshipService,
    protected activatedRoute: ActivatedRoute,
    protected dataUtils: JhiDataUtils,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected accountService: AccountService,
    protected userService: UserService,
    protected studentService: StudentService,
    protected affectationService: AffectationService
  ) {}
  loadAffectation(page?: number): void {
    const pageToLoad: number = page || this.page;
    if (this.student) {
      this.affectationService
        .getAffectationsByStudentLogin(this.student, {
          page: pageToLoad - 1,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe((res: HttpResponse<IAffectation[]>) => (this.affectations = res.body || []));
    }
  }

  loadPage(page?: number): void {
    const pageToLoad: number = page || this.page;

    this.applicationService
      .getApplicationpByStudentLogin({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IApplication[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
        () => this.onError()
      );
    this.invitationInterviewService
      .getInvitationInterviewsByStudentLogin({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IApplication[]>) => this.onSuccessInvitationStudent(res.body, res.headers, pageToLoad),
        () => this.onError()
      );
  }

  registerChangeInAffectations(): void {
    this.eventSubscriber = this.eventManager.subscribe('affectationListModification', () => this.loadAffectation());
  }

  ngOnInit(): void {
    this.accountService.identity().subscribe(account => {
      if (account) {
        this.userService.find(account.login).subscribe(user => {
          this.studentService.findStudentByUser(user).subscribe(student => {
            this.student = student;
            this.loadAffectation();
          });
        });
      }
    });

    this.invitationInterviewService.getNumberOfInvitationInterviewsByStudentLogin().subscribe((NbreOfinvitationInterviews: number) => {
      this.NbreOfinvitationInterviews = NbreOfinvitationInterviews;
    });
    this.applicationService.getNumberOfapplicationByStudentLogin().subscribe((NbreOfapplications: number) => {
      this.NbreOfapplications = NbreOfapplications;
    });

    this.internshipService.getNumberOfInternshipByStudentLoginDomain().subscribe((NbreOfInternshipByDomain: number) => {
      this.NbreOfInternshipByDomain = NbreOfInternshipByDomain;
    });

    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.ngbPaginationPage = data.pagingParams.page;
      this.loadPage();
    });
    this.registerChangeInApplications();
    this.registerChangeInAffectations();
  }

  trackId(index: number, item: IApplication): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }
  trackIdInvitInterview(index: number, item: IInvitationInterview): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  trackIdAffectation(item: IAffectation): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    return this.dataUtils.openFile(contentType, base64String);
  }

  registerChangeInApplications(): void {
    this.eventSubscriber = this.eventManager.subscribe('applicationListModification', () => this.loadPage());
  }
  registerChangeInInvitationInterviews(): void {
    this.eventSubscriber = this.eventManager.subscribe('invitationInterviewListModification', () => this.loadPage());
  }

  delete(application: IApplication): void {
    const modalRef = this.modalService.open(ApplicationDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.application = application;
  }
  delete1(invitationInterview: IInvitationInterview): void {
    const modalRef = this.modalService.open(InvitationInterviewDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.invitationInterview = invitationInterview;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccessInvitationStudent(data: IInvitationInterview[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/views/home-student'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.invitationInterviews = data || [];
  }

  protected onSuccess(data: IApplication[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/views/home-student'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.applications = data || [];
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }
}
