import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IInvitationInterview } from 'app/shared/model/invitation-interview.model';
import { InvitationInterviewService } from '../../invitation-interview/invitation-interview.service';

@Component({
  selector: 'jhi-invitation-interview-cancellation',
  templateUrl: './invitation-interview-cancellation.component.html',
  styleUrls: ['./invitation-interview-cancellation.component.scss']
})
export class InvitationInterviewCancellationComponent implements OnInit {
  @Input()
  invitationInterview: IInvitationInterview | undefined;

  constructor(
    protected invitationInterviewService: InvitationInterviewService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}
  ngOnInit(): void {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  annuler(invitationInterview: IInvitationInterview): void {
    if (invitationInterview !== undefined) {
      this.invitationInterviewService.updateConfirmation('false', invitationInterview.id).subscribe(() => {
        this.eventManager.broadcast('invitationInterviewListModification');
        this.activeModal.close();
      });
    }
  }
}
