import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IInternship } from 'app/shared/model/internship.model';

@Component({
  selector: 'jhi-details-internship',
  templateUrl: './details-internship.component.html'
  /* styleUrls: ['./details-internship.component.scss'] */
})
export class DetailsInternshipComponent implements OnInit {
  internship: IInternship | null = null;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ internship }) => (this.internship = internship));
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  previousState(): void {
    window.history.back();
  }
}
