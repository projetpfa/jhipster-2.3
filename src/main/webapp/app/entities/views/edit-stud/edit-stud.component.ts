import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiDataUtils, JhiFileLoadError, JhiEventManager, JhiEventWithContent } from 'ng-jhipster';

import { IStudent, Student } from 'app/shared/model/student.model';

import { AlertError } from 'app/shared/alert/alert-error.model';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IDepartment } from 'app/shared/model/department.model';
import { DepartmentService } from 'app/entities/department/department.service';
import { StudentService } from 'app/entities/student/student.service';
import { Account } from 'app/core/user/account.model';
import { AccountService } from 'app/core/auth/account.service';


type SelectableEntity = IUser | IDepartment;

@Component({
  selector: 'jhi-edit-stud',
  templateUrl: './edit-stud.component.html'
})
export class EditStudComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];
  departments: IDepartment[] = [];
  account!: Account;
  editForm = this.fb.group({
    id: [],
    photo: [],
    photoContentType: [],
    city: [null, [Validators.required]],
    country: [null, [Validators.required]],
    postalCode: [null, [Validators.required]],
    phoneNumber: [null, [Validators.required]],
    cv: [null, [Validators.required]],
    cvContentType: [],
    level: [null, [Validators.required]],
    user: [],
    department: [],
    firstName: [undefined, [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
    lastName: [undefined, [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
    email: [undefined, [Validators.required, Validators.minLength(5), Validators.maxLength(254), Validators.email]]
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected studentService: StudentService,
    protected userService: UserService,
    protected departmentService: DepartmentService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
    private accountService: AccountService

  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ student }) => {
      this.updateForm(student);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));

      this.departmentService.query().subscribe((res: HttpResponse<IDepartment[]>) => (this.departments = res.body || []));
    });
    this.accountService.identity().subscribe(account => {
      if (account) {
        this.editForm.patchValue({
          firstName: account.firstName,
          lastName: account.lastName,
          email: account.email
    });
        this.account = account;
      }
    });
  }

  updateForm(student: IStudent): void {
    this.editForm.patchValue({
      id: student.id,
      photo: student.photo,
      photoContentType: student.photoContentType,
      city: student.city,
      country: student.country,
      postalCode: student.postalCode,
      phoneNumber: student.phoneNumber,
      cv: student.cv,
      cvContentType: student.cvContentType,
      level: student.level,
      user: student.user,
      department: student.department,

    });
  }
  updateFormUser(user: IUser): void {
    this.editForm.patchValue({
      firstName:user.firstName,
      lastName: user.lastName,
      email: user.email
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
      this.eventManager.broadcast(
        new JhiEventWithContent<AlertError>('enisIntershipApp.error', { ...err, key: 'error.file.' + err.key })
      );
    });
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string): void {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const student = this.createFromForm();
    if (student.id !== undefined) {
      this.subscribeToSaveResponse(this.studentService.update(student));
    } else {
      this.subscribeToSaveResponse(this.studentService.create(student));
    }
    this.account.firstName = this.editForm.get('firstName')!.value;
    this.account.lastName = this.editForm.get('lastName')!.value;
    this.account.email = this.editForm.get('email')!.value;


    this.accountService.save(this.account).subscribe(() => {


    this.accountService.authenticate(this.account);


  });
    this.router.navigate(['/views/home-student']);
  }

  private createFromForm(): IStudent {
    return {
      ...new Student(),
      id: this.editForm.get(['id'])!.value,
      photoContentType: this.editForm.get(['photoContentType'])!.value,
      photo: this.editForm.get(['photo'])!.value,
      city: this.editForm.get(['city'])!.value,
      country: this.editForm.get(['country'])!.value,
      postalCode: this.editForm.get(['postalCode'])!.value,
      phoneNumber: this.editForm.get(['phoneNumber'])!.value,
      cvContentType: this.editForm.get(['cvContentType'])!.value,
      cv: this.editForm.get(['cv'])!.value,
      level: this.editForm.get(['level'])!.value,
      user: this.editForm.get(['user'])!.value,
      department: this.editForm.get(['department'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStudent>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
