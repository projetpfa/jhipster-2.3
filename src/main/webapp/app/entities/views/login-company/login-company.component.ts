import { Component, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

import { LoginService } from 'app/core/login/login.service';
import { UserService } from 'app/core/user/user.service';
import { IUser } from 'app/core/user/user.model';

import { AccountService } from 'app/core/auth/account.service';
import { CompanyService } from 'app/entities/company/company.service';
@Component({
  selector: 'jhi-login-company',
  templateUrl: './login-company.component.html'
})
export class LoginCompanyComponent implements AfterViewInit {
  @ViewChild('username', { static: false })
  username?: ElementRef;

  authenticationError = false;
  currentUser: IUser | undefined;
  idCurrentUser: number | undefined;
  loginForm = this.fb.group({
    username: [''],
    password: [''],
    rememberMe: [false]
  });

  constructor(
    private loginService: LoginService,
    private router: Router,
    public activeModal: NgbActiveModal,
    private fb: FormBuilder,
    private userService: UserService,
    protected modalService: NgbModal,
    protected accountService: AccountService,
    protected companyService: CompanyService
  ) {}

  ngAfterViewInit(): void {
    if (this.username) {
      this.username.nativeElement.focus();
    }
  }

  cancel(): void {
    this.authenticationError = false;
    this.loginForm.patchValue({
      username: '',
      password: ''
    });
    this.activeModal.dismiss('cancel');
  }

  loginCompany(): void {
    this.loginService
      .login({
        username: this.loginForm.get('username')!.value,
        password: this.loginForm.get('password')!.value,
        rememberMe: this.loginForm.get('rememberMe')!.value
      })
      .subscribe(
        () => {
          this.authenticationError = false;
          this.activeModal.close();
          {
            this.userService.find(this.loginForm.get('username')!.value).subscribe(user => {
              this.companyService.findCompanyByUser(user).subscribe(company => {
                if (company) {
                  this.router.navigate(['/views/home-company']);
                } else if (company == null) {
                  this.router.navigate(['/company/new']);
                }
                this.activeModal.close();
              });
            });
          }
        },
        () => (this.authenticationError = true)
      );
  }

  register(): void {
    this.activeModal.dismiss('to state register');
    this.router.navigate(['/account/register']);
  }

  requestResetPassword(): void {
    this.activeModal.dismiss('to state requestReset');
    this.router.navigate(['/account/reset', 'request']);
  }
}
