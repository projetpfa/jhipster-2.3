import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IInvitationInterview } from 'app/shared/model/invitation-interview.model';
import { InvitationInterviewService } from '../../invitation-interview/invitation-interview.service';

@Component({
  selector: 'jhi-invitation-interview-confirmation',
  templateUrl: './invitation-interview-confirmation.component.html',
  styleUrls: ['./invitation-interview-confirmation.component.scss']
})
export class InvitationInterviewConfirmationComponent implements OnInit {
  @Input()
  invitationInterview: IInvitationInterview | undefined;

  constructor(
    protected invitationInterviewService: InvitationInterviewService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}
  ngOnInit(): void {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirm(invitationInterview: IInvitationInterview): void {
    if (invitationInterview !== undefined) {
      this.invitationInterviewService.updateConfirmation('true', invitationInterview.id).subscribe(() => {
        this.eventManager.broadcast('invitationInterviewListModification');
        this.activeModal.close();
      });
    }
  }
}
