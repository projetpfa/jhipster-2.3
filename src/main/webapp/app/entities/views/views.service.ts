import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IViews } from 'app/shared/model/views.model';

type EntityResponseType = HttpResponse<IViews>;
type EntityArrayResponseType = HttpResponse<IViews[]>;

@Injectable({ providedIn: 'root' })
export class ViewsService {
  public resourceUrl = SERVER_API_URL + 'api/views';

  constructor(protected http: HttpClient) {}

  create(views: IViews): Observable<EntityResponseType> {
    return this.http.post<IViews>(this.resourceUrl, views, { observe: 'response' });
  }

  update(views: IViews): Observable<EntityResponseType> {
    return this.http.put<IViews>(this.resourceUrl, views, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IViews>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IViews[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
