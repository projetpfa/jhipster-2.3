import { Component, Input } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IStudentEvaluation, StudentEvaluation } from 'app/shared/model/student-evaluation.model';
import { StudentEvaluationService } from 'app/entities/student-evaluation/student-evaluation.service';
import { IStudent } from 'app/shared/model/student.model';
import { ICompany } from 'app/shared/model/company.model';
import { Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CompanyService } from 'app/entities/company/company.service';
import { StudentService } from 'app/entities/student/student.service';
import { CompanyEvaluationService } from 'app/entities/company-evaluation/company-evaluation.service';
import { ICompanyEvaluation, CompanyEvaluation } from 'app/shared/model/company-evaluation.model';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IAffectation } from 'app/shared/model/affectation.model';
type SelectableEntity = IStudent | ICompany;
@Component({
  selector: 'jhi-evaluations',
  templateUrl: './evaluations.component.html',
  styleUrls: ['./evaluations.component.scss']
})
export class EvaluationsComponent {
  isSaving = false;
  students: IStudent[] = [];
  companies: ICompany[] = [];
  affectations: IAffectation[] = [];
  @Input()
  student: IStudent | undefined;
  @Input()
  company: ICompany | undefined;
  @Input()
  affectation: IAffectation | undefined;
  @Input()
  evaluatingCompany: ICompany | undefined;
  @Input()
  evaluatingStudent: IStudent | undefined;
  editForm = this.fb.group({
    id: [],
    communication: [null, [Validators.required]],
    technical: [null, [Validators.required]],
    supervision: [null, [Validators.required]],
    acceuil: [null, [Validators.required]],
    affectation: [],
    student: [],
    company: []
  });

  constructor(
    protected studentEvaluationService: StudentEvaluationService,
    protected companyEvaluationService: CompanyEvaluationService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager,
    protected studentService: StudentService,
    protected companyService: CompanyService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.activeModal.dismiss();
  }

  private createFromFormStudentEvaluation(): IStudentEvaluation {
    return {
      ...new StudentEvaluation(),

      communication: this.editForm.get(['communication'])!.value,
      technical: this.editForm.get(['technical'])!.value,
      affectation: this.affectation,
      student: this.student,
      company: this.evaluatingCompany
    };
  }

  private createFromFormCompanyEvaluation(): ICompanyEvaluation {
    return {
      ...new CompanyEvaluation(),

      supervision: this.editForm.get(['supervision'])!.value,
      acceuil: this.editForm.get(['acceuil'])!.value,
      affectation: this.affectation,
      company: this.company,
      student: this.evaluatingStudent
    };
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStudentEvaluation>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  confirmEvaluatiton(): void {
    this.isSaving = true;
    if (this.student) {
      const studentEvaluation = this.createFromFormStudentEvaluation();
      this.studentEvaluationService.create(studentEvaluation).subscribe(() => {
        this.eventManager.broadcast('studentEvaluationListModification');
        this.activeModal.close();
      });
    } else if (this.company) {
      const companyEvaluation = this.createFromFormCompanyEvaluation();
      this.companyEvaluationService.create(companyEvaluation).subscribe(() => {
        this.eventManager.broadcast('companyEvaluationListModification');
        this.activeModal.close();
      });
    }
  }
}
