import { Component, OnInit } from '@angular/core';

import { ICompany } from 'app/shared/model/company.model';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { EvaluationsComponent } from '../evaluations/evaluations.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CompanyEvaluationService } from 'app/entities/company-evaluation/company-evaluation.service';
import { Subscription } from 'rxjs';
import { ICompanyEvaluation } from 'app/shared/model/company-evaluation.model';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'jhi-profilesociety',
  templateUrl: './profilesociety.component.html'
})
export class ProfilesocietyComponent implements OnInit {
  company!: ICompany;
  eventSubscriber?: Subscription;

  companyEvaluations: ICompanyEvaluation[] = [];

  nbOfEvaluation: number | undefined;
  nbOfStarsSupervision: number | undefined;
  nbOfStarsAcceuil: number | undefined;
  totalScoreSupervision = 0;
  totalScoreAcceuil = 0;

  constructor(
    protected dataUtils: JhiDataUtils,
    protected activatedRoute: ActivatedRoute,
    protected modalService: NgbModal,
    protected companyEvaluationService: CompanyEvaluationService,
    protected eventManager: JhiEventManager,
    protected router: Router
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ company }) => (this.company = company));
    this.loadAll();
    this.registerChangeInCompanyEvaluations();
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  previousState(): void {
    window.history.back();
  }
  myFunction(): void {
    const x2 = document.getElementById('about');
    const x = document.getElementById('settings');
    if (x2 != null && x != null) {
      x2.style.display = 'block';
      x.style.display = 'none';
    }
  }

  myFunctionf(): void {
    const x2 = document.getElementById('about');
    const x = document.getElementById('settings');
    if (x2 != null && x != null) {
      x2.style.display = 'none';
      x.style.display = 'block';
    }
  }
  evaluate(company: ICompany): void {
    const modalRef = this.modalService.open(EvaluationsComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.company = company;
  }

  onAllumer(): void {
    const x2 = document.getElementById('about');

    const x = document.getElementById('settings');
    if (x2 != null && x != null) {
      x2.style.display = 'none';

      x.style.display = 'block';
    }
  }

  loadAll(): void {
    this.companyEvaluationService.getCompanyEvaluationsByCompany(this.company).subscribe((res: HttpResponse<ICompanyEvaluation[]>) => {
      this.companyEvaluations = res.body || [];
      this.nbOfEvaluation = this.companyEvaluations.length;
      if (this.nbOfEvaluation !== 0) {
        this.companyEvaluations.forEach(element => {
          if (element.supervision) {
            this.totalScoreSupervision = element.supervision + this.totalScoreSupervision;
          }
          if (element.acceuil) {
            this.totalScoreAcceuil = element.acceuil + this.totalScoreAcceuil;
          }
        });
        this.nbOfStarsSupervision = Math.round(this.totalScoreSupervision / this.nbOfEvaluation);
        this.nbOfStarsAcceuil = Math.round(this.totalScoreAcceuil / this.nbOfEvaluation);
      }
    });
  }
  registerChangeInCompanyEvaluations(): void {
    this.eventSubscriber = this.eventManager.subscribe('companyEvaluationListModification', () => this.loadAll());
  }

  trackId(item: ICompanyEvaluation): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }
}
