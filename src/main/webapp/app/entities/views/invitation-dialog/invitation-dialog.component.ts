import { Component, Input } from '@angular/core';
import { IInterview } from 'app/shared/model/interview.model';
import { IApplication } from 'app/shared/model/application.model';
import { ICompany } from 'app/shared/model/company.model';
import { IDeadline } from 'app/shared/model/deadline.model';
import { Validators, FormBuilder } from '@angular/forms';
import { InvitationInterviewService } from 'app/entities/invitation-interview/invitation-interview.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { ApplicationService } from 'app/entities/application/application.service';
import { ActivatedRoute } from '@angular/router';
import { IInvitationInterview, InvitationInterview } from 'app/shared/model/invitation-interview.model';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
type SelectableEntity = IApplication;
@Component({
  selector: 'jhi-invitation-dialog',
  templateUrl: './invitation-dialog.component.html',
  styleUrls: ['./invitation-dialog.component.scss']
})
export class InvitationDialogComponent {
  isSaving = false;

  interviews: IInterview[] = [];
  applications: IApplication[] = [];
  companies: ICompany[] = [];
  deadlines: IDeadline[] = [];

  @Input()
  application: IApplication | undefined;
  editForm = this.fb.group({
    id: [],
    confirmation: [],
    dateInterview: [null, [Validators.required]],
    interview: [],
    application: [],
    company: [],
    deadline: []
  });
  dataUtils: any;

  constructor(
    protected invitationInterviewService: InvitationInterviewService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager,
    protected applicationService: ApplicationService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  private createFromForm(): IInvitationInterview {
    return {
      ...new InvitationInterview(),

      dateInterview: this.editForm.get(['dateInterview'])!.value
        ? moment(this.editForm.get(['dateInterview'])!.value, DATE_TIME_FORMAT)
        : undefined,

      application: this.application
    };
  }
  DatetoString(disponibility: any): string {
    return moment(disponibility)
      .format('YYYY-MM-DD HH:mm:ss')
      .toString();
  }
  protected subscribeToSaveResponse(result: Observable<HttpResponse<IInvitationInterview>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  confirmInvitation(): void {
    this.isSaving = true;
    const invitationInterview = this.createFromForm();
    this.invitationInterviewService.create(invitationInterview).subscribe(() => {
      this.eventManager.broadcast('invitationInterviewListModification');
      this.activeModal.close();
    });
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.activeModal.dismiss();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }

  cancel(): void {
    this.activeModal.dismiss();
  }
}
