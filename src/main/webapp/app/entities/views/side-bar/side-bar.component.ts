import { Component, OnInit } from '@angular/core';
import { AccountService } from 'app/core/auth/account.service';
import { UserService } from 'app/core/user/user.service';
import { StudentService } from 'app/entities/student/student.service';
import { IStudent } from 'app/shared/model/student.model';
import { CompanyService } from 'app/entities/company/company.service';
import { ICompany } from 'app/shared/model/company.model';
import { LoginService } from 'app/core/login/login.service';
import { Router } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

@Component({
  selector: 'jhi-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {
  student!: IStudent | null;
  company!: ICompany | null;

  constructor(
    private accountService: AccountService,
    protected userService: UserService,
    protected studentService: StudentService,
    protected companyService: CompanyService,
    protected loginService: LoginService,
    protected router: Router,
    protected dataUtils: JhiDataUtils
  ) {}

  ngOnInit(): void {
    this.accountService.identity().subscribe(account => {
      if (account) {
        this.userService.find(account.login).subscribe(user => {
          if (user.authorities?.indexOf("'ROLE_STUDENT'")) {
            this.studentService.findStudentByUser(user).subscribe(student => (this.student = student));
          }
          if (user.authorities?.indexOf("'ROLE_COMPANY'")) {
            this.companyService.findCompanyByUser(user).subscribe(company => (this.company = company));
          }
        });
      }
    });
  }
  logout(): void {
    this.loginService.logout();
    this.router.navigate(['']);
  }
  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }
}
