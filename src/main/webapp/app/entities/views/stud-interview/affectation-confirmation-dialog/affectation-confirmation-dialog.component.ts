import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { AffectationService } from 'app/entities/affectation/affectation.service';
import { IAffectation, Affectation } from 'app/shared/model/affectation.model';
import { IInterview } from 'app/shared/model/interview.model';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import * as moment from 'moment';
import { Choice } from 'app/shared/model/enumerations/choice.model';
import { InterviewService } from 'app/entities/interview/interview.service';
type SelectableEntity = IInterview;
@Component({
  selector: 'jhi-affectation-confirmation-dialog',
  templateUrl: './affectation-confirmation-dialog.component.html',
  styleUrls: ['./affectation-confirmation-dialog.component.scss']
})
export class AffectationConfirmationDialogComponent implements OnInit {
  @Input()
  interview!: IInterview;
  isSaving = false;
  
  constructor(
    protected affectationService: AffectationService,
    protected interviewService: InterviewService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}
  ngOnInit(): void {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  private createFromForm(): IAffectation {
    const maDate=moment("2000-01-01");
    
    const today = moment().startOf('day');
    return {
      ...new Affectation(),
      internship: this.interview?.internship,
      dateAffectation : today,
      startDate :maDate,
      endDate : maDate,
    };
  }

  save(): void {
    this.isSaving = true;
    const affectation = this.createFromForm();
    this.subscribeToSaveResponse(this.affectationService.create(affectation));
  }

  confirmAffectation(interview: IInterview ): void {
    if (interview !== undefined){
      this.interviewService.accepteAffectation(Choice.ACCEPTED, interview.id).subscribe(() => {
        this.eventManager.broadcast('interviewListModification');
      });
    }
    this.isSaving = true;
    const affectation = this.createFromForm();
    this.affectationService.create(affectation).subscribe(() => {
      this.eventManager.broadcast('AffectationListModification');
      this.activeModal.close();
    });
    
  }
  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.activeModal.dismiss();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAffectation>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }


}
