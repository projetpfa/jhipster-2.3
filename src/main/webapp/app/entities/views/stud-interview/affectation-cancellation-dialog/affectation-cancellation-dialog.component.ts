import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import { IInterview } from 'app/shared/model/interview.model';
import { InterviewService } from 'app/entities/interview/interview.service';
import { Choice } from 'app/shared/model/enumerations/choice.model';

@Component({
  selector: 'jhi-affectation-cancellation-dialog',
  templateUrl: './affectation-cancellation-dialog.component.html',
  styleUrls: ['./affectation-cancellation-dialog.component.scss']
})
export class AffectationCancellationDialogComponent implements OnInit {

  @Input()
  interview: IInterview | undefined;
  isSaving = false;
  constructor(
    protected interviewService: InterviewService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager,
    protected dataUtils: JhiDataUtils,
  ) { }

  ngOnInit(): void {
  }

  cancel(): void {
    this.activeModal.dismiss();
  }

  annuler(interview: IInterview): void {
     if (interview !== undefined) {
      this.interviewService.refuseAffectation(Choice.REFUSED, interview.id).subscribe(() => {
        this.eventManager.broadcast('interviewListModification');
        this.activeModal.close();
      });
    } 
  }
  

  
  


}
