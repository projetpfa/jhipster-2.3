import { Component, OnInit, Input } from '@angular/core';
import { IInterview } from 'app/shared/model/interview.model';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { AffectationService } from 'app/entities/affectation/affectation.service';
import { IAffectation } from 'app/shared/model/affectation.model';
import * as moment from 'moment';
type SelectableEntity = IInterview;
@Component({
  selector: 'jhi-interview-details',
  templateUrl: './interview-details.component.html',
  styleUrls: ['./interview-details.component.scss']
})

export class  InterviewDetailsComponent implements OnInit {
  @Input()
  interview: IInterview | undefined;
  isSaving = false;
  constructor(
    protected affectationService: AffectationService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager,
    protected dataUtils: JhiDataUtils,
  ) {}
  ngOnInit(): void {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.activeModal.dismiss();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAffectation>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }
  DatetoString(disponibility: any): string {
    return moment(disponibility)
      .format('DD-MM-YYYY HH:mm:ss')
      .toString();
  }


}
