import { Component, OnInit, OnDestroy } from '@angular/core';
import { IInvitationInterview } from 'app/shared/model/invitation-interview.model';
import { Subscription } from 'rxjs';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { InvitationInterviewService } from 'app/entities/invitation-interview/invitation-interview.service';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'jhi-list-interviews',
  templateUrl: './list-interviews.component.html',
  styleUrls: ['./list-interviews.component.scss']
})
export class ListInterviewsComponent implements OnInit, OnDestroy {
  invitationInterviews?: IInvitationInterview[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  predicate!: string;
  ascending!: boolean;
  numberIterations = 5;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  ngbPaginationPage = 1;

  constructor(
    protected invitationInterviewService: InvitationInterviewService,
    protected activatedRoute: ActivatedRoute,
    protected dataUtils: JhiDataUtils,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadPageInterview(page?: number): void {
    const pageToLoad: number = page || this.page;

    this.invitationInterviewService
      .getInvitationInterviewsByCompanyLoginAndConfirmation({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IInvitationInterview[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.ngbPaginationPage = data.pagingParams.page;
      this.loadPageInterview();
    });
    this.registerChangeInInvitationInterviews();
  }
  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackInvitationInterviewId(index: number, item: IInvitationInterview): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    return this.dataUtils.openFile(contentType, base64String);
  }

  registerChangeInInvitationInterviews(): void {
    this.eventSubscriber = this.eventManager.subscribe('interviewListModification', () => this.loadPageInterview());
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: IInvitationInterview[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/views/list-confirmation-interview'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.invitationInterviews = data || [];
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }
}
