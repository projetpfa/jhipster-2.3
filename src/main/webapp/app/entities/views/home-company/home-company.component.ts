import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpHeaders } from '@angular/common/http';
import { IApplication } from 'app/shared/model/application.model';
import { Subscription } from 'rxjs';
import { ApplicationService } from 'app/entities/application/application.service';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { IInvitationInterview } from 'app/shared/model/invitation-interview.model';
import { InvitationInterviewService } from 'app/entities/invitation-interview/invitation-interview.service';
import { InternshipService } from 'app/entities/internship/internship.service';
import { AffectationService } from 'app/entities/affectation/affectation.service';

@Component({
  selector: 'jhi-home-company',
  templateUrl: './home-company.component.html',
  styleUrls: ['./home-company.component.scss']
})
export class HomeCompanyComponent implements OnInit {
  invitationInterviews?: IInvitationInterview[];
  applications?: IApplication[];
  nbreOfapplications?: number;
  nbreOfInternships?: number;
  nbreOfAffectations?: number;
  eventSubscriber?: Subscription;
  totalItems = 0;
  predicate!: string;
  ascending!: boolean;
  numberIterations = 5;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  ngbPaginationPage = 1;
  date = Date.now();

  constructor(
    protected invitationInterviewService: InvitationInterviewService,
    protected applicationService: ApplicationService,
    protected internshipService: InternshipService,
    protected affectationService: AffectationService,
    protected activatedRoute: ActivatedRoute,
    protected dataUtils: JhiDataUtils,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadPage(page?: number): void {
    const pageToLoad: number = page || this.page;

    this.invitationInterviewService
      .getInvitationInterviewsByCompanyLoginAndConfirmation({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IInvitationInterview[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
        () => this.onError()
      );

    this.applicationService
      .getApplicationsByCompanyLogin({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IApplication[]>) => this.onSuccessApplication(res.body, res.headers, pageToLoad),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.ngbPaginationPage = data.pagingParams.page;
      this.loadPage();
    });
    
    this.registerChangeInApplications();

    this.applicationService.getNumberOfapplicationByCompanyLogin().subscribe((nbreOfapplications: number) => {
      this.nbreOfapplications = nbreOfapplications;
    });

    this.internshipService.getNumberOfInternshipByCompanyLogin().subscribe((nbreOfInternships: number) => {
      this.nbreOfInternships = nbreOfInternships;
    });

    this.affectationService.getNumberOfapplicationByCompanyLogin().subscribe((nbreOfAffectations: number) => {
      this.nbreOfAffectations = nbreOfAffectations;
    });
  }

  trackId(index: number, item: IApplication): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }
  trackInvitationInterviewId(index: number, item: IInvitationInterview): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    return this.dataUtils.openFile(contentType, base64String);
  }

  registerChangeInApplications(): void {
    this.eventSubscriber = this.eventManager.subscribe('applicationListModification', () => this.loadPage());
  }
  registerChangeInInvitationInterviews(): void {
    this.eventSubscriber = this.eventManager.subscribe('interviewListModification', () => this.loadPage());
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: IInvitationInterview[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/views/home-company'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.invitationInterviews = data || [];
  }

  protected onSuccessApplication(data: IApplication[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/views/home-company'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.applications = data || [];
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }
}
