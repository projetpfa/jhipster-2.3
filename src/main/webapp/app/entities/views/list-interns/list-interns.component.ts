import { Component, OnInit, OnDestroy } from '@angular/core';
import { IAffectation } from 'app/shared/model/affectation.model';
import { Subscription } from 'rxjs';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { IInternship } from 'app/shared/model/internship.model';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse, HttpHeaders } from '@angular/common/http';
import { AffectationService } from 'app/entities/affectation/affectation.service';
import { EvaluationsComponent } from '../evaluations/evaluations.component';
import { InternshipPeriodComponent } from './internship-period/internship-period.component';
import * as moment from 'moment';

@Component({
  selector: 'jhi-list-interns',
  templateUrl: './list-interns.component.html',
  styleUrls: ['./list-interns.component.scss']
})
export class ListInternsComponent implements OnInit, OnDestroy {
  affectations!: IAffectation[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  internshipPATH?: IInternship;

  maDate= moment("2000-01-01");
  today = moment().startOf('day');

  affectation: IAffectation | null = null;

  constructor(
    protected affectationService: AffectationService,
    protected activatedRoute: ActivatedRoute,
    protected dataUtils: JhiDataUtils,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadPage(page?: number): void {
    const pageToLoad: number = page || this.page;

    this.affectationService
      .getAffectationByInternship(this.internshipPATH, {
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IAffectation[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ internship }) => (this.internshipPATH = internship));
    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.ngbPaginationPage = data.pagingParams.page;

      this.loadPage();
    });

    this.registerChangeInAffectations();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(item: IAffectation): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    return this.dataUtils.openFile(contentType, base64String);
  }

  registerChangeInAffectations(): void {
    this.eventSubscriber = this.eventManager.subscribe('affectationListModification', () => this.loadPage());
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: IAffectation[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/views', this.internshipPATH?.id, 'listeInterns'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.affectations = data || [];
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }
  evaluate(affectation: IAffectation): void {
    const modalRef = this.modalService.open(EvaluationsComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.student = affectation.student;
    modalRef.componentInstance.affectation = affectation;
    modalRef.componentInstance.evaluatingCompany = affectation.internship?.company;
  }

  
  period(affectation: IAffectation): void {
    const modalRef = this.modalService.open(InternshipPeriodComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.affectation= affectation;
  }

  getDate(affectation: IAffectation):boolean {
    if (affectation.endDate!== undefined){
      const d1=affectation.endDate?.toDate()
      const d2=this.maDate.toDate()
      const same = d1.getTime() === d2.getTime();
    
      if(same){
      return true}
    else  {
      return false
    }
  }else return false

    }
    

  DatetoString(disponibility: any): string {
    return moment(disponibility)
      .format('DD/MM/YYYY ')
      .toString();
  }

  
    
}
