import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IAffectation, Affectation } from 'app/shared/model/affectation.model';
import { AffectationService } from 'app/entities/affectation/affectation.service';
import { JhiEventManager } from 'ng-jhipster';
import { FormBuilder, Validators } from '@angular/forms';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IStudent } from 'app/shared/model/student.model';
import { IInternship } from 'app/shared/model/internship.model';
import * as moment from 'moment';
type SelectableEntity = IStudent | IInternship;
@Component({
  selector: 'jhi-internship-period',
  templateUrl: './internship-period.component.html',
  styleUrls: ['./internship-period.component.scss']
})
export class InternshipPeriodComponent implements OnInit {
  
  isSaving = false;
  @Input()
  affectation!: IAffectation;
  editForm = this.fb.group({
    id: [],
    dateAffectation: [null, [Validators.required]],
    startDate: [null, [Validators.required]],
    endDate: [null, [Validators.required]],
    student: [],
    internship: []
  });
 
  constructor(
    public activeModal: NgbActiveModal,
    public affectationService:AffectationService,
    protected eventManager: JhiEventManager,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {}
  
  cancel(): void {
    this.activeModal.dismiss();
  }

 choosePeriod(affectation1:IAffectation): void { 
    const affectation = this.createFromForm(affectation1);
    
    this.subscribeToSaveResponse(this.affectationService.fixePeriod(affectation) );
    this.eventManager.broadcast('affectationListModification');
 }
  
 createFromForm(affectation:IAffectation): IAffectation {
    return {
      ...new Affectation(),
      id: affectation.id,
      
      dateAffectation:affectation?.dateAffectation,
      startDate: this.editForm.get(['startDate'])!.value ? moment(this.editForm.get(['startDate'])!.value, DATE_TIME_FORMAT) : undefined,
      endDate: this.editForm.get(['endDate'])!.value ? moment(this.editForm.get(['endDate'])!.value, DATE_TIME_FORMAT) : undefined,
      student: affectation.student,
      internship: affectation.internship
    };
  }




 protected subscribeToSaveResponse(result: Observable<HttpResponse<IAffectation>>): void {
  result.subscribe(
    () => this.onSaveSuccess(),
    () => this.onSaveError()
  );
  }

  previousState(): void {
    window.history.back();
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.activeModal.dismiss();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}



