import { Component, OnInit, OnDestroy } from '@angular/core';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { Subscription } from 'rxjs';
import { InternshipService } from 'app/entities/internship/internship.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse, HttpHeaders } from '@angular/common/http';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApplicationService } from 'app/entities/application/application.service';
import { IInternship } from 'app/shared/model/internship.model';
import { InternshipDeleteDialogComponent } from 'app/entities/internship/internship-delete-dialog.component';

@Component({
  selector: 'jhi-list-number-application-per-internship',
  templateUrl: './list-number-application-per-internship.component.html',
  styleUrls: ['./list-number-application-per-internship.component.scss']
})
export class ListNumberApplicationPerInternshipComponent implements OnInit, OnDestroy {
  list?: Object[];

  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;

  constructor(
    protected intershipService: InternshipService,
    protected applicationService: ApplicationService,
    protected activatedRoute: ActivatedRoute,
    protected dataUtils: JhiDataUtils,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}




  loadPage(page?: number): void {
    const pageToLoad: number = page || this.page;

    this.applicationService
      .getNumberOfApplicationByInternship({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IInternship[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.ngbPaginationPage = data.pagingParams.page;
      this.loadPage();
    });
    this.registerChangeInInternships();

  }
  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IInternship): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    return this.dataUtils.openFile(contentType, base64String);
  }

  registerChangeInInternships(): void {
    this.eventSubscriber = this.eventManager.subscribe('internshipListModification', () => this.loadPage());
  }

  delete(internship: IInternship): void {
    const modalRef = this.modalService.open(InternshipDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.internship = internship;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: IInternship[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/views/numbreDePostulationParOffre'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.list = data || [];

  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }
}
