import { Component, OnInit } from '@angular/core';
import { IDeadline } from 'app/shared/model/deadline.model';
import { Validators, FormBuilder } from '@angular/forms';
import { JhiDataUtils, JhiEventManager, JhiFileLoadError, JhiEventWithContent } from 'ng-jhipster';
import { InternshipService } from 'app/entities/internship/internship.service';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { IInternship, Internship } from 'app/shared/model/internship.model';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'jhi-create-internship',
  templateUrl: './create-internship.component.html'
})
export class CreateInternshipComponent implements OnInit {
  isSaving = false;


  editForm = this.fb.group({
    id: [],
    photo: [],
    photoContentType: [],
    entitled: [null, [Validators.required]],
    description: [null, [Validators.required]],
    duration: [null, [Validators.required]],
    level: [null, [Validators.required]],
    domain: [null, [Validators.required]],
    brochure: [],
    brochureContentType: [],
    deadline: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected internshipService: InternshipService,
       protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ internship }) => {
      this.updateForm(internship);


    });
  }

  updateForm(internship: IInternship): void {
    this.editForm.patchValue({
      id: internship.id,
      photo: internship.photo,
      photoContentType: internship.photoContentType,
      entitled: internship.entitled,
      description: internship.description,
      duration: internship.duration,
      level: internship.level,
      domain: internship.domain,
      brochure: internship.brochure,
      brochureContentType: internship.brochureContentType,

    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
      this.eventManager.broadcast(
        new JhiEventWithContent<AlertError>('enisStagePlatformeApp.error', { ...err, key: 'error.file.' + err.key })
      );
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const internship = this.createFromForm();
    if (internship.id !== undefined) {
      this.subscribeToSaveResponse(this.internshipService.update(internship));
    } else {
      this.subscribeToSaveResponse(this.internshipService.create(internship));
    }
  }

  private createFromForm(): IInternship {
    return {
      ...new Internship(),
      id: this.editForm.get(['id'])!.value,
      photoContentType: this.editForm.get(['photoContentType'])!.value,
      photo: this.editForm.get(['photo'])!.value,
      entitled: this.editForm.get(['entitled'])!.value,
      description: this.editForm.get(['description'])!.value,
      duration: this.editForm.get(['duration'])!.value,
      level: this.editForm.get(['level'])!.value,
      domain: this.editForm.get(['domain'])!.value,
      brochureContentType: this.editForm.get(['brochureContentType'])!.value,
      brochure: this.editForm.get(['brochure'])!.value

    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IInternship>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IDeadline): any {
    return item.id;
  }
}
