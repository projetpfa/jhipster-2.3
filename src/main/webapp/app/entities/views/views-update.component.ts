import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IViews, Views } from 'app/shared/model/views.model';
import { ViewsService } from './views.service';

@Component({
  selector: 'jhi-views-update',
  templateUrl: './views-update.component.html'
})
export class ViewsUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: []
  });

  constructor(protected viewsService: ViewsService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ views }) => {
      this.updateForm(views);
    });
  }

  updateForm(views: IViews): void {
    this.editForm.patchValue({
      id: views.id
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const views = this.createFromForm();
    if (views.id !== undefined) {
      this.subscribeToSaveResponse(this.viewsService.update(views));
    } else {
      this.subscribeToSaveResponse(this.viewsService.create(views));
    }
  }

  private createFromForm(): IViews {
    return {
      ...new Views(),
      id: this.editForm.get(['id'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IViews>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
