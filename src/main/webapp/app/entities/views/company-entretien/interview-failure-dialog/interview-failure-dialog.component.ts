import { Component, OnInit, Input } from '@angular/core';
import { IInterview } from 'app/shared/model/interview.model';
import { InterviewService } from 'app/entities/interview/interview.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import { STATE } from 'app/shared/model/enumerations/state.model';

@Component({
  selector: 'jhi-interview-failure-dialog',
  templateUrl: './interview-failure-dialog.component.html',
  styleUrls: ['./interview-failure-dialog.component.scss']
})
export class InterviewFailureDialogComponent implements  OnInit {

  @Input()
  interview: IInterview | undefined;
  isSaving = false;
  constructor(
    protected interviewService: InterviewService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager,
    protected dataUtils: JhiDataUtils,
  ) { }

  ngOnInit(): void {
  }

  cancel(): void {
    this.activeModal.dismiss();
  }

  failure(interview: IInterview): void {
     if (interview !== undefined) {
      this.interviewService.failureInterview(STATE.FAILURE, interview.id).subscribe(() => {
        this.eventManager.broadcast('interviewListModification');
        this.activeModal.close();
      });
    }
  }

}
