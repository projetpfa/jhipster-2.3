import { InterviewFailureDialogComponent } from './interview-failure-dialog/interview-failure-dialog.component';
import { InterviewSuccessDialogComponent } from './interview-success-dialog/interview-success-dialog.component';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { IInterview } from 'app/shared/model/interview.model';
import { Subscription } from 'rxjs';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { InterviewService } from 'app/entities/interview/interview.service';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse, HttpHeaders } from '@angular/common/http';
import { InterviewDetailsComponent } from '../stud-interview/interview-details/interview-details.component';

@Component({
  selector: 'jhi-company-entretien',
  templateUrl: './company-entretien.component.html'
})
export class CompanyEntretienComponent implements OnInit, OnDestroy {
  interviews?: IInterview[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  searchText?: any;
  interview!: IInterview;

  constructor(
    protected interviewService: InterviewService,
    protected activatedRoute: ActivatedRoute,
    protected dataUtils: JhiDataUtils,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadPage(page?: number): void {
    const pageToLoad: number = page || this.page;
    this.interviewService
      .getStudentsResponseByInternship({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IInterview[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.ngbPaginationPage = data.pagingParams.page;
      this.loadPage();
    });
    this.registerChangeInInterviews();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  registerChangeInInterviews(): void {
    this.eventSubscriber = this.eventManager.subscribe('interviewListModification', () => this.loadPage());
  }

   sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }
  trackId(index: number, item: IInterview): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }
  protected onSuccess(data: IInterview[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/views/company-interview'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.interviews = data || [];
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }


  detail(interview: IInterview): void {
    const modalRef = this.modalService.open( InterviewDetailsComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.interview = interview;
  }

  successInterview(interview: IInterview): void {
    const modalRef = this.modalService.open( InterviewSuccessDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.interview = interview;
  }
  failureInterview(interview: IInterview): void {
    const modalRef = this.modalService.open(InterviewFailureDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.interview= interview;
  }
}
