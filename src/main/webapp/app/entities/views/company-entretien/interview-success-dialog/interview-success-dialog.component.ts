import { Component, OnInit, Input } from '@angular/core';
import { IInterview } from 'app/shared/model/interview.model';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { InterviewService } from 'app/entities/interview/interview.service';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import { STATE } from 'app/shared/model/enumerations/state.model';

@Component({
  selector: 'jhi-interview-success-dialog',
  templateUrl: './interview-success-dialog.component.html',
  styleUrls: ['./interview-success-dialog.component.scss']
})
export class InterviewSuccessDialogComponent implements  OnInit {

  @Input()
  interview: IInterview | undefined;

  isSaving = false;
  constructor(
    protected interviewService: InterviewService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager,
    protected dataUtils: JhiDataUtils,
  ) { }

  ngOnInit(): void {
  }

  cancel(): void {
    this.activeModal.dismiss();
  }

  success(interview: IInterview): void {
     if (interview !== undefined) {
      this.interviewService.successInterview(STATE.SUCCESS, interview.id).subscribe(() => {
        this.eventManager.broadcast('interviewListModification');
        this.activeModal.close();
      });
    }
  }

}
