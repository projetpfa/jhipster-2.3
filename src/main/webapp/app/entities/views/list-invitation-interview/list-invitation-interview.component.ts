import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IInvitationInterview } from 'app/shared/model/invitation-interview.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { InvitationInterviewService } from '../../invitation-interview/invitation-interview.service';
import { InvitationInterviewDeleteDialogComponent } from '../../invitation-interview/invitation-interview-delete-dialog.component';
import { InvitationInterviewConfirmationComponent } from '../invitation-interview-confirmation/invitation-interview-confirmation.component';
import { InvitationInterviewCancellationComponent } from '../invitation-interview-cancellation/invitation-interview-cancellation.component';

@Component({
  selector: 'jhi-list-invitation-interview',
  templateUrl: './list-invitation-interview.component.html',
  styleUrls: ['./list-invitation-interview.component.scss']
})
export class ListInvitationInterviewComponent implements OnInit, OnDestroy {
  invitationInterviews?: IInvitationInterview[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  invitationInterview: IInvitationInterview | null = null;
  constructor(
    protected invitationInterviewService: InvitationInterviewService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected dataUtils: JhiDataUtils
  ) {}

  loadPage(page?: number): void {
    const pageToLoad: number = page || this.page;

    this.invitationInterviewService
      .getInvitationInterviewsByStudentLogin({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IInvitationInterview[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.ngbPaginationPage = data.pagingParams.page;
      this.loadPage();
    });
    this.registerChangeInInvitationInterviews();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IInvitationInterview): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInInvitationInterviews(): void {
    this.eventSubscriber = this.eventManager.subscribe('invitationInterviewListModification', () => this.loadPage());
  }

  delete(invitationInterview: IInvitationInterview): void {
    const modalRef = this.modalService.open(InvitationInterviewDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.invitationInterview = invitationInterview;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: IInvitationInterview[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/views/listInvitationInterview'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.invitationInterviews = data || [];
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }
  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    return this.dataUtils.openFile(contentType, base64String);
  }
  confirmer(invitationInterview: IInvitationInterview): void {
    const modalRef = this.modalService.open(InvitationInterviewConfirmationComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.invitationInterview = invitationInterview;
  }
  annuler(invitationInterview: IInvitationInterview): void {
    const modalRef = this.modalService.open(InvitationInterviewCancellationComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.invitationInterview = invitationInterview;
  }
}
