import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EnisIntershipSharedModule } from 'app/shared/shared.module';
import { ViewsComponent } from './views.component';
import { ViewsDetailComponent } from './views-detail.component';
import { ViewsUpdateComponent } from './views-update.component';
import { ViewsDeleteDialogComponent } from './views-delete-dialog.component';
import { viewsRoute } from './views.route';
import { StudInternshipComponent } from './stud-internship/stud-internship.component';
import { ListInternshipCompanyComponent } from './list-internship-company/list-internship-company.component';
import { DetailsInternshipComponent } from './details-internship/details-internship.component';
import { ApplicationInternshipComponent } from './application-internship/application-internship.component';
import { CreateInternshipComponent } from './create-internship/create-internship.component';
import { ListCondidateComponent } from './list-condidate/list-condidate.component';
import { ProfilesocietyComponent } from './profilesociety/profilesociety.component';
import { StudentProfileComponent } from './student-profile/student-profile.component';
import { HomeStudentComponent } from './home-student/home-student.component';
import { HomeCompanyComponent } from './home-company/home-company.component';
import { EvaluationsComponent } from './evaluations/evaluations.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { ListInvitationInterviewComponent } from './list-invitation-interview/list-invitation-interview.component';
import { InvitationInterviewConfirmationComponent } from './invitation-interview-confirmation/invitation-interview-confirmation.component';
import { InvitationInterviewCancellationComponent } from './invitation-interview-cancellation/invitation-interview-cancellation.component';
import { LoginCompanyComponent } from './login-company/login-company.component';
import { InvitationDialogComponent } from './invitation-dialog/invitation-dialog.component';
import { ListApplicationComponent } from './list-application/list-application.component';
import { ListInterviewsComponent } from './list-interviews/list-interviews.component';
import { ListInternsComponent } from './list-interns/list-interns.component';
import { ListNumberApplicationPerInternshipComponent } from './list-number-application-per-internship/list-number-application-per-internship.component';
import { StudInterviewComponent } from './stud-interview/stud-interview.component';
import { AffectationConfirmationDialogComponent } from './stud-interview/affectation-confirmation-dialog/affectation-confirmation-dialog.component';
import { AffectationCancellationDialogComponent } from './stud-interview/affectation-cancellation-dialog/affectation-cancellation-dialog.component';
import { EvalEntretienComponent } from './eval-entretien/eval-entretien.component';
import { CompanyEntretienComponent } from './company-entretien/company-entretien.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { EditCompanyComponent } from './edit-company/edit-company.component';
import { EditStudComponent } from './edit-stud/edit-stud.component';
import { ListNumberAffectationPerInternshipComponent } from './list-number-affectation-per-internship/list-number-affectation-per-internship.component';
import { ListAffectationsComponent } from './list-affectations/list-affectations.component';
import { InterviewDetailsComponent } from './stud-interview/interview-details/interview-details.component';
import { InterviewSuccessDialogComponent } from './company-entretien/interview-success-dialog/interview-success-dialog.component';
import { InterviewFailureDialogComponent } from './company-entretien/interview-failure-dialog/interview-failure-dialog.component';
import { LoginStudentComponent } from './login-student/login-student.component';
import { InternshipPeriodComponent } from './list-interns/internship-period/internship-period.component';

@NgModule({
  imports: [EnisIntershipSharedModule, RouterModule.forChild(viewsRoute), Ng2SearchPipeModule],
  declarations: [
    ViewsComponent,
    ViewsDetailComponent,
    ViewsUpdateComponent,
    ViewsDeleteDialogComponent,
    StudInternshipComponent,
    ListInternshipCompanyComponent,
    DetailsInternshipComponent,
    ApplicationInternshipComponent,
    CreateInternshipComponent,
    ListCondidateComponent,
    ProfilesocietyComponent,
    StudentProfileComponent,
    HomeStudentComponent,
    HomeCompanyComponent,
    EvaluationsComponent,
    SideBarComponent,
    ListInvitationInterviewComponent,
    InvitationInterviewConfirmationComponent,
    InvitationInterviewCancellationComponent,
    LoginCompanyComponent,
    InvitationDialogComponent,
    ListApplicationComponent,
    ListInterviewsComponent,
    ListInternsComponent,
    ListNumberApplicationPerInternshipComponent,
    StudInterviewComponent,
    EvalEntretienComponent,
    CompanyEntretienComponent,
    EditCompanyComponent,
    EditStudComponent,
    ListNumberAffectationPerInternshipComponent,
    ListAffectationsComponent,

    AffectationConfirmationDialogComponent,
    AffectationCancellationDialogComponent,
    InterviewDetailsComponent,

    CompanyEntretienComponent,

    EvalEntretienComponent,

    InterviewSuccessDialogComponent,

    InterviewFailureDialogComponent,

    LoginStudentComponent,
    
    InternshipPeriodComponent
  ],
  entryComponents: [ViewsDeleteDialogComponent]
})
export class EnisIntershipViewsModule {}
