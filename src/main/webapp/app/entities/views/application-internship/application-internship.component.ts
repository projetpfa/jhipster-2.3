import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiDataUtils, JhiFileLoadError, JhiEventManager, JhiEventWithContent } from 'ng-jhipster';

import { IApplication, Application } from 'app/shared/model/application.model';
import { ApplicationService } from 'app/entities/application/application.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { IStudent } from 'app/shared/model/student.model';
import { StudentService } from 'app/entities/student/student.service';
import { IInternship } from 'app/shared/model/internship.model';
import { InternshipService } from 'app/entities/internship/internship.service';
import { IDeadline } from 'app/shared/model/deadline.model';
import { DeadlineService } from 'app/entities/deadline/deadline.service';

type SelectableEntity = IStudent | IInternship | IDeadline;
@Component({
  selector: 'jhi-application-internship',
  templateUrl: './application-internship.component.html',
  styleUrls: ['./application-internship.component.scss']
})
export class ApplicationInternshipComponent implements OnInit {
  isSaving = false;
  students: IStudent[] = [];
  internships: IInternship[] = [];
  deadlines: IDeadline[] = [];

  internshipPATH: IInternship | undefined;

  editForm = this.fb.group({
    cv: [null, [Validators.required]],
    cvContentType: [],
    firstDisponibility: [null, [Validators.required]],
    secondDisponibility: [null, [Validators.required]],
    thirdDisponibility: [null, [Validators.required]],
    student: [],
    internship: [],
    deadline: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected applicationService: ApplicationService,
    protected studentService: StudentService,
    protected internshipService: InternshipService,
    protected deadlineService: DeadlineService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ internship }) => (this.internshipPATH = internship));
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
      this.eventManager.broadcast(
        new JhiEventWithContent<AlertError>('enisIntershipApp.error', { ...err, key: 'error.file.' + err.key })
      );
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const application = this.createFromForm();
    this.subscribeToSaveResponse(this.applicationService.create(application));
  }

  private createFromForm(): IApplication {
    return {
      ...new Application(),

      cvContentType: this.editForm.get(['cvContentType'])!.value,
      cv: this.editForm.get(['cv'])!.value,
      firstDisponibility: this.editForm.get(['firstDisponibility'])!.value
        ? moment(this.editForm.get(['firstDisponibility'])!.value, DATE_TIME_FORMAT)
        : undefined,
      secondDisponibility: this.editForm.get(['secondDisponibility'])!.value
        ? moment(this.editForm.get(['secondDisponibility'])!.value, DATE_TIME_FORMAT)
        : undefined,
      thirdDisponibility: this.editForm.get(['thirdDisponibility'])!.value
        ? moment(this.editForm.get(['thirdDisponibility'])!.value, DATE_TIME_FORMAT)
        : undefined,

      internship: this.internshipPATH
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IApplication>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
