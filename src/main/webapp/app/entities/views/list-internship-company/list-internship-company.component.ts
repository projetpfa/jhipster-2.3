import { Component, OnInit, OnDestroy } from '@angular/core';
import { IInternship } from 'app/shared/model/internship.model';
import { Subscription } from 'rxjs';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { InternshipService } from 'app/entities/internship/internship.service';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse, HttpHeaders } from '@angular/common/http';
import { InternshipDeleteDialogComponent } from 'app/entities/internship/internship-delete-dialog.component';
import { ApplicationService } from 'app/entities/application/application.service';

@Component({
  selector: 'jhi-list-internship-company',
  templateUrl: './list-internship-company.component.html'
})
export class ListInternshipCompanyComponent implements OnInit, OnDestroy {
  internships?: IInternship[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;

  constructor(
    protected intershipService: InternshipService,
    protected applicationService: ApplicationService,
    protected activatedRoute: ActivatedRoute,
    protected dataUtils: JhiDataUtils,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadPage(page?: number): void {
    const pageToLoad: number = page || this.page;

    this.intershipService
      .getInternshipByCompanyLogin({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IInternship[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.descending;
      this.predicate = data.pagingParams.predicate;
      this.ngbPaginationPage = data.pagingParams.page;
      this.loadPage();
    });
    this.registerChangeInInternships();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IInternship): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    return this.dataUtils.openFile(contentType, base64String);
  }

  registerChangeInInternships(): void {
    this.eventSubscriber = this.eventManager.subscribe('internshipListModification', () => this.loadPage());
  }

  delete(internship: IInternship): void {
    const modalRef = this.modalService.open(InternshipDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.internship = internship;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: IInternship[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/views/listeOffres'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.internships = data || [];

  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }
}
