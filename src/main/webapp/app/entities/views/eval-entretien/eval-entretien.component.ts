import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';
import { IInterview, Interview } from 'app/shared/model/interview.model';

import { ICompany } from 'app/shared/model/company.model';
import { CompanyService } from 'app/entities/company/company.service';
import { IStudent } from 'app/shared/model/student.model';
import { StudentService } from 'app/entities/student/student.service';
import { IInternship } from 'app/shared/model/internship.model';
import { InternshipService } from 'app/entities/internship/internship.service';

import { InterviewService } from 'app/entities/interview/interview.service';
import { InvitationInterview, IInvitationInterview } from 'app/shared/model/invitation-interview.model';
import { InvitationInterviewService } from 'app/entities/invitation-interview/invitation-interview.service';
import { JhiDataUtils } from 'ng-jhipster';
import { Choice } from 'app/shared/model/enumerations/choice.model';
import { STATE } from 'app/shared/model/enumerations/state.model';
type SelectableEntity = ICompany | IStudent | IInternship;

@Component({
  selector: 'jhi-eval-entretien',
  templateUrl: './eval-entretien.component.html',

})
export class EvalEntretienComponent implements OnInit {

  interviews?: IInterview[];

  isSaving = false;

  companies: ICompany[] = [];

  student: IStudent| null=null;
  company: ICompany | undefined;

  internships: IInternship[] = [];
  students: IStudent[] = [];
  invitationInterview?: InvitationInterview ;
  invitationinterviews: IInvitationInterview[] = [];


  editForm = this.fb.group({
    id: [],
    date: [null, [Validators.required]],
    presence: [null, [Validators.required]],

    communication: [null, [Validators.required]],
    scoreCommunication: [null, [Validators.required]],
    technical: [null, [Validators.required]],
    scoretTechnical: [null, [Validators.required]],
    other: [],
    studFinalChoice: [],
    invitationInterview: [],
    company: [],
    student: [],
    internship: []
  });


  constructor(
    protected interviewService: InterviewService,
    protected studentService: StudentService,
    protected companyService: CompanyService,
     protected internshipService: InternshipService,

    protected activatedRoute: ActivatedRoute,
    protected dataUtils: JhiDataUtils,
    protected invitationInterviewService: InvitationInterviewService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({  invitationInterview }) => (this. invitationInterview =  invitationInterview));

    this.activatedRoute.data.subscribe(({ interview }) => {
      if (!interview.id) {
        const today = moment().startOf('day');
        interview.date = today;
      }

      this.invitationInterviewService
        .query({ filter: 'interview-is-null' })
        .pipe(
          map((res: HttpResponse<IInvitationInterview[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IInvitationInterview[]) => {
          if (!interview.invitationInterview || !interview.invitationInterview.id) {
            this.invitationinterviews = resBody;
          } else {
            this.invitationInterviewService
              .find(interview.invitationInterview.id)
              .pipe(
                map((subRes: HttpResponse<IInvitationInterview>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IInvitationInterview[]) => (this.invitationinterviews = concatRes));
          }
        });

      this.companyService.query().subscribe((res: HttpResponse<ICompany[]>) => (this.companies = res.body || []));

      this.studentService.query().subscribe((res: HttpResponse<IStudent[]>) => (this.students = res.body || []));

      this.internshipService.query().subscribe((res: HttpResponse<IInternship[]>) => (this.internships = res.body || []));
    });
  }


  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const interview = this.createFromForm();
    this.subscribeToSaveResponse(this.interviewService.create(interview));


  }
  private createFromForm(): IInterview {
    return {
      ...new Interview(),
      id: this.editForm.get(['id'])!.value,
      date: this.editForm.get(['date'])!.value ? moment(this.editForm.get(['date'])!.value, DATE_TIME_FORMAT) : undefined,
      presence: this.editForm.get(['presence'])!.value,
      state: STATE.NONE,
      communication: this.editForm.get(['communication'])!.value,
      scoreCommunication: this.editForm.get(['scoreCommunication'])!.value,
      technical: this.editForm.get(['technical'])!.value,
      scoretTechnical: this.editForm.get(['scoretTechnical'])!.value,
      other: this.editForm.get(['other'])!.value,
      studFinalChoice: Choice.NONE,
      company:this.invitationInterview?.company,
      student: this.invitationInterview?.application?.student,
      internship: this.invitationInterview?.application?.internship,
      invitationInterview: this.invitationInterview

    };
  }


  protected subscribeToSaveResponse(result: Observable<HttpResponse<IInterview>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }
  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }


  protected onSaveError(): void {
    this.isSaving = false;
  }



  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }

}
