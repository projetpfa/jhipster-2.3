import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IInterview, Interview } from 'app/shared/model/interview.model';
import { InterviewService } from './interview.service';
import { IInvitationInterview } from 'app/shared/model/invitation-interview.model';
import { InvitationInterviewService } from 'app/entities/invitation-interview/invitation-interview.service';
import { ICompany } from 'app/shared/model/company.model';
import { CompanyService } from 'app/entities/company/company.service';
import { IStudent } from 'app/shared/model/student.model';
import { StudentService } from 'app/entities/student/student.service';
import { IInternship } from 'app/shared/model/internship.model';
import { InternshipService } from 'app/entities/internship/internship.service';

type SelectableEntity = IInvitationInterview | ICompany | IStudent | IInternship;

@Component({
  selector: 'jhi-interview-update',
  templateUrl: './interview-update.component.html'
})
export class InterviewUpdateComponent implements OnInit {
  isSaving = false;
  invitationinterviews: IInvitationInterview[] = [];
  companies: ICompany[] = [];
  students: IStudent[] = [];
  internships: IInternship[] = [];

  editForm = this.fb.group({
    id: [],
    date: [null, [Validators.required]],
    presence: [null, [Validators.required]],
    state: [null, [Validators.required]],
    communication: [null, [Validators.required]],
    scoreCommunication: [null, [Validators.required]],
    technical: [null, [Validators.required]],
    scoretTechnical: [null, [Validators.required]],
    other: [],
    studFinalChoice: [],
    invitationInterview: [],
    company: [],
    student: [],
    internship: []
  });

  constructor(
    protected interviewService: InterviewService,
    protected invitationInterviewService: InvitationInterviewService,
    protected companyService: CompanyService,
    protected studentService: StudentService,
    protected internshipService: InternshipService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ interview }) => {
      if (!interview.id) {
        const today = moment().startOf('day');
        interview.date = today;
      }

      this.updateForm(interview);

      this.invitationInterviewService
        .query({ filter: 'interview-is-null' })
        .pipe(
          map((res: HttpResponse<IInvitationInterview[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IInvitationInterview[]) => {
          if (!interview.invitationInterview || !interview.invitationInterview.id) {
            this.invitationinterviews = resBody;
          } else {
            this.invitationInterviewService
              .find(interview.invitationInterview.id)
              .pipe(
                map((subRes: HttpResponse<IInvitationInterview>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IInvitationInterview[]) => (this.invitationinterviews = concatRes));
          }
        });

      this.companyService.query().subscribe((res: HttpResponse<ICompany[]>) => (this.companies = res.body || []));

      this.studentService.query().subscribe((res: HttpResponse<IStudent[]>) => (this.students = res.body || []));

      this.internshipService.query().subscribe((res: HttpResponse<IInternship[]>) => (this.internships = res.body || []));
    });
  }

  updateForm(interview: IInterview): void {
    this.editForm.patchValue({
      id: interview.id,
      date: interview.date ? interview.date.format(DATE_TIME_FORMAT) : null,
      presence: interview.presence,
      state: interview.state,
      communication: interview.communication,
      scoreCommunication: interview.scoreCommunication,
      technical: interview.technical,
      scoretTechnical: interview.scoretTechnical,
      other: interview.other,
      studFinalChoice: interview.studFinalChoice,
      invitationInterview: interview.invitationInterview,
      company: interview.company,
      student: interview.student,
      internship: interview.internship
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const interview = this.createFromForm();
    if (interview.id !== undefined) {
      this.subscribeToSaveResponse(this.interviewService.update(interview));
    } else {
      this.subscribeToSaveResponse(this.interviewService.create(interview));
    }
  }

  private createFromForm(): IInterview {
    return {
      ...new Interview(),
      id: this.editForm.get(['id'])!.value,
      date: this.editForm.get(['date'])!.value ? moment(this.editForm.get(['date'])!.value, DATE_TIME_FORMAT) : undefined,
      presence: this.editForm.get(['presence'])!.value,
      state: this.editForm.get(['state'])!.value,
      communication: this.editForm.get(['communication'])!.value,
      scoreCommunication: this.editForm.get(['scoreCommunication'])!.value,
      technical: this.editForm.get(['technical'])!.value,
      scoretTechnical: this.editForm.get(['scoretTechnical'])!.value,
      other: this.editForm.get(['other'])!.value,
      studFinalChoice: this.editForm.get(['studFinalChoice'])!.value,
      invitationInterview: this.editForm.get(['invitationInterview'])!.value,
      company: this.editForm.get(['company'])!.value,
      student: this.editForm.get(['student'])!.value,
      internship: this.editForm.get(['internship'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IInterview>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
