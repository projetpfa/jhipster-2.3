import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IInterview } from 'app/shared/model/interview.model';
import { Choice } from 'app/shared/model/enumerations/choice.model';
import { STATE } from 'app/shared/model/enumerations/state.model';

type EntityResponseType = HttpResponse<IInterview>;
type EntityArrayResponseType = HttpResponse<IInterview[]>;

@Injectable({ providedIn: 'root' })
export class InterviewService {


  public resourceUrl = SERVER_API_URL + 'api/interviews';

  constructor(protected http: HttpClient) {}

  create(interview: IInterview): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(interview);
    return this.http
      .post<IInterview>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(interview: IInterview): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(interview);
    return this.http
      .put<IInterview>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IInterview>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IInterview[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  getInterviewsByCompanyLogin(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IInterview[]>(`${this.resourceUrl}/company`, { params: options, observe: 'response' });
  }

  getInterviewsByStudentLogin(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IInterview[]>(`${this.resourceUrl}/student`, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(interview: IInterview): IInterview {
    const copy: IInterview = Object.assign({}, interview, {
      date: interview.date && interview.date.isValid() ? interview.date.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.date = res.body.date ? moment(res.body.date) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((interview: IInterview) => {
        interview.date = interview.date ? moment(interview.date) : undefined;
      });
    }
    return res;
  }
  getStudentsResponseByInternship(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IInterview[]>('api/resultAffectation', { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  accepteAffectation(studFinalChoice: Choice, id: number | undefined): Observable<EntityResponseType> {
    return this.http
    .put<IInterview>(`${this.resourceUrl}/accepte/${id}`, studFinalChoice, { observe: 'response' })
    .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  refuseAffectation(studFinalChoice: Choice, id: number | undefined): Observable<EntityResponseType> {
    return this.http
    .put<IInterview>(`${this.resourceUrl}/refuse/${id}`, studFinalChoice, { observe: 'response' })
    .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }
  successInterview(state: STATE, id: number | undefined): Observable<EntityResponseType> {
    return this.http
    .put<IInterview>(`${this.resourceUrl}/successRESULT/${id}`, state, { observe: 'response' })
    .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  failureInterview(state: STATE, id: number|undefined): Observable<EntityResponseType> {

    return this.http
    .put<IInterview>(`${this.resourceUrl}/failureRESULT/${id}`, state, { observe: 'response' })
    .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

}
