import { IInternship } from './../../shared/model/internship.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IApplication } from 'app/shared/model/application.model';

type EntityResponseType = HttpResponse<IApplication>;
type EntityArrayResponseType = HttpResponse<IApplication[]>;

@Injectable({ providedIn: 'root' })
export class ApplicationService {
  public resourceUrl = SERVER_API_URL + 'api/applications';

  constructor(protected http: HttpClient) {}

  create(application: IApplication): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(application);
    return this.http
      .post<IApplication>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(application: IApplication): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(application);
    return this.http
      .put<IApplication>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IApplication>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IApplication[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getApplicationpByStudentLogin(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IApplication[]>(`${this.resourceUrl}/student`, { params: options, observe: 'response' });
  }

  protected convertDateFromClient(application: IApplication): IApplication {
    const copy: IApplication = Object.assign({}, application, {
      firstDisponibility:
        application.firstDisponibility && application.firstDisponibility.isValid() ? application.firstDisponibility.toJSON() : undefined,
      secondDisponibility:
        application.secondDisponibility && application.secondDisponibility.isValid() ? application.secondDisponibility.toJSON() : undefined,
      thirdDisponibility:
        application.thirdDisponibility && application.thirdDisponibility.isValid() ? application.thirdDisponibility.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.firstDisponibility = res.body.firstDisponibility ? moment(res.body.firstDisponibility) : undefined;
      res.body.secondDisponibility = res.body.secondDisponibility ? moment(res.body.secondDisponibility) : undefined;
      res.body.thirdDisponibility = res.body.thirdDisponibility ? moment(res.body.thirdDisponibility) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((application: IApplication) => {
        application.firstDisponibility = application.firstDisponibility ? moment(application.firstDisponibility) : undefined;
        application.secondDisponibility = application.secondDisponibility ? moment(application.secondDisponibility) : undefined;
        application.thirdDisponibility = application.thirdDisponibility ? moment(application.thirdDisponibility) : undefined;
      });
    }
    return res;
  }
  getApplicationByInternship(internship?: IInternship, req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IApplication[]>(`api/internship/applications/${internship?.id}`, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  getNumberOfApplicationByInternship(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IApplication[]>('/api/internship/applications/nbre', { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  getNumberOfapplicationByStudentLogin(): any {
    return this.http.get<Number>(`${this.resourceUrl}/nbreApplicationByStud`);
  }

  getApplicationsByCompanyLogin(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IApplication[]>(`${this.resourceUrl}/company`, { params: options, observe: 'response' });
  }

  getNumberOfapplicationByCompanyLogin(): any {
    return this.http.get<Number>(`${this.resourceUrl}/nbreApplicationByCompany`);
  }
}
