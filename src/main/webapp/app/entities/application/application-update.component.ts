import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiDataUtils, JhiFileLoadError, JhiEventManager, JhiEventWithContent } from 'ng-jhipster';

import { IApplication, Application } from 'app/shared/model/application.model';
import { ApplicationService } from './application.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { IStudent } from 'app/shared/model/student.model';
import { StudentService } from 'app/entities/student/student.service';
import { IInternship } from 'app/shared/model/internship.model';
import { InternshipService } from 'app/entities/internship/internship.service';

type SelectableEntity = IStudent | IInternship;

@Component({
  selector: 'jhi-application-update',
  templateUrl: './application-update.component.html'
})
export class ApplicationUpdateComponent implements OnInit {
  isSaving = false;
  students: IStudent[] = [];
  internships: IInternship[] = [];

  editForm = this.fb.group({
    id: [],
    cv: [null, [Validators.required]],
    cvContentType: [],
    firstDisponibility: [null, [Validators.required]],
    secondDisponibility: [null, [Validators.required]],
    thirdDisponibility: [null, [Validators.required]],
    student: [],
    internship: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected applicationService: ApplicationService,
    protected studentService: StudentService,
    protected internshipService: InternshipService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ application }) => {
      if (!application.id) {
        const today = moment().startOf('day');
        application.firstDisponibility = today;
        application.secondDisponibility = today;
        application.thirdDisponibility = today;
      }

      this.updateForm(application);

      this.studentService.query().subscribe((res: HttpResponse<IStudent[]>) => (this.students = res.body || []));

      this.internshipService.query().subscribe((res: HttpResponse<IInternship[]>) => (this.internships = res.body || []));
    });
  }

  updateForm(application: IApplication): void {
    this.editForm.patchValue({
      id: application.id,
      cv: application.cv,
      cvContentType: application.cvContentType,
      firstDisponibility: application.firstDisponibility ? application.firstDisponibility.format(DATE_TIME_FORMAT) : null,
      secondDisponibility: application.secondDisponibility ? application.secondDisponibility.format(DATE_TIME_FORMAT) : null,
      thirdDisponibility: application.thirdDisponibility ? application.thirdDisponibility.format(DATE_TIME_FORMAT) : null,
      student: application.student,
      internship: application.internship
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
      this.eventManager.broadcast(
        new JhiEventWithContent<AlertError>('enisIntershipApp.error', { ...err, key: 'error.file.' + err.key })
      );
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const application = this.createFromForm();
    if (application.id !== undefined) {
      this.subscribeToSaveResponse(this.applicationService.update(application));
    } else {
      this.subscribeToSaveResponse(this.applicationService.create(application));
    }
  }

  private createFromForm(): IApplication {
    return {
      ...new Application(),
      id: this.editForm.get(['id'])!.value,
      cvContentType: this.editForm.get(['cvContentType'])!.value,
      cv: this.editForm.get(['cv'])!.value,
      firstDisponibility: this.editForm.get(['firstDisponibility'])!.value
        ? moment(this.editForm.get(['firstDisponibility'])!.value, DATE_TIME_FORMAT)
        : undefined,
      secondDisponibility: this.editForm.get(['secondDisponibility'])!.value
        ? moment(this.editForm.get(['secondDisponibility'])!.value, DATE_TIME_FORMAT)
        : undefined,
      thirdDisponibility: this.editForm.get(['thirdDisponibility'])!.value
        ? moment(this.editForm.get(['thirdDisponibility'])!.value, DATE_TIME_FORMAT)
        : undefined,
      student: this.editForm.get(['student'])!.value,
      internship: this.editForm.get(['internship'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IApplication>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
