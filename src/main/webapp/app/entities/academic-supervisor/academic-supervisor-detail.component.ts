import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IAcademicSupervisor } from 'app/shared/model/academic-supervisor.model';

@Component({
  selector: 'jhi-academic-supervisor-detail',
  templateUrl: './academic-supervisor-detail.component.html'
})
export class AcademicSupervisorDetailComponent implements OnInit {
  academicSupervisor: IAcademicSupervisor | null = null;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ academicSupervisor }) => (this.academicSupervisor = academicSupervisor));
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  previousState(): void {
    window.history.back();
  }
}
