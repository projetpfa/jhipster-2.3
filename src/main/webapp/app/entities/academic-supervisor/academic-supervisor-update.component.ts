import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiDataUtils, JhiFileLoadError, JhiEventManager, JhiEventWithContent } from 'ng-jhipster';

import { IAcademicSupervisor, AcademicSupervisor } from 'app/shared/model/academic-supervisor.model';
import { AcademicSupervisorService } from './academic-supervisor.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IDepartment } from 'app/shared/model/department.model';
import { DepartmentService } from 'app/entities/department/department.service';

type SelectableEntity = IUser | IDepartment;

@Component({
  selector: 'jhi-academic-supervisor-update',
  templateUrl: './academic-supervisor-update.component.html'
})
export class AcademicSupervisorUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];
  departments: IDepartment[] = [];

  editForm = this.fb.group({
    id: [],
    photo: [],
    photoContentType: [],
    city: [null, [Validators.required]],
    country: [null, [Validators.required]],
    postalCode: [null, [Validators.required]],
    phoneNumber: [null, [Validators.required]],
    grade: [],
    user: [],
    department: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected academicSupervisorService: AcademicSupervisorService,
    protected userService: UserService,
    protected departmentService: DepartmentService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ academicSupervisor }) => {
      this.updateForm(academicSupervisor);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));

      this.departmentService.query().subscribe((res: HttpResponse<IDepartment[]>) => (this.departments = res.body || []));
    });
  }

  updateForm(academicSupervisor: IAcademicSupervisor): void {
    this.editForm.patchValue({
      id: academicSupervisor.id,
      photo: academicSupervisor.photo,
      photoContentType: academicSupervisor.photoContentType,
      city: academicSupervisor.city,
      country: academicSupervisor.country,
      postalCode: academicSupervisor.postalCode,
      phoneNumber: academicSupervisor.phoneNumber,
      grade: academicSupervisor.grade,
      user: academicSupervisor.user,
      department: academicSupervisor.department
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
      this.eventManager.broadcast(
        new JhiEventWithContent<AlertError>('enisIntershipApp.error', { ...err, key: 'error.file.' + err.key })
      );
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const academicSupervisor = this.createFromForm();
    if (academicSupervisor.id !== undefined) {
      this.subscribeToSaveResponse(this.academicSupervisorService.update(academicSupervisor));
    } else {
      this.subscribeToSaveResponse(this.academicSupervisorService.create(academicSupervisor));
    }
  }

  private createFromForm(): IAcademicSupervisor {
    return {
      ...new AcademicSupervisor(),
      id: this.editForm.get(['id'])!.value,
      photoContentType: this.editForm.get(['photoContentType'])!.value,
      photo: this.editForm.get(['photo'])!.value,
      city: this.editForm.get(['city'])!.value,
      country: this.editForm.get(['country'])!.value,
      postalCode: this.editForm.get(['postalCode'])!.value,
      phoneNumber: this.editForm.get(['phoneNumber'])!.value,
      grade: this.editForm.get(['grade'])!.value,
      user: this.editForm.get(['user'])!.value,
      department: this.editForm.get(['department'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAcademicSupervisor>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
