import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EnisIntershipSharedModule } from 'app/shared/shared.module';
import { AcademicSupervisorComponent } from './academic-supervisor.component';
import { AcademicSupervisorDetailComponent } from './academic-supervisor-detail.component';
import { AcademicSupervisorUpdateComponent } from './academic-supervisor-update.component';
import { AcademicSupervisorDeleteDialogComponent } from './academic-supervisor-delete-dialog.component';
import { academicSupervisorRoute } from './academic-supervisor.route';

@NgModule({
  imports: [EnisIntershipSharedModule, RouterModule.forChild(academicSupervisorRoute)],
  declarations: [
    AcademicSupervisorComponent,
    AcademicSupervisorDetailComponent,
    AcademicSupervisorUpdateComponent,
    AcademicSupervisorDeleteDialogComponent
  ],
  entryComponents: [AcademicSupervisorDeleteDialogComponent]
})
export class EnisIntershipAcademicSupervisorModule {}
