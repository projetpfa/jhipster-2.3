import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAcademicSupervisor } from 'app/shared/model/academic-supervisor.model';

type EntityResponseType = HttpResponse<IAcademicSupervisor>;
type EntityArrayResponseType = HttpResponse<IAcademicSupervisor[]>;

@Injectable({ providedIn: 'root' })
export class AcademicSupervisorService {
  public resourceUrl = SERVER_API_URL + 'api/academic-supervisors';

  constructor(protected http: HttpClient) {}

  create(academicSupervisor: IAcademicSupervisor): Observable<EntityResponseType> {
    return this.http.post<IAcademicSupervisor>(this.resourceUrl, academicSupervisor, { observe: 'response' });
  }

  update(academicSupervisor: IAcademicSupervisor): Observable<EntityResponseType> {
    return this.http.put<IAcademicSupervisor>(this.resourceUrl, academicSupervisor, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAcademicSupervisor>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAcademicSupervisor[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
