import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IAcademicSupervisor, AcademicSupervisor } from 'app/shared/model/academic-supervisor.model';
import { AcademicSupervisorService } from './academic-supervisor.service';
import { AcademicSupervisorComponent } from './academic-supervisor.component';
import { AcademicSupervisorDetailComponent } from './academic-supervisor-detail.component';
import { AcademicSupervisorUpdateComponent } from './academic-supervisor-update.component';

@Injectable({ providedIn: 'root' })
export class AcademicSupervisorResolve implements Resolve<IAcademicSupervisor> {
  constructor(private service: AcademicSupervisorService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAcademicSupervisor> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((academicSupervisor: HttpResponse<AcademicSupervisor>) => {
          if (academicSupervisor.body) {
            return of(academicSupervisor.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new AcademicSupervisor());
  }
}

export const academicSupervisorRoute: Routes = [
  {
    path: '',
    component: AcademicSupervisorComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.academicSupervisor.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: AcademicSupervisorDetailComponent,
    resolve: {
      academicSupervisor: AcademicSupervisorResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.academicSupervisor.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: AcademicSupervisorUpdateComponent,
    resolve: {
      academicSupervisor: AcademicSupervisorResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.academicSupervisor.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: AcademicSupervisorUpdateComponent,
    resolve: {
      academicSupervisor: AcademicSupervisorResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.academicSupervisor.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
