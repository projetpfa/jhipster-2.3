import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAcademicSupervisor } from 'app/shared/model/academic-supervisor.model';
import { AcademicSupervisorService } from './academic-supervisor.service';
import { AcademicSupervisorDeleteDialogComponent } from './academic-supervisor-delete-dialog.component';

@Component({
  selector: 'jhi-academic-supervisor',
  templateUrl: './academic-supervisor.component.html'
})
export class AcademicSupervisorComponent implements OnInit, OnDestroy {
  academicSupervisors?: IAcademicSupervisor[];
  eventSubscriber?: Subscription;

  constructor(
    protected academicSupervisorService: AcademicSupervisorService,
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.academicSupervisorService
      .query()
      .subscribe((res: HttpResponse<IAcademicSupervisor[]>) => (this.academicSupervisors = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInAcademicSupervisors();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAcademicSupervisor): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    return this.dataUtils.openFile(contentType, base64String);
  }

  registerChangeInAcademicSupervisors(): void {
    this.eventSubscriber = this.eventManager.subscribe('academicSupervisorListModification', () => this.loadAll());
  }

  delete(academicSupervisor: IAcademicSupervisor): void {
    const modalRef = this.modalService.open(AcademicSupervisorDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.academicSupervisor = academicSupervisor;
  }
}
