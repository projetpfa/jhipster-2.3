import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAcademicSupervisor } from 'app/shared/model/academic-supervisor.model';
import { AcademicSupervisorService } from './academic-supervisor.service';

@Component({
  templateUrl: './academic-supervisor-delete-dialog.component.html'
})
export class AcademicSupervisorDeleteDialogComponent {
  academicSupervisor?: IAcademicSupervisor;

  constructor(
    protected academicSupervisorService: AcademicSupervisorService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.academicSupervisorService.delete(id).subscribe(() => {
      this.eventManager.broadcast('academicSupervisorListModification');
      this.activeModal.close();
    });
  }
}
