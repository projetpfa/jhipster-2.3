import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiDataUtils, JhiFileLoadError, JhiEventManager, JhiEventWithContent } from 'ng-jhipster';

import { ICompany, Company } from 'app/shared/model/company.model';
import { CompanyService } from './company.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';

@Component({
  selector: 'jhi-company-update',
  templateUrl: './company-update.component.html'
})
export class CompanyUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];
  account!: Account;
  editForm = this.fb.group({
    id: [],
    photo: [],
    photoContentType: [],
    city: [null, [Validators.required]],
    country: [null, [Validators.required]],
    postalCode: [null, [Validators.required]],
    phoneNumber: [null, [Validators.required]],
    nameCompany: [null, [Validators.required]],
    description: [null, [Validators.required]],
    brochureCompany: [],
    brochureCompanyContentType: [],
    websiteCompany: [],
    user: [],
    email: [undefined, [Validators.required, Validators.minLength(5), Validators.maxLength(254), Validators.email]]

  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected companyService: CompanyService,
    protected userService: UserService,
    protected elementRef: ElementRef,
    private router: Router,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private accountService: AccountService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ company }) => {
      this.updateForm(company);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));
    });
    this.accountService.identity().subscribe(account => {
      if (account) {
        this.editForm.patchValue({

          email: account.email
    });
        this.account = account;
      }
    });
  }

  updateForm(company: ICompany): void {
    this.editForm.patchValue({
      id: company.id,
      photo: company.photo,
      photoContentType: company.photoContentType,
      city: company.city,
      country: company.country,
      postalCode: company.postalCode,
      phoneNumber: company.phoneNumber,
      nameCompany: company.nameCompany,
      description: company.description,
      brochureCompany: company.brochureCompany,
      brochureCompanyContentType: company.brochureCompanyContentType,
      websiteCompany: company.websiteCompany,
      user: company.user
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
      this.eventManager.broadcast(
        new JhiEventWithContent<AlertError>('enisIntershipApp.error', { ...err, key: 'error.file.' + err.key })
      );
    });
  }

  updateFormUser(user: IUser): void {
    this.editForm.patchValue({

      email: user.email
    });
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string): void {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const company = this.createFromForm();
    if (company.id !== undefined) {
      this.subscribeToSaveResponse(this.companyService.update(company));
    } else {
      this.subscribeToSaveResponse(this.companyService.create(company));
    }
    this.router.navigate(['/views/home-company']);
  }

  private createFromForm(): ICompany {
    return {
      ...new Company(),
      id: this.editForm.get(['id'])!.value,
      photoContentType: this.editForm.get(['photoContentType'])!.value,
      photo: this.editForm.get(['photo'])!.value,
      city: this.editForm.get(['city'])!.value,
      country: this.editForm.get(['country'])!.value,
      postalCode: this.editForm.get(['postalCode'])!.value,
      phoneNumber: this.editForm.get(['phoneNumber'])!.value,
      nameCompany: this.editForm.get(['nameCompany'])!.value,
      description: this.editForm.get(['description'])!.value,
      brochureCompanyContentType: this.editForm.get(['brochureCompanyContentType'])!.value,
      brochureCompany: this.editForm.get(['brochureCompany'])!.value,
      websiteCompany: this.editForm.get(['websiteCompany'])!.value,
      user: this.editForm.get(['user'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICompany>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IUser): any {
    return item.id;
  }
}
