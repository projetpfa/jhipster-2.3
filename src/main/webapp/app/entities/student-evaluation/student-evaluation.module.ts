import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EnisIntershipSharedModule } from 'app/shared/shared.module';
import { StudentEvaluationComponent } from './student-evaluation.component';
import { StudentEvaluationDetailComponent } from './student-evaluation-detail.component';
import { StudentEvaluationUpdateComponent } from './student-evaluation-update.component';
import { StudentEvaluationDeleteDialogComponent } from './student-evaluation-delete-dialog.component';
import { studentEvaluationRoute } from './student-evaluation.route';

@NgModule({
  imports: [EnisIntershipSharedModule, RouterModule.forChild(studentEvaluationRoute)],
  declarations: [
    StudentEvaluationComponent,
    StudentEvaluationDetailComponent,
    StudentEvaluationUpdateComponent,
    StudentEvaluationDeleteDialogComponent
  ],
  entryComponents: [StudentEvaluationDeleteDialogComponent]
})
export class EnisIntershipStudentEvaluationModule {}
