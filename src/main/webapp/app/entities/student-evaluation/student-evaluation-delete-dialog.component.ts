import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IStudentEvaluation } from 'app/shared/model/student-evaluation.model';
import { StudentEvaluationService } from './student-evaluation.service';

@Component({
  templateUrl: './student-evaluation-delete-dialog.component.html'
})
export class StudentEvaluationDeleteDialogComponent {
  studentEvaluation?: IStudentEvaluation;

  constructor(
    protected studentEvaluationService: StudentEvaluationService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.studentEvaluationService.delete(id).subscribe(() => {
      this.eventManager.broadcast('studentEvaluationListModification');
      this.activeModal.close();
    });
  }
}
