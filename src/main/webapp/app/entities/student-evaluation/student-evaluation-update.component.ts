import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IStudentEvaluation, StudentEvaluation } from 'app/shared/model/student-evaluation.model';
import { StudentEvaluationService } from './student-evaluation.service';
import { IAffectation } from 'app/shared/model/affectation.model';
import { AffectationService } from 'app/entities/affectation/affectation.service';
import { IStudent } from 'app/shared/model/student.model';
import { StudentService } from 'app/entities/student/student.service';
import { ICompany } from 'app/shared/model/company.model';
import { CompanyService } from 'app/entities/company/company.service';

type SelectableEntity = IAffectation | IStudent | ICompany;

@Component({
  selector: 'jhi-student-evaluation-update',
  templateUrl: './student-evaluation-update.component.html'
})
export class StudentEvaluationUpdateComponent implements OnInit {
  isSaving = false;
  affectations: IAffectation[] = [];
  students: IStudent[] = [];
  companies: ICompany[] = [];

  editForm = this.fb.group({
    id: [],
    communication: [null, [Validators.required]],
    technical: [null, [Validators.required]],
    affectation: [],
    student: [],
    company: []
  });

  constructor(
    protected studentEvaluationService: StudentEvaluationService,
    protected affectationService: AffectationService,
    protected studentService: StudentService,
    protected companyService: CompanyService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ studentEvaluation }) => {
      this.updateForm(studentEvaluation);

      this.affectationService
        .query({ filter: 'studentevaluation-is-null' })
        .pipe(
          map((res: HttpResponse<IAffectation[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IAffectation[]) => {
          if (!studentEvaluation.affectation || !studentEvaluation.affectation.id) {
            this.affectations = resBody;
          } else {
            this.affectationService
              .find(studentEvaluation.affectation.id)
              .pipe(
                map((subRes: HttpResponse<IAffectation>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IAffectation[]) => (this.affectations = concatRes));
          }
        });

      this.studentService.query().subscribe((res: HttpResponse<IStudent[]>) => (this.students = res.body || []));

      this.companyService.query().subscribe((res: HttpResponse<ICompany[]>) => (this.companies = res.body || []));
    });
  }

  updateForm(studentEvaluation: IStudentEvaluation): void {
    this.editForm.patchValue({
      id: studentEvaluation.id,
      communication: studentEvaluation.communication,
      technical: studentEvaluation.technical,
      affectation: studentEvaluation.affectation,
      student: studentEvaluation.student,
      company: studentEvaluation.company
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const studentEvaluation = this.createFromForm();
    if (studentEvaluation.id !== undefined) {
      this.subscribeToSaveResponse(this.studentEvaluationService.update(studentEvaluation));
    } else {
      this.subscribeToSaveResponse(this.studentEvaluationService.create(studentEvaluation));
    }
  }

  private createFromForm(): IStudentEvaluation {
    return {
      ...new StudentEvaluation(),
      id: this.editForm.get(['id'])!.value,
      communication: this.editForm.get(['communication'])!.value,
      technical: this.editForm.get(['technical'])!.value,
      affectation: this.editForm.get(['affectation'])!.value,
      student: this.editForm.get(['student'])!.value,
      company: this.editForm.get(['company'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStudentEvaluation>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
