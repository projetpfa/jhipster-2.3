import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IStudentEvaluation, StudentEvaluation } from 'app/shared/model/student-evaluation.model';
import { StudentEvaluationService } from './student-evaluation.service';
import { StudentEvaluationComponent } from './student-evaluation.component';
import { StudentEvaluationDetailComponent } from './student-evaluation-detail.component';
import { StudentEvaluationUpdateComponent } from './student-evaluation-update.component';

@Injectable({ providedIn: 'root' })
export class StudentEvaluationResolve implements Resolve<IStudentEvaluation> {
  constructor(private service: StudentEvaluationService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IStudentEvaluation> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((studentEvaluation: HttpResponse<StudentEvaluation>) => {
          if (studentEvaluation.body) {
            return of(studentEvaluation.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new StudentEvaluation());
  }
}

export const studentEvaluationRoute: Routes = [
  {
    path: '',
    component: StudentEvaluationComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'enisIntershipApp.studentEvaluation.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: StudentEvaluationDetailComponent,
    resolve: {
      studentEvaluation: StudentEvaluationResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.studentEvaluation.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: StudentEvaluationUpdateComponent,
    resolve: {
      studentEvaluation: StudentEvaluationResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.studentEvaluation.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: StudentEvaluationUpdateComponent,
    resolve: {
      studentEvaluation: StudentEvaluationResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'enisIntershipApp.studentEvaluation.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
