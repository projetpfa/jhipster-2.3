import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IAffectation, Affectation } from 'app/shared/model/affectation.model';
import { AffectationService } from './affectation.service';
import { IStudent } from 'app/shared/model/student.model';
import { StudentService } from 'app/entities/student/student.service';
import { IInternship } from 'app/shared/model/internship.model';
import { InternshipService } from 'app/entities/internship/internship.service';

type SelectableEntity = IStudent | IInternship;

@Component({
  selector: 'jhi-affectation-update',
  templateUrl: './affectation-update.component.html'
})
export class AffectationUpdateComponent implements OnInit {
  isSaving = false;
  students: IStudent[] = [];
  internships: IInternship[] = [];

  editForm = this.fb.group({
    id: [],
    dateAffectation: [null, [Validators.required]],
    startDate: [null, [Validators.required]],
    endDate: [null, [Validators.required]],
    student: [],
    internship: []
  });

  constructor(
    protected affectationService: AffectationService,
    protected studentService: StudentService,
    protected internshipService: InternshipService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ affectation }) => {
      if (!affectation.id) {
        const today = moment().startOf('day');
        affectation.dateAffectation = today;
        affectation.startDate = today;
        affectation.endDate = today;
      }

      this.updateForm(affectation);

      this.studentService.query().subscribe((res: HttpResponse<IStudent[]>) => (this.students = res.body || []));

      this.internshipService.query().subscribe((res: HttpResponse<IInternship[]>) => (this.internships = res.body || []));
    });
  }

  updateForm(affectation: IAffectation): void {
    this.editForm.patchValue({
      id: affectation.id,
      dateAffectation: affectation.dateAffectation ? affectation.dateAffectation.format(DATE_TIME_FORMAT) : null,
      startDate: affectation.startDate ? affectation.startDate.format(DATE_TIME_FORMAT) : null,
      endDate: affectation.endDate ? affectation.endDate.format(DATE_TIME_FORMAT) : null,
      student: affectation.student,
      internship: affectation.internship
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const affectation = this.createFromForm();
    if (affectation.id !== undefined) {
      this.subscribeToSaveResponse(this.affectationService.update(affectation));
    } else {
      this.subscribeToSaveResponse(this.affectationService.create(affectation));
    }
  }

  private createFromForm(): IAffectation {
    return {
      ...new Affectation(),
      id: this.editForm.get(['id'])!.value,
      dateAffectation: this.editForm.get(['dateAffectation'])!.value
        ? moment(this.editForm.get(['dateAffectation'])!.value, DATE_TIME_FORMAT)
        : undefined,
      startDate: this.editForm.get(['startDate'])!.value ? moment(this.editForm.get(['startDate'])!.value, DATE_TIME_FORMAT) : undefined,
      endDate: this.editForm.get(['endDate'])!.value ? moment(this.editForm.get(['endDate'])!.value, DATE_TIME_FORMAT) : undefined,
      student: this.editForm.get(['student'])!.value,
      internship: this.editForm.get(['internship'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAffectation>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
