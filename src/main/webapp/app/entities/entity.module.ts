import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'student',
        loadChildren: () => import('./student/student.module').then(m => m.EnisIntershipStudentModule)
      },
      {
        path: 'company',
        loadChildren: () => import('./company/company.module').then(m => m.EnisIntershipCompanyModule)
      },
      {
        path: 'academic-supervisor',
        loadChildren: () => import('./academic-supervisor/academic-supervisor.module').then(m => m.EnisIntershipAcademicSupervisorModule)
      },
      {
        path: 'internship',
        loadChildren: () => import('./internship/internship.module').then(m => m.EnisIntershipInternshipModule)
      },
      {
        path: 'interview',
        loadChildren: () => import('./interview/interview.module').then(m => m.EnisIntershipInterviewModule)
      },
      {
        path: 'invitation-interview',
        loadChildren: () => import('./invitation-interview/invitation-interview.module').then(m => m.EnisIntershipInvitationInterviewModule)
      },
      {
        path: 'student-evaluation',
        loadChildren: () => import('./student-evaluation/student-evaluation.module').then(m => m.EnisIntershipStudentEvaluationModule)
      },
      {
        path: 'company-evaluation',
        loadChildren: () => import('./company-evaluation/company-evaluation.module').then(m => m.EnisIntershipCompanyEvaluationModule)
      },
      {
        path: 'department',
        loadChildren: () => import('./department/department.module').then(m => m.EnisIntershipDepartmentModule)
      },
      {
        path: 'application',
        loadChildren: () => import('./application/application.module').then(m => m.EnisIntershipApplicationModule)
      },
      {
        path: 'affectation',
        loadChildren: () => import('./affectation/affectation.module').then(m => m.EnisIntershipAffectationModule)
      },
      {
        path: 'deadline',
        loadChildren: () => import('./deadline/deadline.module').then(m => m.EnisIntershipDeadlineModule)
      },
      {
        path: 'views',
        loadChildren: () => import('./views/views.module').then(m => m.EnisIntershipViewsModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class EnisIntershipEntityModule {}
