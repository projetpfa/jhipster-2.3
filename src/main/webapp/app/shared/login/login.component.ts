import { Component, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

import { LoginService } from 'app/core/login/login.service';
import { UserService } from 'app/core/user/user.service';
import { AccountService } from 'app/core/auth/account.service';
import { StudentService } from 'app/entities/student/student.service';
import { CompanyService } from 'app/entities/company/company.service';

@Component({
  selector: 'jhi-login-modal',
  templateUrl: './login.component.html'
})
export class LoginModalComponent implements AfterViewInit {
  @ViewChild('username', { static: false })
  username?: ElementRef;

  authenticationError = false;

  loginForm = this.fb.group({
    username: [''],
    password: [''],
    rememberMe: [false]
  });

  constructor(
    private loginService: LoginService,
    private router: Router,
    public activeModal: NgbActiveModal,
    private fb: FormBuilder,
    private userService: UserService,
    protected modalService: NgbModal,
    protected accountService: AccountService,
    protected studentService: StudentService,
    protected companyService: CompanyService
  ) {}

  ngAfterViewInit(): void {
    if (this.username) {
      this.username.nativeElement.focus();
    }
  }

  cancel(): void {
    this.authenticationError = false;
    this.loginForm.patchValue({
      username: '',
      password: ''
    });
    this.activeModal.dismiss('cancel');
  }

  login(): void {
    this.loginService
      .login({
        username: this.loginForm.get('username')!.value,
        password: this.loginForm.get('password')!.value,
        rememberMe: this.loginForm.get('rememberMe')!.value
      })
      .subscribe(
        () => {
          this.authenticationError = false;
          this.activeModal.close();
          {
            this.userService.find(this.loginForm.get('username')!.value).subscribe(user => {
              if (user.authorities?.includes('ROLE_STUDENT')) {
                this.studentService.findStudentByUser(user).subscribe(student => {
                  if (student) {
                    this.router.navigate(['/views/home-student']);
                  } else if (student == null) {
                    this.router.navigate(['/student/new']);
                  }
                  this.activeModal.close();
                });
              } else if (user.authorities?.includes('ROLE_COMPANY')) {
                this.companyService.findCompanyByUser(user).subscribe(company => {
                  if (company) {
                    this.router.navigate(['/views/home-company']);
                  } else if (company == null) {
                    this.router.navigate(['/company/new']);
                  }
                  this.activeModal.close();
                });
              } else if (user.authorities?.includes('ROLE_ADMIN')) {
                this.router.navigate(['/admin/user-management']);
              }
            });
          }
        },
        () => (this.authenticationError = true)
      );
  }

  requestResetPassword(): void {
    this.activeModal.dismiss('to state requestReset');
    this.router.navigate(['/account/reset', 'request']);
  }
}
