import { Moment } from 'moment';
import { DEADLINES } from 'app/shared/model/enumerations/deadlines.model';

export interface IDeadline {
  id?: number;
  start?: Moment;
  end?: Moment;
  type?: DEADLINES;
}

export class Deadline implements IDeadline {
  constructor(public id?: number, public start?: Moment, public end?: Moment, public type?: DEADLINES) {}
}
