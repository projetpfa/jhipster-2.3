import { Moment } from 'moment';
import { IStudent } from 'app/shared/model/student.model';
import { IInternship } from 'app/shared/model/internship.model';

export interface IAffectation {
  id?: number;
  dateAffectation?: Moment;
  startDate?: Moment;
  endDate?: Moment;
  student?: IStudent;
  internship?: IInternship;
}

export class Affectation implements IAffectation {
  constructor(
    public id?: number,
    public dateAffectation?: Moment,
    public startDate?: Moment,
    public endDate?: Moment,
    public student?: IStudent,
    public internship?: IInternship
  ) {}
}
