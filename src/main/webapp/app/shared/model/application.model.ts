import { Moment } from 'moment';
import { IStudent } from 'app/shared/model/student.model';
import { IInternship } from 'app/shared/model/internship.model';

export interface IApplication {
  id?: number;
  cvContentType?: string;
  cv?: any;
  firstDisponibility?: Moment;
  secondDisponibility?: Moment;
  thirdDisponibility?: Moment;
  student?: IStudent;
  internship?: IInternship;
}

export class Application implements IApplication {
  constructor(
    public id?: number,
    public cvContentType?: string,
    public cv?: any,
    public firstDisponibility?: Moment,
    public secondDisponibility?: Moment,
    public thirdDisponibility?: Moment,
    public student?: IStudent,
    public internship?: IInternship
  ) {}
}
