import { IUser } from 'app/core/user/user.model';
import { IDepartment } from 'app/shared/model/department.model';

export interface IAcademicSupervisor {
  id?: number;
  photoContentType?: string;
  photo?: any;
  city?: string;
  country?: string;
  postalCode?: number;
  phoneNumber?: number;
  grade?: string;
  user?: IUser;
  department?: IDepartment;
}

export class AcademicSupervisor implements IAcademicSupervisor {
  constructor(
    public id?: number,
    public photoContentType?: string,
    public photo?: any,
    public city?: string,
    public country?: string,
    public postalCode?: number,
    public phoneNumber?: number,
    public grade?: string,
    public user?: IUser,
    public department?: IDepartment
  ) {}
}
