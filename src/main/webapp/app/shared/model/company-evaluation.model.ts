import { IAffectation } from 'app/shared/model/affectation.model';
import { ICompany } from 'app/shared/model/company.model';
import { IStudent } from 'app/shared/model/student.model';

export interface ICompanyEvaluation {
  id?: number;
  supervision?: number;
  acceuil?: number;
  affectation?: IAffectation;
  company?: ICompany;
  student?: IStudent;
}

export class CompanyEvaluation implements ICompanyEvaluation {
  constructor(
    public id?: number,
    public supervision?: number,
    public acceuil?: number,
    public affectation?: IAffectation,
    public company?: ICompany,
    public student?: IStudent
  ) {}
}
