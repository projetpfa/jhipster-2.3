import { ICompany } from 'app/shared/model/company.model';
import { Level } from 'app/shared/model/enumerations/level.model';
import { Domain } from 'app/shared/model/enumerations/domain.model';

export interface IInternship {
  id?: number;
  photoContentType?: string;
  photo?: any;
  entitled?: string;
  description?: string;
  duration?: string;
  level?: Level;
  domain?: Domain;
  brochureContentType?: string;
  brochure?: any;
  company?: ICompany;
}

export class Internship implements IInternship {
  constructor(
    public id?: number,
    public photoContentType?: string,
    public photo?: any,
    public entitled?: string,
    public description?: string,
    public duration?: string,
    public level?: Level,
    public domain?: Domain,
    public brochureContentType?: string,
    public brochure?: any,
    public company?: ICompany
  ) {}
}
