import { Domain } from 'app/shared/model/enumerations/domain.model';

export interface IDepartment {
  id?: number;
  name?: Domain;
}

export class Department implements IDepartment {
  constructor(public id?: number, public name?: Domain) {}
}
