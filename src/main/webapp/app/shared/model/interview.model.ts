import { Moment } from 'moment';
import { IInvitationInterview } from 'app/shared/model/invitation-interview.model';
import { ICompany } from 'app/shared/model/company.model';
import { IStudent } from 'app/shared/model/student.model';
import { IInternship } from 'app/shared/model/internship.model';
import { ATTENDANCE } from 'app/shared/model/enumerations/attendance.model';
import { STATE } from 'app/shared/model/enumerations/state.model';
import { Choice } from 'app/shared/model/enumerations/choice.model';

export interface IInterview {
  id?: number;
  date?: Moment;
  presence?: ATTENDANCE;
  state?: STATE;
  communication?: string;
  scoreCommunication?: number;
  technical?: string;
  scoretTechnical?: number;
  other?: string;
  studFinalChoice?: Choice;
  invitationInterview?: IInvitationInterview;
  company?: ICompany;
  student?: IStudent;
  internship?: IInternship;
}

export class Interview implements IInterview {
  constructor(
    public id?: number,
    public date?: Moment,
    public presence?: ATTENDANCE,
    public state?: STATE,
    public communication?: string,
    public scoreCommunication?: number,
    public technical?: string,
    public scoretTechnical?: number,
    public other?: string,
    public studFinalChoice?: Choice,
    public invitationInterview?: IInvitationInterview,
    public company?: ICompany,
    public student?: IStudent,
    public internship?: IInternship
  ) {}
}
