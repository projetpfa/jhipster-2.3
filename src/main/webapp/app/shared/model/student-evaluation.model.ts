import { IAffectation } from 'app/shared/model/affectation.model';
import { IStudent } from 'app/shared/model/student.model';
import { ICompany } from 'app/shared/model/company.model';

export interface IStudentEvaluation {
  id?: number;
  communication?: number;
  technical?: number;
  affectation?: IAffectation;
  student?: IStudent;
  company?: ICompany;
}

export class StudentEvaluation implements IStudentEvaluation {
  constructor(
    public id?: number,
    public communication?: number,
    public technical?: number,
    public affectation?: IAffectation,
    public student?: IStudent,
    public company?: ICompany
  ) {}
}
