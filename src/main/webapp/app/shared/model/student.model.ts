import { IUser } from 'app/core/user/user.model';
import { IDepartment } from 'app/shared/model/department.model';
import { Level } from 'app/shared/model/enumerations/level.model';

export interface IStudent {
  id?: number;
  photoContentType?: string;
  photo?: any;
  city?: string;
  country?: string;
  postalCode?: number;
  phoneNumber?: number;
  cvContentType?: string;
  cv?: any;
  level?: Level;
  user?: IUser;
  department?: IDepartment;
}

export class Student implements IStudent {
  constructor(
    public id?: number,
    public photoContentType?: string,
    public photo?: any,
    public city?: string,
    public country?: string,
    public postalCode?: number,
    public phoneNumber?: number,
    public cvContentType?: string,
    public cv?: any,
    public level?: Level,
    public user?: IUser,
    public department?: IDepartment
  ) {}
}
