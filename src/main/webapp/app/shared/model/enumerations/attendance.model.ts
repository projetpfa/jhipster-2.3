export const enum ATTENDANCE {
  NONE = 'NONE',
  PRESENT = 'PRESENT',
  ABSENT = 'ABSENT'
}
