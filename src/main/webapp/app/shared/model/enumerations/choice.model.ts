export const enum Choice {
  NONE = 'NONE',
  ACCEPTED = 'ACCEPTED',
  REFUSED = 'REFUSED'
}
