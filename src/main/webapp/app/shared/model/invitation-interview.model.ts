import { Moment } from 'moment';
import { IApplication } from 'app/shared/model/application.model';
import { ICompany } from 'app/shared/model/company.model';

export interface IInvitationInterview {
  id?: number;
  confirmation?: string;
  dateInterview?: Moment;
  application?: IApplication;
  company?: ICompany;
}

export class InvitationInterview implements IInvitationInterview {
  constructor(
    public id?: number,
    public confirmation?: string,
    public dateInterview?: Moment,
    public application?: IApplication,
    public company?: ICompany
  ) {}
}
