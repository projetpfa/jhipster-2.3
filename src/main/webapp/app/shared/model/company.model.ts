import { IUser } from 'app/core/user/user.model';

export interface ICompany {
  id?: number;
  photoContentType?: string;
  photo?: any;
  city?: string;
  country?: string;
  postalCode?: number;
  phoneNumber?: number;
  nameCompany?: string;
  description?: string;
  brochureCompanyContentType?: string;
  brochureCompany?: any;
  websiteCompany?: string;
  user?: IUser;
}

export class Company implements ICompany {
  constructor(
    public id?: number,
    public photoContentType?: string,
    public photo?: any,
    public city?: string,
    public country?: string,
    public postalCode?: number,
    public phoneNumber?: number,
    public nameCompany?: string,
    public description?: string,
    public brochureCompanyContentType?: string,
    public brochureCompany?: any,
    public websiteCompany?: string,
    public user?: IUser
  ) {}
}
