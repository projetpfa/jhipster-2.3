import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { LoginModalComponent } from 'app/shared/login/login.component';
import { LoginCompanyComponent } from 'app/entities/views/login-company/login-company.component';
import { LoginStudentComponent } from 'app/entities/views/login-student/login-student.component';

@Injectable({ providedIn: 'root' })
export class LoginModalService {
  private isOpen = false;
  private isOpen2 = false;
  private isOpen3 = false;
  constructor(private modalService: NgbModal) {}

  open(): void {
    if (this.isOpen) {
      return;
    }
    this.isOpen = true;
    const modalRef: NgbModalRef = this.modalService.open(LoginModalComponent);
    modalRef.result.finally(() => (this.isOpen = false));
  }
  openCompany(): void {
    if (this.isOpen2) {
      return;
    }
    this.isOpen2 = true;
    const modalRef: NgbModalRef = this.modalService.open(LoginCompanyComponent);
    modalRef.result.finally(() => (this.isOpen2 = false));
  }
  openStudent(): void {
    if (this.isOpen3) {
      return;
    }
    this.isOpen3 = true;
    const modalRef: NgbModalRef = this.modalService.open(LoginStudentComponent);
    modalRef.result.finally(() => (this.isOpen3 = false));
  }
}
