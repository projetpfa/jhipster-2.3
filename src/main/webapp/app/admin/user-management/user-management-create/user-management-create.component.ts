import { Component, OnInit } from '@angular/core';
import { User } from 'app/core/user/user.model';
import { LANGUAGES } from 'app/core/language/language.constants';
import { Validators, FormBuilder } from '@angular/forms';
import { RegisterService } from 'app/account/register/register.service';
import { UserService } from 'app/core/user/user.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'jhi-user-management-create',
  templateUrl: './user-management-create.component.html',
  styleUrls: ['./user-management-create.component.scss']
})
export class UserManagementCreateComponent implements OnInit {


  user!: User;
  languages = LANGUAGES;
  authorities: string[] = [];
  isSaving = false;
  doNotMatch = false;
  error = false;
  errorEmailExists = false;
  errorUserExists = false;


  editForm = this.fb.group({
    id: [],
    login: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(50), Validators.pattern('^[_.@A-Za-z0-9-]*')]],
    firstName: ['', [Validators.maxLength(50)]],
    lastName: ['', [Validators.maxLength(50)]],
    email: ['', [Validators.minLength(5), Validators.maxLength(254), Validators.email]],
    activated: [],
    langKey: [],
    authorities: [],
    password: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
    confirmPassword: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]]
  });

  constructor(private registerService: RegisterService,private userService: UserService, private route: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.route.data.subscribe(({ user }) => {
      if (user) {
        this.user = user;
        if (this.user.id === undefined) {
          this.user.activated = true;
        }
        this.updateForm(user);
      }
    });
    this.userService.authorities().subscribe(authorities => {
      this.authorities = authorities;
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    this.updateUser(this.user);
    if (this.user.id !== undefined) {
      this.userService.update(this.user).subscribe(
        () => this.onSaveSuccess(),
        () => this.onSaveError()
      );
    } else {
    const password = this.editForm.get(['password'])!.value;
    if (password !== this.editForm.get(['confirmPassword'])!.value) {
      this.doNotMatch = true;
    } else {
      this.registerService.save(this.user).subscribe(
        () => this.onSaveSuccess(),
        () => this.onSaveError()
      );



    }}
  }

  private updateForm(user: User): void {
    this.editForm.patchValue({
      id: user.id,
      login: user.login,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      activated: user.activated,
      langKey: user.langKey,
      authorities: user.authorities,
      passowrd:user.password
    });
  }

  private updateUser(user: User): void {
    user.login = this.editForm.get(['login'])!.value;
    user.firstName = this.editForm.get(['firstName'])!.value;
    user.lastName = this.editForm.get(['lastName'])!.value;
    user.email = this.editForm.get(['email'])!.value;
    user.activated = this.editForm.get(['activated'])!.value;
    user.langKey = this.editForm.get(['langKey'])!.value;
    user.authorities = this.editForm.get(['authorities'])!.value;
    user.password = this.editForm.get(['password'])!.value;

  }

  private onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  private onSaveError(): void {
    this.isSaving = false;
  }


}
