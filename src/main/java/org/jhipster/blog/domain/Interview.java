package org.jhipster.blog.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;

import org.jhipster.blog.domain.enumeration.ATTENDANCE;

import org.jhipster.blog.domain.enumeration.STATE;

import org.jhipster.blog.domain.enumeration.Choice;

/**
 * A Interview.
 */
@Entity
@Table(name = "interview")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Interview implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "date", nullable = false)
    private Instant date;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "presence", nullable = false)
    private ATTENDANCE presence;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "state", nullable = false)
    private STATE state;

    @NotNull
    @Column(name = "communication", nullable = false)
    private String communication;

    @NotNull
    @Column(name = "score_communication", nullable = false)
    private Integer scoreCommunication;

    @NotNull
    @Column(name = "technical", nullable = false)
    private String technical;

    @NotNull
    @Column(name = "scoret_technical", nullable = false)
    private Integer scoretTechnical;

    @Column(name = "other")
    private String other;

    @Enumerated(EnumType.STRING)
    @Column(name = "stud_final_choice")
    private Choice studFinalChoice;

    @OneToOne
    @JoinColumn(unique = true)
    private InvitationInterview invitationInterview;

    @ManyToOne
    @JsonIgnoreProperties("interviews")
    private Company company;

    @ManyToOne
    @JsonIgnoreProperties("interviews")
    private Student student;

    @ManyToOne
    @JsonIgnoreProperties("interviews")
    private Internship internship;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDate() {
        return date;
    }

    public Interview date(Instant date) {
        this.date = date;
        return this;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public ATTENDANCE getPresence() {
        return presence;
    }

    public Interview presence(ATTENDANCE presence) {
        this.presence = presence;
        return this;
    }

    public void setPresence(ATTENDANCE presence) {
        this.presence = presence;
    }

    public STATE getState() {
        return state;
    }

    public Interview state(STATE state) {
        this.state = state;
        return this;
    }

    public void setState(STATE state) {
        this.state = state;
    }

    public String getCommunication() {
        return communication;
    }

    public Interview communication(String communication) {
        this.communication = communication;
        return this;
    }

    public void setCommunication(String communication) {
        this.communication = communication;
    }

    public Integer getScoreCommunication() {
        return scoreCommunication;
    }

    public Interview scoreCommunication(Integer scoreCommunication) {
        this.scoreCommunication = scoreCommunication;
        return this;
    }

    public void setScoreCommunication(Integer scoreCommunication) {
        this.scoreCommunication = scoreCommunication;
    }

    public String getTechnical() {
        return technical;
    }

    public Interview technical(String technical) {
        this.technical = technical;
        return this;
    }

    public void setTechnical(String technical) {
        this.technical = technical;
    }

    public Integer getScoretTechnical() {
        return scoretTechnical;
    }

    public Interview scoretTechnical(Integer scoretTechnical) {
        this.scoretTechnical = scoretTechnical;
        return this;
    }

    public void setScoretTechnical(Integer scoretTechnical) {
        this.scoretTechnical = scoretTechnical;
    }

    public String getOther() {
        return other;
    }

    public Interview other(String other) {
        this.other = other;
        return this;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public Choice getStudFinalChoice() {
        return studFinalChoice;
    }

    public Interview studFinalChoice(Choice studFinalChoice) {
        this.studFinalChoice = studFinalChoice;
        return this;
    }

    public void setStudFinalChoice(Choice studFinalChoice) {
        this.studFinalChoice = studFinalChoice;
    }

    public InvitationInterview getInvitationInterview() {
        return invitationInterview;
    }

    public Interview invitationInterview(InvitationInterview invitationInterview) {
        this.invitationInterview = invitationInterview;
        return this;
    }

    public void setInvitationInterview(InvitationInterview invitationInterview) {
        this.invitationInterview = invitationInterview;
    }

    public Company getCompany() {
        return company;
    }

    public Interview company(Company company) {
        this.company = company;
        return this;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Student getStudent() {
        return student;
    }

    public Interview student(Student student) {
        this.student = student;
        return this;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Internship getInternship() {
        return internship;
    }

    public Interview internship(Internship internship) {
        this.internship = internship;
        return this;
    }

    public void setInternship(Internship internship) {
        this.internship = internship;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Interview)) {
            return false;
        }
        return id != null && id.equals(((Interview) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Interview{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", presence='" + getPresence() + "'" +
            ", state='" + getState() + "'" +
            ", communication='" + getCommunication() + "'" +
            ", scoreCommunication=" + getScoreCommunication() +
            ", technical='" + getTechnical() + "'" +
            ", scoretTechnical=" + getScoretTechnical() +
            ", other='" + getOther() + "'" +
            ", studFinalChoice='" + getStudFinalChoice() + "'" +
            "}";
    }
}
