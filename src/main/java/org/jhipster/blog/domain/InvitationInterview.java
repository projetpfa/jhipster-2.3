package org.jhipster.blog.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;

/**
 * A InvitationInterview.
 */
@Entity
@Table(name = "invitation_interview")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class InvitationInterview implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "confirmation")
    private String confirmation;

    @NotNull
    @Column(name = "date_interview", nullable = false)
    private Instant dateInterview;

    @OneToOne
    @JoinColumn(unique = true)
    private Application application;

    @ManyToOne
    @JsonIgnoreProperties("invitationInterviews")
    private Company company;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getConfirmation() {
        return confirmation;
    }

    public InvitationInterview confirmation(String confirmation) {
        this.confirmation = confirmation;
        return this;
    }

    public void setConfirmation(String confirmation) {
        this.confirmation = confirmation;
    }

    public Instant getDateInterview() {
        return dateInterview;
    }

    public InvitationInterview dateInterview(Instant dateInterview) {
        this.dateInterview = dateInterview;
        return this;
    }

    public void setDateInterview(Instant dateInterview) {
        this.dateInterview = dateInterview;
    }

    public Application getApplication() {
        return application;
    }

    public InvitationInterview application(Application application) {
        this.application = application;
        return this;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public Company getCompany() {
        return company;
    }

    public InvitationInterview company(Company company) {
        this.company = company;
        return this;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof InvitationInterview)) {
            return false;
        }
        return id != null && id.equals(((InvitationInterview) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "InvitationInterview{" +
            "id=" + getId() +
            ", confirmation='" + getConfirmation() + "'" +
            ", dateInterview='" + getDateInterview() + "'" +
            "}";
    }
}
