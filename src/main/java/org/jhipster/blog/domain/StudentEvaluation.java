package org.jhipster.blog.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A StudentEvaluation.
 */
@Entity
@Table(name = "student_evaluation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class StudentEvaluation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "communication", nullable = false)
    private Integer communication;

    @NotNull
    @Column(name = "technical", nullable = false)
    private Integer technical;

    @OneToOne
    @JoinColumn(unique = true)
    private Affectation affectation;

    @ManyToOne
    @JsonIgnoreProperties("studentEvaluations")
    private Student student;

    @ManyToOne
    @JsonIgnoreProperties("studentEvaluations")
    private Company company;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCommunication() {
        return communication;
    }

    public StudentEvaluation communication(Integer communication) {
        this.communication = communication;
        return this;
    }

    public void setCommunication(Integer communication) {
        this.communication = communication;
    }

    public Integer getTechnical() {
        return technical;
    }

    public StudentEvaluation technical(Integer technical) {
        this.technical = technical;
        return this;
    }

    public void setTechnical(Integer technical) {
        this.technical = technical;
    }

    public Affectation getAffectation() {
        return affectation;
    }

    public StudentEvaluation affectation(Affectation affectation) {
        this.affectation = affectation;
        return this;
    }

    public void setAffectation(Affectation affectation) {
        this.affectation = affectation;
    }

    public Student getStudent() {
        return student;
    }

    public StudentEvaluation student(Student student) {
        this.student = student;
        return this;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Company getCompany() {
        return company;
    }

    public StudentEvaluation company(Company company) {
        this.company = company;
        return this;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof StudentEvaluation)) {
            return false;
        }
        return id != null && id.equals(((StudentEvaluation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "StudentEvaluation{" +
            "id=" + getId() +
            ", communication=" + getCommunication() +
            ", technical=" + getTechnical() +
            "}";
    }
}
