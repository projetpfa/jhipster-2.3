package org.jhipster.blog.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jhipster.blog.domain.enumeration.Level;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A Student.
 */
@Entity
@Table(name = "student")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Student implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Lob
	@Column(name = "photo")
	private byte[] photo;

	@Column(name = "photo_content_type")
	private String photoContentType;

	@NotNull
	@Column(name = "city", nullable = false)
	private String city;

	@NotNull
	@Column(name = "country", nullable = false)
	private String country;

	@NotNull
	@Column(name = "postal_code", nullable = false)
	private Integer postalCode;

	@NotNull
	@Column(name = "phone_number", nullable = false)
	private Integer phoneNumber;

	@Lob
	@Column(name = "cv", nullable = false)
	private byte[] cv;

	@Column(name = "cv_content_type", nullable = false)
	private String cvContentType;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "level", nullable = false)
	private Level level;

	@OneToOne
	@JoinColumn(unique = true)
	private User user;

	@ManyToOne
	@JsonIgnoreProperties("students")
	private Department department;

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public Student photo(byte[] photo) {
		this.photo = photo;
		return this;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public String getPhotoContentType() {
		return photoContentType;
	}

	public Student photoContentType(String photoContentType) {
		this.photoContentType = photoContentType;
		return this;
	}

	public void setPhotoContentType(String photoContentType) {
		this.photoContentType = photoContentType;
	}

	public String getCity() {
		return city;
	}

	public Student city(String city) {
		this.city = city;
		return this;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public Student country(String country) {
		this.country = country;
		return this;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Integer getPostalCode() {
		return postalCode;
	}

	public Student postalCode(Integer postalCode) {
		this.postalCode = postalCode;
		return this;
	}

	public void setPostalCode(Integer postalCode) {
		this.postalCode = postalCode;
	}

	public Integer getPhoneNumber() {
		return phoneNumber;
	}

	public Student phoneNumber(Integer phoneNumber) {
		this.phoneNumber = phoneNumber;
		return this;
	}

	public void setPhoneNumber(Integer phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public byte[] getCv() {
		return cv;
	}

	public Student cv(byte[] cv) {
		this.cv = cv;
		return this;
	}

	public void setCv(byte[] cv) {
		this.cv = cv;
	}

	public String getCvContentType() {
		return cvContentType;
	}

	public Student cvContentType(String cvContentType) {
		this.cvContentType = cvContentType;
		return this;
	}

	public void setCvContentType(String cvContentType) {
		this.cvContentType = cvContentType;
	}

	public Level getLevel() {
		return level;
	}

	public Student level(Level level) {
		this.level = level;
		return this;
	}

	public void setLevel(Level level) {
		this.level = level;
	}

	public User getUser() {
		return user;
	}

	public Student user(User user) {
		this.user = user;
		return this;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Department getDepartment() {
		return department;
	}

	public Student department(Department department) {
		this.department = department;
		return this;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Student)) {
			return false;
		}
		return id != null && id.equals(((Student) o).id);
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return "Student{" + "id=" + getId() + ", photo='" + getPhoto() + "'" + ", photoContentType='"
				+ getPhotoContentType() + "'" + ", city='" + getCity() + "'" + ", country='" + getCountry() + "'"
				+ ", postalCode=" + getPostalCode() + ", phoneNumber=" + getPhoneNumber() + ", cv='" + getCv() + "'"
				+ ", cvContentType='" + getCvContentType() + "'" + ", level='" + getLevel() + "'" + "}";
	}
}
