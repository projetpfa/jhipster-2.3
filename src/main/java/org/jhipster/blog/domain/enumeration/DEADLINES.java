package org.jhipster.blog.domain.enumeration;

/**
 * The DEADLINES enumeration.
 */
public enum DEADLINES {
    DEADLINEINTERVIEW, DEADLINEAFFECTATION, DEADLINERESULTINTERVIEW, DEADLINEAPPLICATION
}
