package org.jhipster.blog.domain.enumeration;

/**
 * The Choice enumeration.
 */
public enum Choice {
    NONE, ACCEPTED, REFUSED
}
