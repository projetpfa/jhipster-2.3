package org.jhipster.blog.domain.enumeration;

/**
 * The Level enumeration.
 */
public enum Level {
    FIRSTYEAR, SECONDYEAR, THIRDYEAR
}
