package org.jhipster.blog.domain.enumeration;

/**
 * The STATE enumeration.
 */
public enum STATE {
    NONE, SUCCESS, FAILURE
}
