package org.jhipster.blog.domain.enumeration;

/**
 * The ATTENDANCE enumeration.
 */
public enum ATTENDANCE {
    NONE, PRESENT, ABSENT
}
