package org.jhipster.blog.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A CompanyEvaluation.
 */
@Entity
@Table(name = "company_evaluation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CompanyEvaluation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "supervision", nullable = false)
    private Integer supervision;

    @NotNull
    @Column(name = "acceuil", nullable = false)
    private Integer acceuil;

    @OneToOne
    @JoinColumn(unique = true)
    private Affectation affectation;

    @ManyToOne
    @JsonIgnoreProperties("companyEvaluations")
    private Company company;

    @ManyToOne
    @JsonIgnoreProperties("companyEvaluations")
    private Student student;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSupervision() {
        return supervision;
    }

    public CompanyEvaluation supervision(Integer supervision) {
        this.supervision = supervision;
        return this;
    }

    public void setSupervision(Integer supervision) {
        this.supervision = supervision;
    }

    public Integer getAcceuil() {
        return acceuil;
    }

    public CompanyEvaluation acceuil(Integer acceuil) {
        this.acceuil = acceuil;
        return this;
    }

    public void setAcceuil(Integer acceuil) {
        this.acceuil = acceuil;
    }

    public Affectation getAffectation() {
        return affectation;
    }

    public CompanyEvaluation affectation(Affectation affectation) {
        this.affectation = affectation;
        return this;
    }

    public void setAffectation(Affectation affectation) {
        this.affectation = affectation;
    }

    public Company getCompany() {
        return company;
    }

    public CompanyEvaluation company(Company company) {
        this.company = company;
        return this;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Student getStudent() {
        return student;
    }

    public CompanyEvaluation student(Student student) {
        this.student = student;
        return this;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompanyEvaluation)) {
            return false;
        }
        return id != null && id.equals(((CompanyEvaluation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "CompanyEvaluation{" +
            "id=" + getId() +
            ", supervision=" + getSupervision() +
            ", acceuil=" + getAcceuil() +
            "}";
    }
}
