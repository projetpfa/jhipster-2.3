package org.jhipster.blog.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * A Company.
 */
@Entity
@Table(name = "company")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Company implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Lob
    @Column(name = "photo")
    private byte[] photo;

    @Column(name = "photo_content_type")
    private String photoContentType;

    @NotNull
    @Column(name = "city", nullable = false)
    private String city;

    @NotNull
    @Column(name = "country", nullable = false)
    private String country;

    @NotNull
    @Column(name = "postal_code", nullable = false)
    private Integer postalCode;

    @NotNull
    @Column(name = "phone_number", nullable = false)
    private Integer phoneNumber;

    @NotNull
    @Column(name = "name_company", nullable = false)
    private String nameCompany;

    @NotNull
    @Column(name = "description", nullable = false)
    private String description;

    @Lob
    @Column(name = "brochure_company")
    private byte[] brochureCompany;

    @Column(name = "brochure_company_content_type")
    private String brochureCompanyContentType;

    @Column(name = "website_company")
    private String websiteCompany;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;
    
    @OneToMany(mappedBy="company" , cascade=CascadeType.ALL)
    List<Internship> internships;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public Company photo(byte[] photo) {
        this.photo = photo;
        return this;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getPhotoContentType() {
        return photoContentType;
    }

    public Company photoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
        return this;
    }

    public void setPhotoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
    }

    public String getCity() {
        return city;
    }

    public Company city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public Company country(String country) {
        this.country = country;
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getPostalCode() {
        return postalCode;
    }

    public Company postalCode(Integer postalCode) {
        this.postalCode = postalCode;
        return this;
    }

    public void setPostalCode(Integer postalCode) {
        this.postalCode = postalCode;
    }

    public Integer getPhoneNumber() {
        return phoneNumber;
    }

    public Company phoneNumber(Integer phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public void setPhoneNumber(Integer phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getNameCompany() {
        return nameCompany;
    }

    public Company nameCompany(String nameCompany) {
        this.nameCompany = nameCompany;
        return this;
    }

    public void setNameCompany(String nameCompany) {
        this.nameCompany = nameCompany;
    }

    public String getDescription() {
        return description;
    }

    public Company description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getBrochureCompany() {
        return brochureCompany;
    }

    public Company brochureCompany(byte[] brochureCompany) {
        this.brochureCompany = brochureCompany;
        return this;
    }

    public void setBrochureCompany(byte[] brochureCompany) {
        this.brochureCompany = brochureCompany;
    }

    public String getBrochureCompanyContentType() {
        return brochureCompanyContentType;
    }

    public Company brochureCompanyContentType(String brochureCompanyContentType) {
        this.brochureCompanyContentType = brochureCompanyContentType;
        return this;
    }

    public void setBrochureCompanyContentType(String brochureCompanyContentType) {
        this.brochureCompanyContentType = brochureCompanyContentType;
    }

    public String getWebsiteCompany() {
        return websiteCompany;
    }

    public Company websiteCompany(String websiteCompany) {
        this.websiteCompany = websiteCompany;
        return this;
    }

    public void setWebsiteCompany(String websiteCompany) {
        this.websiteCompany = websiteCompany;
    }

    public User getUser() {
        return user;
    }

    public Company user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Company)) {
            return false;
        }
        return id != null && id.equals(((Company) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Company{" +
            "id=" + getId() +
            ", photo='" + getPhoto() + "'" +
            ", photoContentType='" + getPhotoContentType() + "'" +
            ", city='" + getCity() + "'" +
            ", country='" + getCountry() + "'" +
            ", postalCode=" + getPostalCode() +
            ", phoneNumber=" + getPhoneNumber() +
            ", nameCompany='" + getNameCompany() + "'" +
            ", description='" + getDescription() + "'" +
            ", brochureCompany='" + getBrochureCompany() + "'" +
            ", brochureCompanyContentType='" + getBrochureCompanyContentType() + "'" +
            ", websiteCompany='" + getWebsiteCompany() + "'" +
            "}";
    }
}
