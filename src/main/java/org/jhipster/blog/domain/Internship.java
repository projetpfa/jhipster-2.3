package org.jhipster.blog.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.jhipster.blog.domain.enumeration.Domain;
import org.jhipster.blog.domain.enumeration.Level;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A Internship.
 */
@Entity
@Table(name = "internship")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Internship implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Lob
	@Column(name = "photo")
	private byte[] photo;

	@Column(name = "photo_content_type")
	private String photoContentType;

	@NotNull
	@Column(name = "entitled", nullable = false)
	private String entitled;

	@NotNull
	@Column(name = "description", nullable = false)
	private String description;

	@NotNull
	@Column(name = "duration", nullable = false)
	private String duration;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "level", nullable = false)
	private Level level;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "domain", nullable = false)
	private Domain domain;

	@Lob
	@Column(name = "brochure")
	private byte[] brochure;

	@Column(name = "brochure_content_type")
	private String brochureContentType;

	

	@ManyToOne
	@JsonIgnoreProperties("internships")
	private Company company;

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {

		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public Internship photo(byte[] photo) {
		this.photo = photo;
		return this;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public String getPhotoContentType() {
		return photoContentType;
	}

	public Internship photoContentType(String photoContentType) {
		this.photoContentType = photoContentType;
		return this;
	}

	public void setPhotoContentType(String photoContentType) {
		this.photoContentType = photoContentType;
	}

	public String getEntitled() {
		return entitled;
	}

	public Internship entitled(String entitled) {
		this.entitled = entitled;
		return this;
	}

	public void setEntitled(String entitled) {
		this.entitled = entitled;
	}

	public String getDescription() {
		return description;
	}

	public Internship description(String description) {
		this.description = description;
		return this;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDuration() {
		return duration;
	}

	public Internship duration(String duration) {
		this.duration = duration;
		return this;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public Level getLevel() {
		return level;
	}

	public Internship level(Level level) {
		this.level = level;
		return this;
	}

	public void setLevel(Level level) {
		this.level = level;
	}

	public Domain getDomain() {
		return domain;
	}

	public Internship domain(Domain domain) {
		this.domain = domain;
		return this;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public byte[] getBrochure() {
		return brochure;
	}

	public Internship brochure(byte[] brochure) {
		this.brochure = brochure;
		return this;
	}

	public void setBrochure(byte[] brochure) {
		this.brochure = brochure;
	}

	public String getBrochureContentType() {
		return brochureContentType;
	}

	public Internship brochureContentType(String brochureContentType) {
		this.brochureContentType = brochureContentType;
		return this;
	}

	public void setBrochureContentType(String brochureContentType) {
		this.brochureContentType = brochureContentType;
	}

	

	public Company getCompany() {
		return company;
	}

	public Internship company(Company company) {
		this.company = company;
		return this;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Internship)) {
			return false;
		}
		return id != null && id.equals(((Internship) o).id);
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return "Internship{" + "id=" + getId() + ", photo='" + getPhoto() + "'" + ", photoContentType='"
				+ getPhotoContentType() + "'" + ", entitled='" + getEntitled() + "'" + ", description='"
				+ getDescription() + "'" + ", duration='" + getDuration() + "'" + ", level='" + getLevel() + "'"
				+ ", domain='" + getDomain() + "'" + ", brochure='" + getBrochure() + "'" + ", brochureContentType='"
				+ getBrochureContentType() + "'" + "}";
	}
}
