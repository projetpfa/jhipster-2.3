package org.jhipster.blog.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;

/**
 * A Application.
 */
@Entity
@Table(name = "application")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Application implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    
    @Lob
    @Column(name = "cv", nullable = false)
    private byte[] cv;

    @Column(name = "cv_content_type", nullable = false)
    private String cvContentType;

    @NotNull
    @Column(name = "first_disponibility", nullable = false)
    private Instant firstDisponibility;

    @NotNull
    @Column(name = "second_disponibility", nullable = false)
    private Instant secondDisponibility;

    @NotNull
    @Column(name = "third_disponibility", nullable = false)
    private Instant thirdDisponibility;

    @ManyToOne
    @JsonIgnoreProperties("applications")
    private Student student;

    @ManyToOne
    @JsonIgnoreProperties("applications")
    private Internship internship;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getCv() {
        return cv;
    }

    public Application cv(byte[] cv) {
        this.cv = cv;
        return this;
    }

    public void setCv(byte[] cv) {
        this.cv = cv;
    }

    public String getCvContentType() {
        return cvContentType;
    }

    public Application cvContentType(String cvContentType) {
        this.cvContentType = cvContentType;
        return this;
    }

    public void setCvContentType(String cvContentType) {
        this.cvContentType = cvContentType;
    }

    public Instant getFirstDisponibility() {
        return firstDisponibility;
    }

    public Application firstDisponibility(Instant firstDisponibility) {
        this.firstDisponibility = firstDisponibility;
        return this;
    }

    public void setFirstDisponibility(Instant firstDisponibility) {
        this.firstDisponibility = firstDisponibility;
    }

    public Instant getSecondDisponibility() {
        return secondDisponibility;
    }

    public Application secondDisponibility(Instant secondDisponibility) {
        this.secondDisponibility = secondDisponibility;
        return this;
    }

    public void setSecondDisponibility(Instant secondDisponibility) {
        this.secondDisponibility = secondDisponibility;
    }

    public Instant getThirdDisponibility() {
        return thirdDisponibility;
    }

    public Application thirdDisponibility(Instant thirdDisponibility) {
        this.thirdDisponibility = thirdDisponibility;
        return this;
    }

    public void setThirdDisponibility(Instant thirdDisponibility) {
        this.thirdDisponibility = thirdDisponibility;
    }

    public Student getStudent() {
        return student;
    }

    public Application student(Student student) {
        this.student = student;
        return this;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Internship getInternship() {
        return internship;
    }

    public Application internship(Internship internship) {
        this.internship = internship;
        return this;
    }

    public void setInternship(Internship internship) {
        this.internship = internship;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Application)) {
            return false;
        }
        return id != null && id.equals(((Application) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Application{" +
            "id=" + getId() +
            ", cv='" + getCv() + "'" +
            ", cvContentType='" + getCvContentType() + "'" +
            ", firstDisponibility='" + getFirstDisponibility() + "'" +
            ", secondDisponibility='" + getSecondDisponibility() + "'" +
            ", thirdDisponibility='" + getThirdDisponibility() + "'" +
            "}";
    }
}
