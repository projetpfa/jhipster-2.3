package org.jhipster.blog.web.rest;

import org.jhipster.blog.domain.Views;
import org.jhipster.blog.repository.ViewsRepository;
import org.jhipster.blog.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.jhipster.blog.domain.Views}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ViewsResource {

    private final Logger log = LoggerFactory.getLogger(ViewsResource.class);

    private static final String ENTITY_NAME = "views";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ViewsRepository viewsRepository;

    public ViewsResource(ViewsRepository viewsRepository) {
        this.viewsRepository = viewsRepository;
    }

    /**
     * {@code POST  /views} : Create a new views.
     *
     * @param views the views to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new views, or with status {@code 400 (Bad Request)} if the views has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/views")
    public ResponseEntity<Views> createViews(@RequestBody Views views) throws URISyntaxException {
        log.debug("REST request to save Views : {}", views);
        if (views.getId() != null) {
            throw new BadRequestAlertException("A new views cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Views result = viewsRepository.save(views);
        return ResponseEntity.created(new URI("/api/views/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /views} : Updates an existing views.
     *
     * @param views the views to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated views,
     * or with status {@code 400 (Bad Request)} if the views is not valid,
     * or with status {@code 500 (Internal Server Error)} if the views couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/views")
    public ResponseEntity<Views> updateViews(@RequestBody Views views) throws URISyntaxException {
        log.debug("REST request to update Views : {}", views);
        if (views.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Views result = viewsRepository.save(views);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, views.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /views} : get all the views.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of views in body.
     */
    @GetMapping("/views")
    public ResponseEntity<List<Views>> getAllViews(Pageable pageable) {
        log.debug("REST request to get a page of Views");
        Page<Views> page = viewsRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /views/:id} : get the "id" views.
     *
     * @param id the id of the views to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the views, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/views/{id}")
    public ResponseEntity<Views> getViews(@PathVariable Long id) {
        log.debug("REST request to get Views : {}", id);
        Optional<Views> views = viewsRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(views);
    }

    /**
     * {@code DELETE  /views/:id} : delete the "id" views.
     *
     * @param id the id of the views to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/views/{id}")
    public ResponseEntity<Void> deleteViews(@PathVariable Long id) {
        log.debug("REST request to delete Views : {}", id);
        viewsRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
