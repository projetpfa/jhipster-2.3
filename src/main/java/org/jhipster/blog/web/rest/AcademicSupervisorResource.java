package org.jhipster.blog.web.rest;

import org.jhipster.blog.domain.AcademicSupervisor;
import org.jhipster.blog.service.AcademicSupervisorService;
import org.jhipster.blog.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.jhipster.blog.domain.AcademicSupervisor}.
 */
@RestController
@RequestMapping("/api")
public class AcademicSupervisorResource {

    private final Logger log = LoggerFactory.getLogger(AcademicSupervisorResource.class);

    private static final String ENTITY_NAME = "academicSupervisor";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AcademicSupervisorService academicSupervisorService;

    public AcademicSupervisorResource(AcademicSupervisorService academicSupervisorService) {
        this.academicSupervisorService = academicSupervisorService;
    }

    /**
     * {@code POST  /academic-supervisors} : Create a new academicSupervisor.
     *
     * @param academicSupervisor the academicSupervisor to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new academicSupervisor, or with status {@code 400 (Bad Request)} if the academicSupervisor has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/academic-supervisors")
    public ResponseEntity<AcademicSupervisor> createAcademicSupervisor(@Valid @RequestBody AcademicSupervisor academicSupervisor) throws URISyntaxException {
        log.debug("REST request to save AcademicSupervisor : {}", academicSupervisor);
        if (academicSupervisor.getId() != null) {
            throw new BadRequestAlertException("A new academicSupervisor cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AcademicSupervisor result = academicSupervisorService.save(academicSupervisor);
        return ResponseEntity.created(new URI("/api/academic-supervisors/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /academic-supervisors} : Updates an existing academicSupervisor.
     *
     * @param academicSupervisor the academicSupervisor to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated academicSupervisor,
     * or with status {@code 400 (Bad Request)} if the academicSupervisor is not valid,
     * or with status {@code 500 (Internal Server Error)} if the academicSupervisor couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/academic-supervisors")
    public ResponseEntity<AcademicSupervisor> updateAcademicSupervisor(@Valid @RequestBody AcademicSupervisor academicSupervisor) throws URISyntaxException {
        log.debug("REST request to update AcademicSupervisor : {}", academicSupervisor);
        if (academicSupervisor.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AcademicSupervisor result = academicSupervisorService.save(academicSupervisor);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, academicSupervisor.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /academic-supervisors} : get all the academicSupervisors.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of academicSupervisors in body.
     */
    @GetMapping("/academic-supervisors")
    public List<AcademicSupervisor> getAllAcademicSupervisors() {
        log.debug("REST request to get all AcademicSupervisors");
        return academicSupervisorService.findAll();
    }

    /**
     * {@code GET  /academic-supervisors/:id} : get the "id" academicSupervisor.
     *
     * @param id the id of the academicSupervisor to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the academicSupervisor, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/academic-supervisors/{id}")
    public ResponseEntity<AcademicSupervisor> getAcademicSupervisor(@PathVariable Long id) {
        log.debug("REST request to get AcademicSupervisor : {}", id);
        Optional<AcademicSupervisor> academicSupervisor = academicSupervisorService.findOne(id);
        return ResponseUtil.wrapOrNotFound(academicSupervisor);
    }

    /**
     * {@code DELETE  /academic-supervisors/:id} : delete the "id" academicSupervisor.
     *
     * @param id the id of the academicSupervisor to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/academic-supervisors/{id}")
    public ResponseEntity<Void> deleteAcademicSupervisor(@PathVariable Long id) {
        log.debug("REST request to delete AcademicSupervisor : {}", id);
        academicSupervisorService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
