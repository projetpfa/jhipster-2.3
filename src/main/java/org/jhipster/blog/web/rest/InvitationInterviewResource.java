package org.jhipster.blog.web.rest;

import org.jhipster.blog.domain.InvitationInterview;
import org.jhipster.blog.service.InvitationInterviewService;
import org.jhipster.blog.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.jhipster.blog.domain.InvitationInterview}.
 */
@RestController
@RequestMapping("/api")
public class InvitationInterviewResource {

    private final Logger log = LoggerFactory.getLogger(InvitationInterviewResource.class);

    private static final String ENTITY_NAME = "invitationInterview";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final InvitationInterviewService invitationInterviewService;

    public InvitationInterviewResource(InvitationInterviewService invitationInterviewService) {
        this.invitationInterviewService = invitationInterviewService;
    }

    /**
     * {@code POST  /invitation-interviews} : Create a new invitationInterview.
     *
     * @param invitationInterview the invitationInterview to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new invitationInterview, or with status {@code 400 (Bad Request)} if the invitationInterview has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/invitation-interviews")
    public ResponseEntity<InvitationInterview> createInvitationInterview(@Valid @RequestBody InvitationInterview invitationInterview) throws URISyntaxException {
        log.debug("REST request to save InvitationInterview : {}", invitationInterview);
        if (invitationInterview.getId() != null) {
            throw new BadRequestAlertException("A new invitationInterview cannot already have an ID", ENTITY_NAME, "idexists");
        }
        InvitationInterview result = invitationInterviewService.save(invitationInterview);
        return ResponseEntity.created(new URI("/api/invitation-interviews/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /invitation-interviews} : Updates an existing invitationInterview.
     *
     * @param invitationInterview the invitationInterview to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated invitationInterview,
     * or with status {@code 400 (Bad Request)} if the invitationInterview is not valid,
     * or with status {@code 500 (Internal Server Error)} if the invitationInterview couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/invitation-interviews")
    public ResponseEntity<InvitationInterview> updateInvitationInterview(@Valid @RequestBody InvitationInterview invitationInterview) throws URISyntaxException {
        log.debug("REST request to update InvitationInterview : {}", invitationInterview);
        if (invitationInterview.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        InvitationInterview result = invitationInterviewService.save(invitationInterview);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, invitationInterview.getId().toString()))
            .body(result);
    }



    /**
     * {@code GET  /invitation-interviews} : get all the invitationInterviews.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of invitationInterviews in body.
     */
    @GetMapping("/invitation-interviews")
    public ResponseEntity<List<InvitationInterview>> getAllInvitationInterviews(Pageable pageable) {
        log.debug("REST request to get a page of InvitationInterviews");
        Page<InvitationInterview> page = invitationInterviewService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /invitation-interviews/:id} : get the "id" invitationInterview.
     *
     * @param id the id of the invitationInterview to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the invitationInterview, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/invitation-interviews/{id}")
    public ResponseEntity<InvitationInterview> getInvitationInterview(@PathVariable Long id) {
        log.debug("REST request to get InvitationInterview : {}", id);
        Optional<InvitationInterview> invitationInterview = invitationInterviewService.findOne(id);
        return ResponseUtil.wrapOrNotFound(invitationInterview);
    }

    @GetMapping("/invitation-interviews/company")
    public ResponseEntity<List<InvitationInterview>> getInvitationInterviewsByCompanyAndConfirmation(Pageable pageable){
        log.debug("REST request to get page of confirmed InvitationsInterviews By Company Login");
        Page<InvitationInterview> page= invitationInterviewService.findByCompanyLoginAndConfirmation(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }



    /**
     * {@code DELETE  /invitation-interviews/:id} : delete the "id" invitationInterview.
     *
     * @param id the id of the invitationInterview to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/invitation-interviews/{id}")
    public ResponseEntity<Void> deleteInvitationInterview(@PathVariable Long id) {
        log.debug("REST request to delete InvitationInterview : {}", id);
        invitationInterviewService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/invitation-interviews/student")
    public ResponseEntity<List<InvitationInterview>> getInvitationInterviewsByLoginStudent(Pageable pageable){
        log.debug("REST request to get page of InvitationsInterviews of student");
        Page<InvitationInterview> page= invitationInterviewService.findInvitationsOfLoginStudent(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }

    @GetMapping("/invitation-interviews/nbrInvit")
    public Long getNumberInvitationInterviewsByLoginStudent(){
        log.debug("REST request to get number of InvitationsInterviews recieved for student");
       return invitationInterviewService.findNumberOfInvitationByLoginStudent();

    }
    @PutMapping("/invitation-interviews/update/{id}")
    public ResponseEntity<Void> updateConfirmation(@PathVariable Long id,@Valid @RequestBody String confirmation) throws URISyntaxException {
        log.debug("REST request to update InvitationInterview : {}", id);
        if (id == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        invitationInterviewService.updateConfirmation(id,confirmation);
        return ResponseEntity
        		.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }



}
