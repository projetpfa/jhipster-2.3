package org.jhipster.blog.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.jhipster.blog.domain.Internship;
import org.jhipster.blog.service.InternshipService;
import org.jhipster.blog.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link org.jhipster.blog.domain.Internship}.
 */
@RestController
@RequestMapping("/api")
public class InternshipResource {

	private final Logger log = LoggerFactory.getLogger(InternshipResource.class);

	private static final String ENTITY_NAME = "internship";

	@Value("${jhipster.clientApp.name}")
	private String applicationName;

	private final InternshipService internshipService;

	public InternshipResource(InternshipService internshipService) {
		this.internshipService = internshipService;
	}

	/**
	 * {@code POST  /internships} : Create a new internship.
	 *
	 * @param internship the internship to create.
	 * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
	 *         body the new internship, or with status {@code 400 (Bad Request)} if
	 *         the internship has already an ID.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PostMapping("/internships")
	public ResponseEntity<Internship> createInternship(@Valid @RequestBody Internship internship)
			throws URISyntaxException {
		log.debug("REST request to save Internship : {}", internship);
		if (internship.getId() != null) {
			throw new BadRequestAlertException("A new internship cannot already have an ID", ENTITY_NAME, "idexists");
		}
		Internship result = internshipService.save(internship);
		return ResponseEntity
				.created(new URI("/api/internships/" + result.getId())).headers(HeaderUtil
						.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
				.body(result);
	}

	/**
	 * {@code PUT  /internships} : Updates an existing internship.
	 *
	 * @param internship the internship to update.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the updated internship, or with status {@code 400 (Bad Request)} if
	 *         the internship is not valid, or with status
	 *         {@code 500 (Internal Server Error)} if the internship couldn't be
	 *         updated.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PutMapping("/internships")
	public ResponseEntity<Internship> updateInternship(@Valid @RequestBody Internship internship)
			throws URISyntaxException {
		log.debug("REST request to update Internship : {}", internship);
		if (internship.getId() == null) {
			throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
		}
		Internship result = internshipService.save(internship);
		return ResponseEntity.ok().headers(
				HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, internship.getId().toString()))
				.body(result);
	}

	/**
	 * {@code GET  /internships} : get all the internships.
	 *
	 * @param pageable the pagination information.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
	 *         of internships in body.
	 */
	@GetMapping("/internships")
	public ResponseEntity<List<Internship>> getAllInternships(Pageable pageable) {
		log.debug("REST request to get a page of Internships");
		Page<Internship> page = internshipService.findAll(pageable);
		HttpHeaders headers = PaginationUtil
				.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
		return ResponseEntity.ok().headers(headers).body(page.getContent());
	}

	/**
	 * {@code GET  /internships/:id} : get the "id" internship.
	 *
	 * @param id the id of the internship to retrieve.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the internship, or with status {@code 404 (Not Found)}.
	 */
	@GetMapping("/internships/{id}")
	public ResponseEntity<Internship> getInternship(@PathVariable Long id) {
		log.debug("REST request to get Internship : {}", id);
		Optional<Internship> internship = internshipService.findOne(id);
		return ResponseUtil.wrapOrNotFound(internship);
	}


	/**
	 * {@code DELETE  /internships/:id} : delete the "id" internship.
	 *
	 * @param id the id of the internship to delete.
	 * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
	 */
	@DeleteMapping("/internships/{id}")
	public ResponseEntity<Void> deleteInternship(@PathVariable Long id) {
		log.debug("REST request to delete Internship : {}", id);
		internshipService.delete(id);
		return ResponseEntity.noContent()
				.headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
				.build();
	}
	@GetMapping("/internships/company")
	public ResponseEntity<List<Internship>> getInternshipsByCompanyLogin(Pageable pageable) {
		log.debug("REST request to get page of Internships by company");
		Page<Internship> page = internshipService.findByCompanyLogin(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
		return ResponseEntity.ok().headers(headers).body(page.getContent());
	}

	@GetMapping("/internships/NbreInternship/Student")
	public Long getNumberInternshipsByLoginStudentDomain() {
		log.debug("REST request to get number of Internships by studentDomain");
		if (internshipService.findNumberInternshipsByLoginStudentDomain()!=0) {
			return internshipService.findNumberInternshipsByLoginStudentDomain();
		}
		else {
			return new Long(0) ;}

	}


	@GetMapping("/internships/AllInternships/Student")
	public  ResponseEntity<List<Internship>> getAllInternshipsByLoginStudentDomain(Pageable pageable) {
		log.debug("REST request to get all Internships by studentDomain");
		Page<Internship> page = internshipService.findByLoginStudentDomain(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
		return ResponseEntity.ok().headers(headers).body(page.getContent());
	}

    @GetMapping("internships/NbreInternship/Company")
    public Long getNumberInternshipsByLoginCompany(){
        log.debug("REST request to get number of Internships of company");
        if (internshipService.findNumberInternshipsByLoginCompany()!=0) {
			return internshipService.findNumberInternshipsByLoginCompany();
		}
		else {
			return new Long(0) ;}


    }

}
