package org.jhipster.blog.web.rest;

import org.jhipster.blog.domain.Interview;
import org.jhipster.blog.domain.enumeration.Choice;
import org.jhipster.blog.service.InterviewService;
import org.jhipster.blog.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import com.google.common.net.MediaType;

import java.io.Console;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.jhipster.blog.domain.Interview}.
 */
@RestController
@RequestMapping("/api")
public class InterviewResource {

    private final Logger log = LoggerFactory.getLogger(InterviewResource.class);

    private static final String ENTITY_NAME = "interview";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final InterviewService interviewService;

    public InterviewResource(InterviewService interviewService) {
        this.interviewService = interviewService;
    }

    /**
     * {@code POST  /interviews} : Create a new interview.
     *
     * @param interview the interview to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new interview, or with status {@code 400 (Bad Request)} if the interview has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/interviews")
    public ResponseEntity<Interview> createInterview(@Valid @RequestBody Interview interview) throws URISyntaxException {
        log.debug("REST request to save Interview : {}", interview);
        if (interview.getId() != null) {
            throw new BadRequestAlertException("A new interview cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Interview result = interviewService.save(interview);
        return ResponseEntity.created(new URI("/api/interviews/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /interviews} : Updates an existing interview.
     *
     * @param interview the interview to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated interview,
     * or with status {@code 400 (Bad Request)} if the interview is not valid,
     * or with status {@code 500 (Internal Server Error)} if the interview couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/interviews")
    public ResponseEntity<Interview> updateInterview(@Valid @RequestBody Interview interview) throws URISyntaxException {
        log.debug("REST request to update Interview : {}", interview);
        if (interview.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Interview result = interviewService.save(interview);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, interview.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /interviews} : get all the interviews.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of interviews in body.
     */
    @GetMapping("/interviews")
    public ResponseEntity<List<Interview>> getAllInterviews(Pageable pageable) {
        log.debug("REST request to get a page of Interviews");
        Page<Interview> page = interviewService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /interviews/:id} : get the "id" interview.
     *
     * @param id the id of the interview to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the interview, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/interviews/{id}")
    public ResponseEntity<Interview> getInterview(@PathVariable Long id) {
        log.debug("REST request to get Interview : {}", id);
        Optional<Interview> interview = interviewService.findOne(id);
        return ResponseUtil.wrapOrNotFound(interview);
    }

    @GetMapping("/interviews/company")
    ResponseEntity<List<Interview>> getInterviewsByCompany(Pageable pageable){
        log.debug("REST request to get page of Interviews By Company : {}");
        Page<Interview> page=interviewService.findByCompanyLogin(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());


    }

    @GetMapping("/interviews/student")
    ResponseEntity<List<Interview>> getInterviewsByStudent(Pageable pageable){
        log.debug("REST request to get page of Interviews By Student : {}");
        Page<Interview> page=interviewService.findByStudentLogin(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());


    }

    /**
     * {@code DELETE  /interviews/:id} : delete the "id" interview.
     *
     * @param id the id of the interview to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/interviews/{id}")
    public ResponseEntity<Void> deleteInterview(@PathVariable Long id) {
        log.debug("REST request to delete Interview : {}", id);
        interviewService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
    @GetMapping("/resultAffectation")
    ResponseEntity<List<Object[]>>getResultAffectation(Pageable pageable){
        log.debug("REST request to get page");
        Page<Object[]> page = interviewService.findFinalStudentChoiceByInternship(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());}


    
    @PutMapping("/interviews/refuse/{id}")
    public ResponseEntity<Void> RefuseAffectation(@PathVariable Long id,@Valid @RequestBody String studFinalChoice) throws URISyntaxException {
        
        log.debug("REST request to refuse affectation: {}", id);
        if (id == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
            interviewService.RefuseAffectation(id,studFinalChoice);
        
        
        return ResponseEntity
        		.noContent().headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @PutMapping("/interviews/accepte/{id}")
    public ResponseEntity<Void> AccepteAffectation(@PathVariable Long id,@Valid @RequestBody String studFinalChoice) throws URISyntaxException {
        
        log.debug("REST request to accepte affectation : {}", id);
        if (id == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
            interviewService.AccepteAffectation(id,studFinalChoice);
        
        
        return ResponseEntity
        		.noContent().headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
    @PutMapping("/interviews/failureRESULT/{id}")
    public ResponseEntity<Void> FailureInterview(@PathVariable Long id,@Valid @RequestBody String state) throws URISyntaxException {
        
        log.debug("REST request to make state interview failure : {}", id);
        if (id == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
            interviewService.FailureInterview(id, state);
        
        
        return ResponseEntity
        		.noContent().headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @PutMapping("/interviews/successRESULT/{id}")
    public ResponseEntity<Void> SuccessInterview(@PathVariable Long id,@Valid @RequestBody String state) throws URISyntaxException {
        
        log.debug("REST request make sate of interview success : {}", id);
        if (id == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
            interviewService.SuccessInterview(id, state);
        
        
        return ResponseEntity
        		.noContent().headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
