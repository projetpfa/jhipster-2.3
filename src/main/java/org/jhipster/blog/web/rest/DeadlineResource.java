package org.jhipster.blog.web.rest;

import org.jhipster.blog.domain.Deadline;
import org.jhipster.blog.service.DeadlineService;
import org.jhipster.blog.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.jhipster.blog.domain.Deadline}.
 */
@RestController
@RequestMapping("/api")
public class DeadlineResource {

    private final Logger log = LoggerFactory.getLogger(DeadlineResource.class);

    private static final String ENTITY_NAME = "deadline";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DeadlineService deadlineService;

    public DeadlineResource(DeadlineService deadlineService) {
        this.deadlineService = deadlineService;
    }

    /**
     * {@code POST  /deadlines} : Create a new deadline.
     *
     * @param deadline the deadline to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new deadline, or with status {@code 400 (Bad Request)} if the deadline has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/deadlines")
    public ResponseEntity<Deadline> createDeadline(@Valid @RequestBody Deadline deadline) throws URISyntaxException {
        log.debug("REST request to save Deadline : {}", deadline);
        if (deadline.getId() != null) {
            throw new BadRequestAlertException("A new deadline cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Deadline result = deadlineService.save(deadline);
        return ResponseEntity.created(new URI("/api/deadlines/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /deadlines} : Updates an existing deadline.
     *
     * @param deadline the deadline to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated deadline,
     * or with status {@code 400 (Bad Request)} if the deadline is not valid,
     * or with status {@code 500 (Internal Server Error)} if the deadline couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/deadlines")
    public ResponseEntity<Deadline> updateDeadline(@Valid @RequestBody Deadline deadline) throws URISyntaxException {
        log.debug("REST request to update Deadline : {}", deadline);
        if (deadline.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Deadline result = deadlineService.save(deadline);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, deadline.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /deadlines} : get all the deadlines.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of deadlines in body.
     */
    @GetMapping("/deadlines")
    public List<Deadline> getAllDeadlines() {
        log.debug("REST request to get all Deadlines");
        return deadlineService.findAll();
    }

    /**
     * {@code GET  /deadlines/:id} : get the "id" deadline.
     *
     * @param id the id of the deadline to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the deadline, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/deadlines/{id}")
    public ResponseEntity<Deadline> getDeadline(@PathVariable Long id) {
        log.debug("REST request to get Deadline : {}", id);
        Optional<Deadline> deadline = deadlineService.findOne(id);
        return ResponseUtil.wrapOrNotFound(deadline);
    }

    /**
     * {@code DELETE  /deadlines/:id} : delete the "id" deadline.
     *
     * @param id the id of the deadline to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/deadlines/{id}")
    public ResponseEntity<Void> deleteDeadline(@PathVariable Long id) {
        log.debug("REST request to delete Deadline : {}", id);
        deadlineService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
