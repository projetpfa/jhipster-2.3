package org.jhipster.blog.web.rest;

import org.jhipster.blog.domain.Company;
import org.jhipster.blog.domain.CompanyEvaluation;
import org.jhipster.blog.service.CompanyEvaluationService;
import org.jhipster.blog.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.jhipster.blog.domain.CompanyEvaluation}.
 */
@RestController
@RequestMapping("/api")
public class CompanyEvaluationResource {

    private final Logger log = LoggerFactory.getLogger(CompanyEvaluationResource.class);

    private static final String ENTITY_NAME = "companyEvaluation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CompanyEvaluationService companyEvaluationService;

    public CompanyEvaluationResource(CompanyEvaluationService companyEvaluationService) {
        this.companyEvaluationService = companyEvaluationService;
    }

    /**
     * {@code POST  /company-evaluations} : Create a new companyEvaluation.
     *
     * @param companyEvaluation the companyEvaluation to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new companyEvaluation, or with status {@code 400 (Bad Request)} if the companyEvaluation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/company-evaluations")
    public ResponseEntity<CompanyEvaluation> createCompanyEvaluation(@Valid @RequestBody CompanyEvaluation companyEvaluation) throws URISyntaxException {
        log.debug("REST request to save CompanyEvaluation : {}", companyEvaluation);
        if (companyEvaluation.getId() != null) {
            throw new BadRequestAlertException("A new companyEvaluation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CompanyEvaluation result = companyEvaluationService.save(companyEvaluation);
        return ResponseEntity.created(new URI("/api/company-evaluations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /company-evaluations} : Updates an existing companyEvaluation.
     *
     * @param companyEvaluation the companyEvaluation to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated companyEvaluation,
     * or with status {@code 400 (Bad Request)} if the companyEvaluation is not valid,
     * or with status {@code 500 (Internal Server Error)} if the companyEvaluation couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/company-evaluations")
    public ResponseEntity<CompanyEvaluation> updateCompanyEvaluation(@Valid @RequestBody CompanyEvaluation companyEvaluation) throws URISyntaxException {
        log.debug("REST request to update CompanyEvaluation : {}", companyEvaluation);
        if (companyEvaluation.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CompanyEvaluation result = companyEvaluationService.save(companyEvaluation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, companyEvaluation.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /company-evaluations} : get all the companyEvaluations.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of companyEvaluations in body.
     */
    @GetMapping("/company-evaluations")
    public ResponseEntity<List<CompanyEvaluation>> getAllCompanyEvaluations(Pageable pageable) {
        log.debug("REST request to get a page of CompanyEvaluations");
        Page<CompanyEvaluation> page = companyEvaluationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /company-evaluations/:id} : get the "id" companyEvaluation.
     *
     * @param id the id of the companyEvaluation to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the companyEvaluation, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/company-evaluations/{id}")
    public ResponseEntity<CompanyEvaluation> getCompanyEvaluation(@PathVariable Long id) {
        log.debug("REST request to get CompanyEvaluation : {}", id);
        Optional<CompanyEvaluation> companyEvaluation = companyEvaluationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(companyEvaluation);
    }

    /**
     * {@code DELETE  /company-evaluations/:id} : delete the "id" companyEvaluation.
     *
     * @param id the id of the companyEvaluation to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/company-evaluations/{id}")
    public ResponseEntity<Void> deleteCompanyEvaluation(@PathVariable Long id) {
        log.debug("REST request to delete CompanyEvaluation : {}", id);
        companyEvaluationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
    @GetMapping("/company-evaluation/{company}")
	public ResponseEntity<List<CompanyEvaluation>> getCompanyEvaluationByCompany(@PathVariable Company company,Pageable pageable) {
		log.debug("REST request to get page of CompanyEvaluations by company");
		Page<CompanyEvaluation> page = companyEvaluationService.findByCompany(company, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
		return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
