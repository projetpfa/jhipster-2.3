package org.jhipster.blog.service;

import org.jhipster.blog.domain.InvitationInterview;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link InvitationInterview}.
 */
public interface InvitationInterviewService {

    /**
     * Save a invitationInterview.
     *
     * @param invitationInterview the entity to save.
     * @return the persisted entity.
     */
    InvitationInterview save(InvitationInterview invitationInterview);

    /**
     * Get all the invitationInterviews.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<InvitationInterview> findAll(Pageable pageable);

    /**
     * Get the "id" invitationInterview.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<InvitationInterview> findOne(Long id);

    /**
     * Delete the "id" invitationInterview.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    Page<InvitationInterview> findByCompanyLoginAndConfirmation(Pageable pageable);
    
    Page<InvitationInterview> findInvitationsOfLoginStudent(Pageable pageable);
    
    Long findNumberOfInvitationByLoginStudent();
    
    void updateConfirmation(Long id,String confirmation);
    
}
