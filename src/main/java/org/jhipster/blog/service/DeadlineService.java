package org.jhipster.blog.service;

import org.jhipster.blog.domain.Deadline;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Deadline}.
 */
public interface DeadlineService {

    /**
     * Save a deadline.
     *
     * @param deadline the entity to save.
     * @return the persisted entity.
     */
    Deadline save(Deadline deadline);

    /**
     * Get all the deadlines.
     *
     * @return the list of entities.
     */
    List<Deadline> findAll();

    /**
     * Get the "id" deadline.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Deadline> findOne(Long id);

    /**
     * Delete the "id" deadline.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
