package org.jhipster.blog.service;

import java.util.Optional;
import org.jhipster.blog.domain.Application;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link Application}.
 */
public interface ApplicationService {

	/**
	 * Save a application.
	 *
	 * @param application the entity to save.
	 * @return the persisted entity.
	 */
	Application save(Application application);

	/**
	 * Get all the applications.
	 *
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	Page<Application> findAll(Pageable pageable);

	/**
	 * Get the "id" application.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	Optional<Application> findOne(Long id);

	/**
	 * Delete the "id" application.
	 *
	 * @param id the id of the entity.
	 */
	void delete(Long id);

	Page<Application> findByStudentLogin(Pageable pageable);

	/*Page<Application> findByInternshipAndCompanyLogin(Internship internship,Pageable pageable);*/

	Page<Object[]> findInternshipAndNbreOfApp( Pageable pageable);

	Page<Application> findByInternshipId(Long internship, Pageable pageable);

    Long findNumberOfApplicationByLoginStudent();

    Page<Application> findApplicationsOfLoginCompany(Pageable pageable);

    Long findNumberOfApplicationByLoginCompany();
}
