package org.jhipster.blog.service;

import org.jhipster.blog.domain.AcademicSupervisor;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link AcademicSupervisor}.
 */
public interface AcademicSupervisorService {

    /**
     * Save a academicSupervisor.
     *
     * @param academicSupervisor the entity to save.
     * @return the persisted entity.
     */
    AcademicSupervisor save(AcademicSupervisor academicSupervisor);

    /**
     * Get all the academicSupervisors.
     *
     * @return the list of entities.
     */
    List<AcademicSupervisor> findAll();

    /**
     * Get the "id" academicSupervisor.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AcademicSupervisor> findOne(Long id);

    /**
     * Delete the "id" academicSupervisor.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
