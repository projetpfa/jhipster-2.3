package org.jhipster.blog.service;

import org.jhipster.blog.domain.Company;
import org.jhipster.blog.domain.CompanyEvaluation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link CompanyEvaluation}.
 */
public interface CompanyEvaluationService {

    /**
     * Save a companyEvaluation.
     *
     * @param companyEvaluation the entity to save.
     * @return the persisted entity.
     */
    CompanyEvaluation save(CompanyEvaluation companyEvaluation);

    /**
     * Get all the companyEvaluations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CompanyEvaluation> findAll(Pageable pageable);

    /**
     * Get the "id" companyEvaluation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CompanyEvaluation> findOne(Long id);

    /**
     * Delete the "id" companyEvaluation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    Page<CompanyEvaluation> findByCompany(Company company, Pageable pageable);
}
