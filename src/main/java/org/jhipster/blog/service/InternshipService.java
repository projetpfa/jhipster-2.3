package org.jhipster.blog.service;

import java.util.Optional;

import org.jhipster.blog.domain.Internship;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link Internship}.
 */
public interface InternshipService {

    /**
     * Save a internship.
     *
     * @param internship the entity to save.
     * @return the persisted entity.
     */
    Internship save(Internship internship);

    /**
     * Get all the internships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Internship> findAll(Pageable pageable);

    /**
     * Get the "id" internship.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Internship> findOne(Long id);

    /**
     * Delete the "id" internship.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

	Page<Internship> findByCompanyLogin(Pageable pageable);

	Long findNumberInternshipsByLoginStudentDomain();

    Page<Internship> findByLoginStudentDomain (Pageable pageable);

    Long findNumberInternshipsByLoginCompany();


}
