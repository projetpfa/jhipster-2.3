package org.jhipster.blog.service.impl;

import org.jhipster.blog.service.InvitationInterviewService;
import org.jhipster.blog.domain.Company;
import org.jhipster.blog.domain.InvitationInterview;
import org.jhipster.blog.domain.Student;
import org.jhipster.blog.repository.CompanyRepository;
import org.jhipster.blog.repository.InvitationInterviewRepository;
import org.jhipster.blog.repository.StudentRepository;
import org.jhipster.blog.repository.UserRepository;
import org.jhipster.blog.security.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link InvitationInterview}.
 */
@Service
@Transactional
public class InvitationInterviewServiceImpl implements InvitationInterviewService {

    private final Logger log = LoggerFactory.getLogger(InvitationInterviewServiceImpl.class);

    private final InvitationInterviewRepository invitationInterviewRepository;

    

    private Company company;
    
    private Student student;

    private final UserRepository userRepository;

    private final CompanyRepository companyRepository;
    
    private final StudentRepository studentRepository;

    public InvitationInterviewServiceImpl(InvitationInterviewRepository invitationInterviewRepository,CompanyRepository companyRepository,UserRepository userRepository,StudentRepository studentRepository) {
        this.invitationInterviewRepository = invitationInterviewRepository;
        this.companyRepository = companyRepository;
		this.userRepository = userRepository;
		this.studentRepository=studentRepository;
    }

    /**
     * Save a invitationInterview.
     *
     * @param invitationInterview the entity to save.
     * @return the persisted entity.
     */
    @Override
    public InvitationInterview save(InvitationInterview invitationInterview) {


        SecurityUtils.getCurrentUserLogin()
		.flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			company = companyRepository.findCompanyByUser(user);
			invitationInterview.setCompany(company);
			 log.debug("Request to save InvitationInterview : {}", invitationInterview);
		});
        return invitationInterviewRepository.save(invitationInterview);
    }

    /**
     * Get all the invitationInterviews.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<InvitationInterview> findAll(Pageable pageable) {
        log.debug("Request to get all InvitationInterviews");
        return invitationInterviewRepository.findAll(pageable);
    }

    /**
     * Get one invitationInterview by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<InvitationInterview> findOne(Long id) {
        log.debug("Request to get InvitationInterview : {}", id);
        return invitationInterviewRepository.findById(id);
    }

    /**
     * Delete the invitationInterview by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete InvitationInterview : {}", id);
        invitationInterviewRepository.deleteById(id);
    }

    @Override
    public Page<InvitationInterview> findByCompanyLoginAndConfirmation(Pageable pageable){
        SecurityUtils.getCurrentUserLogin()
        .flatMap(userRepository::findOneByLogin).ifPresent(user->{
            company=companyRepository.findCompanyByUser(user);
            log.debug("Request to get all InvitationsInterviews by company and confirmation");
        });
        
        return invitationInterviewRepository.findByCompanyAndConfirmation(company,"true",pageable);
    }

	@Override
	public Page<InvitationInterview> findInvitationsOfLoginStudent(Pageable pageable) {
		SecurityUtils.getCurrentUserLogin()
		.flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			student = studentRepository.findStudentByUser(user);
			log.debug("Request to get all Invitations  of student: {}",student);});
		return invitationInterviewRepository.findInvitationByLoginStudent(student.getId(),pageable);}

	@Override
	public Long findNumberOfInvitationByLoginStudent() {
		SecurityUtils.getCurrentUserLogin()
		.flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			student = studentRepository.findStudentByUser(user);
			log.debug("Request to get all Invitations  of student: {}",student);});
		return invitationInterviewRepository.findNumberOfInvitationByLoginStudent(student.getId());
	}

	@Override
	public void updateConfirmation(Long id,String confirmation) {
		 log.debug("Request to answer InvitationsInterviews");
  
		 invitationInterviewRepository.updateConfirmation(id,confirmation);
	}
	
}

