package org.jhipster.blog.service.impl;

import java.util.Optional;

import org.jhipster.blog.domain.Application;
import org.jhipster.blog.domain.Company;
import org.jhipster.blog.domain.Student;
import org.jhipster.blog.repository.ApplicationRepository;
import org.jhipster.blog.repository.CompanyRepository;
import org.jhipster.blog.repository.StudentRepository;
import org.jhipster.blog.repository.UserRepository;
import org.jhipster.blog.security.SecurityUtils;
import org.jhipster.blog.service.ApplicationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Application}.
 */
@Service
@Transactional
public class ApplicationServiceImpl implements ApplicationService {

	private final Logger log = LoggerFactory.getLogger(ApplicationServiceImpl.class);
	private Student student;
	private Company company;
	private final ApplicationRepository applicationRepository;
	private final UserRepository userRepository;
	private final StudentRepository studentRepository;
	private final CompanyRepository companyRepository;
	public ApplicationServiceImpl(ApplicationRepository applicationRepository,StudentRepository studentRepository, CompanyRepository  companyRepository ,UserRepository userRepository) {
		this.studentRepository = studentRepository;
		this.companyRepository = companyRepository;
		this.userRepository = userRepository;
		this.applicationRepository = applicationRepository;
	}

	/**
	 * Save a application.
	 *
	 * @param application the entity to save.
	 * @return the persisted entity.
	 */
	@Override
	public Application save(Application application) {

		SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			student = studentRepository.findStudentByUser(user);
			application.setStudent(student);
			log.debug("Request to save Application : {}", application);
		});
		return applicationRepository.save(application);
	}

	/**
	 * Get all the applications.
	 *
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<Application> findAll(Pageable pageable) {
		log.debug("Request to get all Applications");
		return applicationRepository.findAll(pageable);
	}

	/**
	 * Get one application by id.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	@Override
	@Transactional(readOnly = true)
	public Optional<Application> findOne(Long id) {
		log.debug("Request to get Application : {}", id);
		return applicationRepository.findById(id);
	}

	/**
	 * Delete the application by id.
	 *
	 * @param id the id of the entity.
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete Application : {}", id);
		applicationRepository.deleteById(id);
    }

    @Override
	public Page<Application> findByStudentLogin(Pageable pageable) {

		SecurityUtils.getCurrentUserLogin()
		.flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			student = studentRepository.findStudentByUser(user);
			log.debug("Request to get all Internships by company : {}", student);
		});
        return applicationRepository.findByStudent(student,pageable);
	}

	@Override
	public Page<Application> findByInternshipId(Long internship, Pageable pageable) {
		log.debug("Request to get all Applications for an Internship: {}", internship);
		return applicationRepository.findByInternshipId(internship, pageable);
	}



	@Override
	public Page<Object[]> findInternshipAndNbreOfApp(Pageable pageable) {
		SecurityUtils.getCurrentUserLogin()
		.flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			company = companyRepository.findCompanyByUser(user);

			log.debug("Request to get all Internships and nbre of application by company : {}",company);
		});
		return applicationRepository.findInternshipAndNbreOfApp(company.getId(),pageable);}

	@Override
	public Long findNumberOfApplicationByLoginStudent() {
		SecurityUtils.getCurrentUserLogin()
		.flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			student = studentRepository.findStudentByUser(user);
			log.debug("Request to get number Invitations  of student: {}",student);});
		return applicationRepository.findNumberOfApplicationByLoginStudent(student.getId());
	}

    @Override
    public Page<Application> findApplicationsOfLoginCompany(Pageable pageable) {
        SecurityUtils.getCurrentUserLogin()
		.flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			company = companyRepository.findCompanyByUser(user);
			log.debug("Request to get all Applications  of application: {}",company);});
        return applicationRepository.findApplicationByLoginCompany(company.getId(), pageable);
    }

    @Override
    public Long findNumberOfApplicationByLoginCompany() {
        SecurityUtils.getCurrentUserLogin()
		.flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			company = companyRepository.findCompanyByUser(user);
            log.debug("Request to get number Applications  of company: {}",company);});
        return applicationRepository.findNumberOfApplicationByLoginCompany(company.getId());

    }


}
