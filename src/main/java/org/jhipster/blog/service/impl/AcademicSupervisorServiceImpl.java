package org.jhipster.blog.service.impl;

import org.jhipster.blog.service.AcademicSupervisorService;
import org.jhipster.blog.domain.AcademicSupervisor;
import org.jhipster.blog.repository.AcademicSupervisorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link AcademicSupervisor}.
 */
@Service
@Transactional
public class AcademicSupervisorServiceImpl implements AcademicSupervisorService {

    private final Logger log = LoggerFactory.getLogger(AcademicSupervisorServiceImpl.class);

    private final AcademicSupervisorRepository academicSupervisorRepository;

    public AcademicSupervisorServiceImpl(AcademicSupervisorRepository academicSupervisorRepository) {
        this.academicSupervisorRepository = academicSupervisorRepository;
    }

    /**
     * Save a academicSupervisor.
     *
     * @param academicSupervisor the entity to save.
     * @return the persisted entity.
     */
    @Override
    public AcademicSupervisor save(AcademicSupervisor academicSupervisor) {
        log.debug("Request to save AcademicSupervisor : {}", academicSupervisor);
        return academicSupervisorRepository.save(academicSupervisor);
    }

    /**
     * Get all the academicSupervisors.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<AcademicSupervisor> findAll() {
        log.debug("Request to get all AcademicSupervisors");
        return academicSupervisorRepository.findAll();
    }

    /**
     * Get one academicSupervisor by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AcademicSupervisor> findOne(Long id) {
        log.debug("Request to get AcademicSupervisor : {}", id);
        return academicSupervisorRepository.findById(id);
    }

    /**
     * Delete the academicSupervisor by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AcademicSupervisor : {}", id);
        academicSupervisorRepository.deleteById(id);
    }
}
