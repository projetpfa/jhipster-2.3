package org.jhipster.blog.service.impl;

import java.util.List;
import java.util.Optional;

import org.jhipster.blog.domain.Company;
import org.jhipster.blog.domain.User;
import org.jhipster.blog.repository.CompanyRepository;
import org.jhipster.blog.repository.UserRepository;
import org.jhipster.blog.security.SecurityUtils;
import org.jhipster.blog.service.CompanyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



/**
 * Service Implementation for managing {@link Company}.
 */
@Service
@Transactional
public class CompanyServiceImpl implements CompanyService {

	private final Logger log = LoggerFactory.getLogger(CompanyServiceImpl.class);

	private final CompanyRepository companyRepository;
	private final UserRepository userRepository;

	public CompanyServiceImpl(CompanyRepository companyRepository,UserRepository userRepository) {
		this.userRepository = userRepository;
		this.companyRepository = companyRepository;
	}

	/**
	 * Save a company.
	 *
	 * @param company the entity to save.
	 * @return the persisted entity.
	 */
	@Override
	public Company save(Company company) {
		SecurityUtils.getCurrentUserLogin()
		.flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			company.setUser(user);
			log.debug("Request to save Company : {}", company);

		});
		return companyRepository.save(company);
	}

	/**
	 * Get all the companies.
	 *
	 * @return the list of entities.
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Company> findAll() {
		log.debug("Request to get all Companies");
		return companyRepository.findAll();
	}

	/**
	 * Get one company by id.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	@Override
	@Transactional(readOnly = true)
	public Optional<Company> findOne(Long id) {
		log.debug("Request to get Company : {}", id);
		return companyRepository.findById(id);
	}

	/**
	 * Delete the company by id.
	 *
	 * @param id the id of the entity.
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete Company : {}", id);
		companyRepository.deleteById(id);
	}
	@Override
	public boolean existsCompanyByUser(User user) {

		return companyRepository. existsCompanyByUser(user);
	}
	@Override
	public Company  findCompanyByUser(User user) {
	
		
		return companyRepository. findCompanyByUser(user);
	}
}
