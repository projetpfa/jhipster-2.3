package org.jhipster.blog.service.impl;

import org.jhipster.blog.service.AffectationService;
import org.jhipster.blog.domain.Affectation;
import org.jhipster.blog.domain.Company;
import org.jhipster.blog.domain.Student;
import org.jhipster.blog.repository.AffectationRepository;
import org.jhipster.blog.repository.CompanyRepository;
import org.jhipster.blog.repository.StudentRepository;
import org.jhipster.blog.repository.UserRepository;
import org.jhipster.blog.security.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Affectation}.
 */
@Service
@Transactional
public class AffectationServiceImpl implements AffectationService {

    private final Logger log = LoggerFactory.getLogger(AffectationServiceImpl.class);

    private final AffectationRepository affectationRepository;

    private final UserRepository userRepository;

    private final CompanyRepository companyRepository;

    private Company company;

    private Student student;

    private final StudentRepository studentRepository;

    public AffectationServiceImpl(AffectationRepository affectationRepository,CompanyRepository  companyRepository ,UserRepository userRepository, StudentRepository studentRepository) {
        this.affectationRepository = affectationRepository;
        this.userRepository = userRepository;
        this.companyRepository = companyRepository;
        this.studentRepository=studentRepository;

    }

    /**
     * Save a affectation.
     *
     * @param affectation the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Affectation save(Affectation affectation) {
         SecurityUtils.getCurrentUserLogin()
		.flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			student = studentRepository.findStudentByUser(user);
			affectation.setStudent(student);
		}); 
        log.debug("Request to save Affectation : {}", affectation);
        return affectationRepository.save(affectation);
    }

    @Override
    public Affectation savePeriod(Affectation affectation) {

        log.debug("Request to save Affectation : {}", affectation);
        return affectationRepository.save(affectation);
    }

    /**
     * Get all the affectations.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<Affectation> findAll() {
        log.debug("Request to get all Affectations");
        return affectationRepository.findAll();
    }

    /**
     * Get one affectation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Affectation> findOne(Long id) {
        log.debug("Request to get Affectation : {}", id);
        return affectationRepository.findById(id);
    }

    /**
     * Delete the affectation by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Affectation : {}", id);
        affectationRepository.deleteById(id);
    }

    @Override
    public Long findNumberOfAffectationByLoginCompany() {
        SecurityUtils.getCurrentUserLogin()
		.flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			company = companyRepository.findCompanyByUser(user);
            log.debug("Request to get number Applications  of company: {}",company);});
        return affectationRepository.findNumberOfAffectationByLoginCompany(company.getId());
    }

    @Override
	public Page<Affectation> findByStudent(Student student ,Pageable pageable) {

			log.debug("Request to get all Internships by company : {}", student);

        return affectationRepository.findByStudent(student,pageable);
    }

	@Override
	public Page<Affectation> findByInternshipId(Long internship, Pageable pageable) {
		log.debug("Request to get all Affectations for an Internship: {}", internship);
		return affectationRepository.findByInternshipId(internship, pageable);
    }
    
	@Override
	public Page<Object[]> findInternshipAndNbreOfAffectation(Pageable pageable) {
		SecurityUtils.getCurrentUserLogin()
		.flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			company = companyRepository.findCompanyByUser(user);

			log.debug("Request to get all Internships and nbre of affectation by company : {}",company);
		});
        return affectationRepository.findInternshipAndNbreOfAffectation(company.getId(),pageable);}
    
    @Override
	public Page<Affectation> findAffectationOfLoginCompany(Pageable pageable) {
		SecurityUtils.getCurrentUserLogin()
		.flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			company = companyRepository.findCompanyByUser(user);
			log.debug("Request to get all Affectations  of company: {}",company);});
		return affectationRepository.findAffectationByCompany(company.getId(), pageable);
    }

}
