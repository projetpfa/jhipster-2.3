package org.jhipster.blog.service.impl;

import org.jhipster.blog.service.CompanyEvaluationService;
import org.jhipster.blog.domain.Company;
import org.jhipster.blog.domain.CompanyEvaluation;
import org.jhipster.blog.repository.CompanyEvaluationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link CompanyEvaluation}.
 */
@Service
@Transactional
public class CompanyEvaluationServiceImpl implements CompanyEvaluationService {

    private final Logger log = LoggerFactory.getLogger(CompanyEvaluationServiceImpl.class);

    private final CompanyEvaluationRepository companyEvaluationRepository;

    public CompanyEvaluationServiceImpl(CompanyEvaluationRepository companyEvaluationRepository) {
        this.companyEvaluationRepository = companyEvaluationRepository;
    }

    /**
     * Save a companyEvaluation.
     *
     * @param companyEvaluation the entity to save.
     * @return the persisted entity.
     */
    @Override
    public CompanyEvaluation save(CompanyEvaluation companyEvaluation) {
        log.debug("Request to save CompanyEvaluation : {}", companyEvaluation);
        return companyEvaluationRepository.save(companyEvaluation);
    }

    /**
     * Get all the companyEvaluations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CompanyEvaluation> findAll(Pageable pageable) {
        log.debug("Request to get all CompanyEvaluations");
        return companyEvaluationRepository.findAll(pageable);
    }

    /**
     * Get one companyEvaluation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<CompanyEvaluation> findOne(Long id) {
        log.debug("Request to get CompanyEvaluation : {}", id);
        return companyEvaluationRepository.findById(id);
    }

    /**
     * Delete the companyEvaluation by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete CompanyEvaluation : {}", id);
        companyEvaluationRepository.deleteById(id);
    }
    @Override
	public Page<CompanyEvaluation> findByCompany(Company company ,Pageable pageable) {

			log.debug("Request to get all CompanuEvaluations by company : {}", company);

        return companyEvaluationRepository.findByCompany(company,pageable);
    }
}
