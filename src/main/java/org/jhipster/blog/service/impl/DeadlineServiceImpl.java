package org.jhipster.blog.service.impl;

import org.jhipster.blog.service.DeadlineService;
import org.jhipster.blog.domain.Deadline;
import org.jhipster.blog.repository.DeadlineRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Deadline}.
 */
@Service
@Transactional
public class DeadlineServiceImpl implements DeadlineService {

    private final Logger log = LoggerFactory.getLogger(DeadlineServiceImpl.class);

    private final DeadlineRepository deadlineRepository;

    public DeadlineServiceImpl(DeadlineRepository deadlineRepository) {
        this.deadlineRepository = deadlineRepository;
    }

    /**
     * Save a deadline.
     *
     * @param deadline the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Deadline save(Deadline deadline) {
        log.debug("Request to save Deadline : {}", deadline);
        return deadlineRepository.save(deadline);
    }

    /**
     * Get all the deadlines.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<Deadline> findAll() {
        log.debug("Request to get all Deadlines");
        return deadlineRepository.findAll();
    }

    /**
     * Get one deadline by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Deadline> findOne(Long id) {
        log.debug("Request to get Deadline : {}", id);
        return deadlineRepository.findById(id);
    }

    /**
     * Delete the deadline by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Deadline : {}", id);
        deadlineRepository.deleteById(id);
    }
}
