package org.jhipster.blog.service.impl;

import org.jhipster.blog.service.StudentEvaluationService;
import org.jhipster.blog.domain.Student;
import org.jhipster.blog.domain.StudentEvaluation;
import org.jhipster.blog.repository.StudentEvaluationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link StudentEvaluation}.
 */
@Service
@Transactional
public class StudentEvaluationServiceImpl implements StudentEvaluationService {

    private final Logger log = LoggerFactory.getLogger(StudentEvaluationServiceImpl.class);

    private final StudentEvaluationRepository studentEvaluationRepository;

    public StudentEvaluationServiceImpl(StudentEvaluationRepository studentEvaluationRepository) {
        this.studentEvaluationRepository = studentEvaluationRepository;
    }

    /**
     * Save a studentEvaluation.
     *
     * @param studentEvaluation the entity to save.
     * @return the persisted entity.
     */
    @Override
    public StudentEvaluation save(StudentEvaluation studentEvaluation) {
        log.debug("Request to save StudentEvaluation : {}", studentEvaluation);
        return studentEvaluationRepository.save(studentEvaluation);
    }

    /**
     * Get all the studentEvaluations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<StudentEvaluation> findAll(Pageable pageable) {
        log.debug("Request to get all StudentEvaluations");
        return studentEvaluationRepository.findAll(pageable);
    }

    /**
     * Get one studentEvaluation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<StudentEvaluation> findOne(Long id) {
        log.debug("Request to get StudentEvaluation : {}", id);
        return studentEvaluationRepository.findById(id);
    }

    /**
     * Delete the studentEvaluation by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete StudentEvaluation : {}", id);
        studentEvaluationRepository.deleteById(id);
    }
    @Override
	public Page<StudentEvaluation> findByStudent(Student student ,Pageable pageable) {

			log.debug("Request to get all StudentEvaluation by student : {}", student);

        return studentEvaluationRepository.findByStudent(student,pageable);
    }
}
