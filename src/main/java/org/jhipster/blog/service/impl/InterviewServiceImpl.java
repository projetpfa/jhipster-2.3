package org.jhipster.blog.service.impl;

import org.jhipster.blog.service.InterviewService;
import org.jhipster.blog.domain.Interview;
import org.jhipster.blog.domain.Student;
import org.jhipster.blog.domain.enumeration.Choice;
import org.jhipster.blog.repository.InterviewRepository;
import org.jhipster.blog.repository.StudentRepository;
import org.jhipster.blog.security.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jhipster.blog.domain.Company;
import org.jhipster.blog.repository.UserRepository;
import org.jhipster.blog.repository.CompanyRepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import javax.validation.Valid;

/**
 * Service Implementation for managing {@link Interview}.
 */
@Service
@Transactional
public class InterviewServiceImpl implements InterviewService {

    private final Logger log = LoggerFactory.getLogger(InterviewServiceImpl.class);

    private final InterviewRepository interviewRepository;

    private Company company;

    private Student student;

    private final UserRepository userRepository;

    private final CompanyRepository companyRepository;

    private final StudentRepository studentRepository;

    public InterviewServiceImpl(InterviewRepository interviewRepository, CompanyRepository companyRepository,
            UserRepository userRepository, StudentRepository studentRepository) {
        this.interviewRepository = interviewRepository;
        this.companyRepository = companyRepository;
        this.userRepository = userRepository;
        this.studentRepository = studentRepository;
    }

    /**
     * Save a interview.
     *
     * @param interview the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Interview save(Interview interview) {
        log.debug("Request to save Interview : {}", interview);
        return interviewRepository.save(interview);
    }

    /**
     * Get all the interviews.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Interview> findAll(Pageable pageable) {
        log.debug("Request to get all Interviews");
        return interviewRepository.findAll(pageable);
    }

    /**
     * Get one interview by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Interview> findOne(Long id) {
        log.debug("Request to get Interview : {}", id);
        return interviewRepository.findById(id);
    }

    /**
     * Delete the interview by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Interview : {}", id);
        interviewRepository.deleteById(id);
    }

    @Override
    public Page<Interview> findByCompanyLogin(Pageable pageable) {

        SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).ifPresent(user -> {
            company = companyRepository.findCompanyByUser(user);
            log.debug("Request to get all Internships by company : {}", company);
        });
        return interviewRepository.findByCompany(company, pageable);
    }

    @Override
    public Page<Interview> findByStudentLogin(Pageable pageable) {
        SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).ifPresent(user -> {
            student = studentRepository.findStudentByUser(user);
            log.debug("Request to get all Internships by student : {}", student);
        });
        return interviewRepository.findByStudent(student, pageable);
    }

    @Override
    public void RefuseAffectation(Long id,  String studFinalChoice) {
        log.debug("Request to RefuseAffectation");
  
		 interviewRepository.RefuseAffectation(id);

    }

    @Override
    public void AccepteAffectation(Long id, @Valid String studFinalChoice) {
        log.debug("Request to Accepte Affectation");
  
		 interviewRepository.AccepteAffectation(id);

    }
    

	@Override
	public Page<Object[]> findFinalStudentChoiceByInternship(Pageable pageable) {
		SecurityUtils.getCurrentUserLogin()
		.flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			company = companyRepository.findCompanyByUser(user);
			log.debug("Request to get response of all successful student by Internship");
		});
		return interviewRepository.findFinalStudentChoiceByInternship(company.getId(),pageable);
	}

	@Override
	public void SuccessInterview(Long id, @Valid String state) {
		 log.debug("Request to make state of interview as success ");
		  
		 interviewRepository.SuccessInterview(id);

		
	}

	@Override
	public void FailureInterview(Long id, @Valid String state) {
		 log.debug("Request to make state of interview as failure ");
		  
		 interviewRepository.FailureInterview(id);

		
	}
	
    
}

