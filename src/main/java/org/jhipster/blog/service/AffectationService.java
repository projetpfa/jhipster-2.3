package org.jhipster.blog.service;

import org.jhipster.blog.domain.Affectation;
import org.jhipster.blog.domain.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Affectation}.
 */
public interface AffectationService {

    /**
     * Save a affectation.
     *
     * @param affectation the entity to save.
     * @return the persisted entity.
     */
    Affectation save(Affectation affectation);

    /**
     * Get all the affectations.
     *
     * @return the list of entities.
     */
    List<Affectation> findAll();

    /**
     * Get the "id" affectation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Affectation> findOne(Long id);

    /**
     * Delete the "id" affectation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    Long findNumberOfAffectationByLoginCompany();


    Page<Affectation> findByStudent(Student student, Pageable pageable);
    Page<Affectation> findByInternshipId(Long internship, Pageable pageable);
    Page<Object[]> findInternshipAndNbreOfAffectation( Pageable pageable);
    Page<Affectation> findAffectationOfLoginCompany(Pageable pageable);
     
    Affectation savePeriod(Affectation affectation) ;
}
