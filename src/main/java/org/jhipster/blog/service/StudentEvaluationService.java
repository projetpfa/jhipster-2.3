package org.jhipster.blog.service;

import org.jhipster.blog.domain.Student;
import org.jhipster.blog.domain.StudentEvaluation;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link StudentEvaluation}.
 */
public interface StudentEvaluationService {

    /**
     * Save a studentEvaluation.
     *
     * @param studentEvaluation the entity to save.
     * @return the persisted entity.
     */
    StudentEvaluation save(StudentEvaluation studentEvaluation);

    /**
     * Get all the studentEvaluations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<StudentEvaluation> findAll(Pageable pageable);

    /**
     * Get the "id" studentEvaluation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<StudentEvaluation> findOne(Long id);

    /**
     * Delete the "id" studentEvaluation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    Page<StudentEvaluation> findByStudent(Student student, Pageable pageable);
}
