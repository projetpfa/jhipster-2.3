package org.jhipster.blog.service;

import org.jhipster.blog.domain.Student;
import org.jhipster.blog.domain.User;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Student}.
 */
public interface StudentService {

    /**
     * Save a student.
     *
     * @param student the entity to save.
     * @return the persisted entity.
     */
    Student save(Student student);

    /**
     * Get all the students.
     *
     * @return the list of entities.
     */
    List<Student> findAll();

    /**
     * Get the "id" student.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Student> findOne(Long id);

    /**
     * Delete the "id" student.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    boolean existsStudentByUser(User user);
    Student findStudentByUser(User user);
}

