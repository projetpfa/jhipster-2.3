package org.jhipster.blog.repository;

import org.jhipster.blog.domain.Affectation;
import org.jhipster.blog.domain.Student;
import org.jhipster.blog.domain.StudentEvaluation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the StudentEvaluation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StudentEvaluationRepository extends JpaRepository<StudentEvaluation, Long> {
    Page<StudentEvaluation> findByStudent(Student student,Pageable pageable);
}
