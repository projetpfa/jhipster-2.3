package org.jhipster.blog.repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.EnumType;

import org.jhipster.blog.domain.Company;
import org.jhipster.blog.domain.Internship;
import org.jhipster.blog.domain.User;
import org.jhipster.blog.domain.enumeration.Domain;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Internship entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InternshipRepository extends JpaRepository<Internship, Long> {
	 Page<Internship> findByCompany(Company company, Pageable pageable);

	 @Query("select internship from Internship internship where internship.domain= :domain")
	 Page<Internship> findByLoginStudentDomain(@Param("domain") Domain domain, Pageable pageable);

	 @Query("select count(internship) from Internship internship where internship.domain= :domain")
     Long findNumberInternshipsByLoginStudentDomain(@Param("domain") Domain domain );

     @Query("select count(internship) from Internship internship where internship.company.id = :company")
     Long findNumberOfInternshipByLoginCompany(@Param("company") Long company);
}
