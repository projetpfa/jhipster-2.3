package org.jhipster.blog.repository;

import org.jhipster.blog.domain.InvitationInterview;
import org.jhipster.blog.domain.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.transaction.Transactional;

import org.jhipster.blog.domain.Application;
import org.jhipster.blog.domain.Company;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the InvitationInterview entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InvitationInterviewRepository extends JpaRepository<InvitationInterview, Long> {
    Page<InvitationInterview> findByCompanyAndConfirmation(Company company,String confirmation,Pageable pageable);
    
    @Transactional
    @Modifying
	@Query("update InvitationInterview invitInterview set invitInterview.confirmation = :confirmation where invitInterview.id= :id")
	void updateConfirmation(@Param("id")Long id,@Param("confirmation") String confirmation);
	   		   
		
    @Query("select invit from InvitationInterview invit where invit.application.student.id = :student")
    Page<InvitationInterview> findInvitationByLoginStudent(@Param("student") Long student,Pageable pageable);
    
    
    @Query("select count(invit) from InvitationInterview invit where invit.application.student.id = :student")
    Long findNumberOfInvitationByLoginStudent(@Param("student") Long student);
    
    
  
    
    
   
}
