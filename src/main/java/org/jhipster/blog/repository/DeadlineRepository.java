package org.jhipster.blog.repository;

import org.jhipster.blog.domain.Deadline;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Deadline entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DeadlineRepository extends JpaRepository<Deadline, Long> {
}
