package org.jhipster.blog.repository;

import org.jhipster.blog.domain.Affectation;


import org.jhipster.blog.domain.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Affectation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AffectationRepository extends JpaRepository<Affectation, Long> {

    @Query("select count(affectation) from Affectation affectation where affectation.internship.company.id = :company")
    Long findNumberOfAffectationByLoginCompany(@Param("company") Long company);

    @Query("select aff.internship As INTERSHIP_ID , count(aff) As NBR_AFF FROM Affectation aff Group By aff.internship Having aff.internship.id in (select inter.id FROM Internship inter where inter.company.id = :company) ORDER BY NBR_AFF DESC")
    Page<Object[]>findInternshipAndNbreOfAffectation(@Param("company")Long company,Pageable pageable);

   
    
    @Query("select aff from Affectation aff where aff.internship.company.id = :company")
    Page<Affectation> findAffectationByCompany(@Param("company") Long company,Pageable pageable);

    Page<Affectation> findByStudent(Student student,Pageable pageable);
    Page<Affectation> findByInternshipId(Long internship, Pageable pageable);

}


