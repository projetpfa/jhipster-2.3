package org.jhipster.blog.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.jhipster.blog.domain.Company;
import org.jhipster.blog.domain.Interview;
import org.jhipster.blog.domain.Student;
import org.jhipster.blog.domain.User;
import org.jhipster.blog.domain.enumeration.Choice;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Interview entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InterviewRepository extends JpaRepository<Interview, Long> {
    Page<Interview> findByCompany(Company company,Pageable pageable);

	Page<Interview> findByStudent(Student student, Pageable pageable);
	@Query("select interv.internship,interv.student,interv.studFinalChoice,interv from Interview interv where ((interv.company.id= :company))")
	Page<Object[]> findFinalStudentChoiceByInternship(@Param("company") Long company, Pageable pageable);

    @Transactional
    @Modifying
	@Query("update Interview interview set interview.studFinalChoice = 'REFUSED' where interview.id= :id")
    void RefuseAffectation(@Param("id")Long id);

    @Transactional
    @Modifying
    
	@Query("update Interview interview set interview.studFinalChoice = 'ACCEPTED' where interview.id= :id")
    void AccepteAffectation(@Param("id")Long id);
	
    @Transactional
    @Modifying
	@Query("update Interview interview set interview.state = 'FAILURE' where interview.id= :id")
    void FailureInterview(@Param("id")Long id);

    @Transactional
    @Modifying  
	@Query("update Interview interview set interview.state = 'SUCCESS' where interview.id= :id")
    void SuccessInterview(@Param("id")Long id);
	

}
