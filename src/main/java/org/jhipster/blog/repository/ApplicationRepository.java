package org.jhipster.blog.repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.jhipster.blog.domain.Student;
import org.jhipster.blog.domain.Application;
import org.jhipster.blog.domain.User;
import org.jhipster.blog.domain.Application;
import org.jhipster.blog.domain.Company;
import org.jhipster.blog.domain.Internship;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Application entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ApplicationRepository extends JpaRepository<Application, Long> {
    Page<Application> findByStudent(Student student,Pageable pageable);

    Page<Application> findByInternshipId(Long internship, Pageable pageable);

    @Query("select app from Application app where app.internship.company.id = :company")
    Page<Application>findApplicationByLoginCompany(@Param("company") Long company,Pageable pageable);

	@Query("select app.internship As INTERSHIP_ID , count(app) As NBR_APP FROM Application app Group By app.internship Having app.internship.id in (select inter.id FROM Internship inter where inter.company.id = :company) ORDER BY NBR_APP DESC")
	Page<Object[]>findInternshipAndNbreOfApp(@Param("company")Long company,Pageable pageable);

	@Query("select count(app) from Application app where app.student.id = :student")
    Long findNumberOfApplicationByLoginStudent(@Param("student") Long student);

    @Query("select count(app) from Application app where app.internship.company.id = :company")
    Long findNumberOfApplicationByLoginCompany(@Param("company") Long company);


}
