package org.jhipster.blog.repository;

import org.jhipster.blog.domain.Company;
import org.jhipster.blog.domain.Student;
import org.jhipster.blog.domain.User;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Student entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
	Student findStudentByUser (User user);
	Boolean existsStudentByUser (User user);
}
