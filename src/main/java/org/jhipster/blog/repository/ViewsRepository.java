package org.jhipster.blog.repository;

import org.jhipster.blog.domain.Views;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Views entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ViewsRepository extends JpaRepository<Views, Long> {
}
