package org.jhipster.blog.repository;

import org.jhipster.blog.domain.Company;
import org.jhipster.blog.domain.CompanyEvaluation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CompanyEvaluation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CompanyEvaluationRepository extends JpaRepository<CompanyEvaluation, Long> {
    Page<CompanyEvaluation> findByCompany(Company company,Pageable pageable);
}
