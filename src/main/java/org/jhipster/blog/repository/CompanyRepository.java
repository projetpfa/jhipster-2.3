package org.jhipster.blog.repository;

import org.jhipster.blog.domain.Company;
import org.jhipster.blog.domain.User;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Company entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {
	Company findCompanyByUser (User user);
    Boolean existsCompanyByUser (User user);
}
