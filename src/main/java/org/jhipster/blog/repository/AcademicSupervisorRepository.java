package org.jhipster.blog.repository;

import org.jhipster.blog.domain.AcademicSupervisor;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the AcademicSupervisor entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AcademicSupervisorRepository extends JpaRepository<AcademicSupervisor, Long> {
}
