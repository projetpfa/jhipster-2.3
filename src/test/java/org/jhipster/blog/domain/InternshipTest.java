package org.jhipster.blog.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.jhipster.blog.web.rest.TestUtil;

public class InternshipTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Internship.class);
        Internship internship1 = new Internship();
        internship1.setId(1L);
        Internship internship2 = new Internship();
        internship2.setId(internship1.getId());
        assertThat(internship1).isEqualTo(internship2);
        internship2.setId(2L);
        assertThat(internship1).isNotEqualTo(internship2);
        internship1.setId(null);
        assertThat(internship1).isNotEqualTo(internship2);
    }
}
