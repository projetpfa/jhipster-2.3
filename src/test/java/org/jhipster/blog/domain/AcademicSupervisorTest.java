package org.jhipster.blog.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.jhipster.blog.web.rest.TestUtil;

public class AcademicSupervisorTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AcademicSupervisor.class);
        AcademicSupervisor academicSupervisor1 = new AcademicSupervisor();
        academicSupervisor1.setId(1L);
        AcademicSupervisor academicSupervisor2 = new AcademicSupervisor();
        academicSupervisor2.setId(academicSupervisor1.getId());
        assertThat(academicSupervisor1).isEqualTo(academicSupervisor2);
        academicSupervisor2.setId(2L);
        assertThat(academicSupervisor1).isNotEqualTo(academicSupervisor2);
        academicSupervisor1.setId(null);
        assertThat(academicSupervisor1).isNotEqualTo(academicSupervisor2);
    }
}
