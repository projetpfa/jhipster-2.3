package org.jhipster.blog.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.jhipster.blog.web.rest.TestUtil;

public class CompanyEvaluationTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompanyEvaluation.class);
        CompanyEvaluation companyEvaluation1 = new CompanyEvaluation();
        companyEvaluation1.setId(1L);
        CompanyEvaluation companyEvaluation2 = new CompanyEvaluation();
        companyEvaluation2.setId(companyEvaluation1.getId());
        assertThat(companyEvaluation1).isEqualTo(companyEvaluation2);
        companyEvaluation2.setId(2L);
        assertThat(companyEvaluation1).isNotEqualTo(companyEvaluation2);
        companyEvaluation1.setId(null);
        assertThat(companyEvaluation1).isNotEqualTo(companyEvaluation2);
    }
}
