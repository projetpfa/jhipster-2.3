package org.jhipster.blog.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.jhipster.blog.web.rest.TestUtil;

public class StudentEvaluationTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(StudentEvaluation.class);
        StudentEvaluation studentEvaluation1 = new StudentEvaluation();
        studentEvaluation1.setId(1L);
        StudentEvaluation studentEvaluation2 = new StudentEvaluation();
        studentEvaluation2.setId(studentEvaluation1.getId());
        assertThat(studentEvaluation1).isEqualTo(studentEvaluation2);
        studentEvaluation2.setId(2L);
        assertThat(studentEvaluation1).isNotEqualTo(studentEvaluation2);
        studentEvaluation1.setId(null);
        assertThat(studentEvaluation1).isNotEqualTo(studentEvaluation2);
    }
}
