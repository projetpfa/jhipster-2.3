package org.jhipster.blog.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.jhipster.blog.web.rest.TestUtil;

public class DeadlineTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Deadline.class);
        Deadline deadline1 = new Deadline();
        deadline1.setId(1L);
        Deadline deadline2 = new Deadline();
        deadline2.setId(deadline1.getId());
        assertThat(deadline1).isEqualTo(deadline2);
        deadline2.setId(2L);
        assertThat(deadline1).isNotEqualTo(deadline2);
        deadline1.setId(null);
        assertThat(deadline1).isNotEqualTo(deadline2);
    }
}
