package org.jhipster.blog.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.jhipster.blog.web.rest.TestUtil;

public class InvitationInterviewTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(InvitationInterview.class);
        InvitationInterview invitationInterview1 = new InvitationInterview();
        invitationInterview1.setId(1L);
        InvitationInterview invitationInterview2 = new InvitationInterview();
        invitationInterview2.setId(invitationInterview1.getId());
        assertThat(invitationInterview1).isEqualTo(invitationInterview2);
        invitationInterview2.setId(2L);
        assertThat(invitationInterview1).isNotEqualTo(invitationInterview2);
        invitationInterview1.setId(null);
        assertThat(invitationInterview1).isNotEqualTo(invitationInterview2);
    }
}
