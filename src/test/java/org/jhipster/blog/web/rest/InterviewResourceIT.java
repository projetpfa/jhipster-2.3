package org.jhipster.blog.web.rest;

import org.jhipster.blog.EnisIntershipApp;
import org.jhipster.blog.domain.Interview;
import org.jhipster.blog.repository.InterviewRepository;
import org.jhipster.blog.service.InterviewService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.jhipster.blog.domain.enumeration.ATTENDANCE;
import org.jhipster.blog.domain.enumeration.STATE;
import org.jhipster.blog.domain.enumeration.Choice;
/**
 * Integration tests for the {@link InterviewResource} REST controller.
 */
@SpringBootTest(classes = EnisIntershipApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class InterviewResourceIT {

    private static final Instant DEFAULT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final ATTENDANCE DEFAULT_PRESENCE = ATTENDANCE.NONE;
    private static final ATTENDANCE UPDATED_PRESENCE = ATTENDANCE.PRESENT;

    private static final STATE DEFAULT_STATE = STATE.NONE;
    private static final STATE UPDATED_STATE = STATE.SUCCESS;

    private static final String DEFAULT_COMMUNICATION = "AAAAAAAAAA";
    private static final String UPDATED_COMMUNICATION = "BBBBBBBBBB";

    private static final Integer DEFAULT_SCORE_COMMUNICATION = 1;
    private static final Integer UPDATED_SCORE_COMMUNICATION = 2;

    private static final String DEFAULT_TECHNICAL = "AAAAAAAAAA";
    private static final String UPDATED_TECHNICAL = "BBBBBBBBBB";

    private static final Integer DEFAULT_SCORET_TECHNICAL = 1;
    private static final Integer UPDATED_SCORET_TECHNICAL = 2;

    private static final String DEFAULT_OTHER = "AAAAAAAAAA";
    private static final String UPDATED_OTHER = "BBBBBBBBBB";

    private static final Choice DEFAULT_STUD_FINAL_CHOICE = Choice.NONE;
    private static final Choice UPDATED_STUD_FINAL_CHOICE = Choice.ACCEPTED;

    @Autowired
    private InterviewRepository interviewRepository;

    @Autowired
    private InterviewService interviewService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restInterviewMockMvc;

    private Interview interview;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Interview createEntity(EntityManager em) {
        Interview interview = new Interview()
            .date(DEFAULT_DATE)
            .presence(DEFAULT_PRESENCE)
            .state(DEFAULT_STATE)
            .communication(DEFAULT_COMMUNICATION)
            .scoreCommunication(DEFAULT_SCORE_COMMUNICATION)
            .technical(DEFAULT_TECHNICAL)
            .scoretTechnical(DEFAULT_SCORET_TECHNICAL)
            .other(DEFAULT_OTHER)
            .studFinalChoice(DEFAULT_STUD_FINAL_CHOICE);
        return interview;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Interview createUpdatedEntity(EntityManager em) {
        Interview interview = new Interview()
            .date(UPDATED_DATE)
            .presence(UPDATED_PRESENCE)
            .state(UPDATED_STATE)
            .communication(UPDATED_COMMUNICATION)
            .scoreCommunication(UPDATED_SCORE_COMMUNICATION)
            .technical(UPDATED_TECHNICAL)
            .scoretTechnical(UPDATED_SCORET_TECHNICAL)
            .other(UPDATED_OTHER)
            .studFinalChoice(UPDATED_STUD_FINAL_CHOICE);
        return interview;
    }

    @BeforeEach
    public void initTest() {
        interview = createEntity(em);
    }

    @Test
    @Transactional
    public void createInterview() throws Exception {
        int databaseSizeBeforeCreate = interviewRepository.findAll().size();

        // Create the Interview
        restInterviewMockMvc.perform(post("/api/interviews")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(interview)))
            .andExpect(status().isCreated());

        // Validate the Interview in the database
        List<Interview> interviewList = interviewRepository.findAll();
        assertThat(interviewList).hasSize(databaseSizeBeforeCreate + 1);
        Interview testInterview = interviewList.get(interviewList.size() - 1);
        assertThat(testInterview.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testInterview.getPresence()).isEqualTo(DEFAULT_PRESENCE);
        assertThat(testInterview.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testInterview.getCommunication()).isEqualTo(DEFAULT_COMMUNICATION);
        assertThat(testInterview.getScoreCommunication()).isEqualTo(DEFAULT_SCORE_COMMUNICATION);
        assertThat(testInterview.getTechnical()).isEqualTo(DEFAULT_TECHNICAL);
        assertThat(testInterview.getScoretTechnical()).isEqualTo(DEFAULT_SCORET_TECHNICAL);
        assertThat(testInterview.getOther()).isEqualTo(DEFAULT_OTHER);
        assertThat(testInterview.getStudFinalChoice()).isEqualTo(DEFAULT_STUD_FINAL_CHOICE);
    }

    @Test
    @Transactional
    public void createInterviewWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = interviewRepository.findAll().size();

        // Create the Interview with an existing ID
        interview.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInterviewMockMvc.perform(post("/api/interviews")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(interview)))
            .andExpect(status().isBadRequest());

        // Validate the Interview in the database
        List<Interview> interviewList = interviewRepository.findAll();
        assertThat(interviewList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = interviewRepository.findAll().size();
        // set the field null
        interview.setDate(null);

        // Create the Interview, which fails.

        restInterviewMockMvc.perform(post("/api/interviews")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(interview)))
            .andExpect(status().isBadRequest());

        List<Interview> interviewList = interviewRepository.findAll();
        assertThat(interviewList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPresenceIsRequired() throws Exception {
        int databaseSizeBeforeTest = interviewRepository.findAll().size();
        // set the field null
        interview.setPresence(null);

        // Create the Interview, which fails.

        restInterviewMockMvc.perform(post("/api/interviews")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(interview)))
            .andExpect(status().isBadRequest());

        List<Interview> interviewList = interviewRepository.findAll();
        assertThat(interviewList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStateIsRequired() throws Exception {
        int databaseSizeBeforeTest = interviewRepository.findAll().size();
        // set the field null
        interview.setState(null);

        // Create the Interview, which fails.

        restInterviewMockMvc.perform(post("/api/interviews")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(interview)))
            .andExpect(status().isBadRequest());

        List<Interview> interviewList = interviewRepository.findAll();
        assertThat(interviewList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCommunicationIsRequired() throws Exception {
        int databaseSizeBeforeTest = interviewRepository.findAll().size();
        // set the field null
        interview.setCommunication(null);

        // Create the Interview, which fails.

        restInterviewMockMvc.perform(post("/api/interviews")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(interview)))
            .andExpect(status().isBadRequest());

        List<Interview> interviewList = interviewRepository.findAll();
        assertThat(interviewList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkScoreCommunicationIsRequired() throws Exception {
        int databaseSizeBeforeTest = interviewRepository.findAll().size();
        // set the field null
        interview.setScoreCommunication(null);

        // Create the Interview, which fails.

        restInterviewMockMvc.perform(post("/api/interviews")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(interview)))
            .andExpect(status().isBadRequest());

        List<Interview> interviewList = interviewRepository.findAll();
        assertThat(interviewList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTechnicalIsRequired() throws Exception {
        int databaseSizeBeforeTest = interviewRepository.findAll().size();
        // set the field null
        interview.setTechnical(null);

        // Create the Interview, which fails.

        restInterviewMockMvc.perform(post("/api/interviews")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(interview)))
            .andExpect(status().isBadRequest());

        List<Interview> interviewList = interviewRepository.findAll();
        assertThat(interviewList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkScoretTechnicalIsRequired() throws Exception {
        int databaseSizeBeforeTest = interviewRepository.findAll().size();
        // set the field null
        interview.setScoretTechnical(null);

        // Create the Interview, which fails.

        restInterviewMockMvc.perform(post("/api/interviews")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(interview)))
            .andExpect(status().isBadRequest());

        List<Interview> interviewList = interviewRepository.findAll();
        assertThat(interviewList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllInterviews() throws Exception {
        // Initialize the database
        interviewRepository.saveAndFlush(interview);

        // Get all the interviewList
        restInterviewMockMvc.perform(get("/api/interviews?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(interview.getId().intValue())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].presence").value(hasItem(DEFAULT_PRESENCE.toString())))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())))
            .andExpect(jsonPath("$.[*].communication").value(hasItem(DEFAULT_COMMUNICATION)))
            .andExpect(jsonPath("$.[*].scoreCommunication").value(hasItem(DEFAULT_SCORE_COMMUNICATION)))
            .andExpect(jsonPath("$.[*].technical").value(hasItem(DEFAULT_TECHNICAL)))
            .andExpect(jsonPath("$.[*].scoretTechnical").value(hasItem(DEFAULT_SCORET_TECHNICAL)))
            .andExpect(jsonPath("$.[*].other").value(hasItem(DEFAULT_OTHER)))
            .andExpect(jsonPath("$.[*].studFinalChoice").value(hasItem(DEFAULT_STUD_FINAL_CHOICE.toString())));
    }
    
    @Test
    @Transactional
    public void getInterview() throws Exception {
        // Initialize the database
        interviewRepository.saveAndFlush(interview);

        // Get the interview
        restInterviewMockMvc.perform(get("/api/interviews/{id}", interview.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(interview.getId().intValue()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.presence").value(DEFAULT_PRESENCE.toString()))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE.toString()))
            .andExpect(jsonPath("$.communication").value(DEFAULT_COMMUNICATION))
            .andExpect(jsonPath("$.scoreCommunication").value(DEFAULT_SCORE_COMMUNICATION))
            .andExpect(jsonPath("$.technical").value(DEFAULT_TECHNICAL))
            .andExpect(jsonPath("$.scoretTechnical").value(DEFAULT_SCORET_TECHNICAL))
            .andExpect(jsonPath("$.other").value(DEFAULT_OTHER))
            .andExpect(jsonPath("$.studFinalChoice").value(DEFAULT_STUD_FINAL_CHOICE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingInterview() throws Exception {
        // Get the interview
        restInterviewMockMvc.perform(get("/api/interviews/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInterview() throws Exception {
        // Initialize the database
        interviewService.save(interview);

        int databaseSizeBeforeUpdate = interviewRepository.findAll().size();

        // Update the interview
        Interview updatedInterview = interviewRepository.findById(interview.getId()).get();
        // Disconnect from session so that the updates on updatedInterview are not directly saved in db
        em.detach(updatedInterview);
        updatedInterview
            .date(UPDATED_DATE)
            .presence(UPDATED_PRESENCE)
            .state(UPDATED_STATE)
            .communication(UPDATED_COMMUNICATION)
            .scoreCommunication(UPDATED_SCORE_COMMUNICATION)
            .technical(UPDATED_TECHNICAL)
            .scoretTechnical(UPDATED_SCORET_TECHNICAL)
            .other(UPDATED_OTHER)
            .studFinalChoice(UPDATED_STUD_FINAL_CHOICE);

        restInterviewMockMvc.perform(put("/api/interviews")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedInterview)))
            .andExpect(status().isOk());

        // Validate the Interview in the database
        List<Interview> interviewList = interviewRepository.findAll();
        assertThat(interviewList).hasSize(databaseSizeBeforeUpdate);
        Interview testInterview = interviewList.get(interviewList.size() - 1);
        assertThat(testInterview.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testInterview.getPresence()).isEqualTo(UPDATED_PRESENCE);
        assertThat(testInterview.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testInterview.getCommunication()).isEqualTo(UPDATED_COMMUNICATION);
        assertThat(testInterview.getScoreCommunication()).isEqualTo(UPDATED_SCORE_COMMUNICATION);
        assertThat(testInterview.getTechnical()).isEqualTo(UPDATED_TECHNICAL);
        assertThat(testInterview.getScoretTechnical()).isEqualTo(UPDATED_SCORET_TECHNICAL);
        assertThat(testInterview.getOther()).isEqualTo(UPDATED_OTHER);
        assertThat(testInterview.getStudFinalChoice()).isEqualTo(UPDATED_STUD_FINAL_CHOICE);
    }

    @Test
    @Transactional
    public void updateNonExistingInterview() throws Exception {
        int databaseSizeBeforeUpdate = interviewRepository.findAll().size();

        // Create the Interview

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInterviewMockMvc.perform(put("/api/interviews")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(interview)))
            .andExpect(status().isBadRequest());

        // Validate the Interview in the database
        List<Interview> interviewList = interviewRepository.findAll();
        assertThat(interviewList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteInterview() throws Exception {
        // Initialize the database
        interviewService.save(interview);

        int databaseSizeBeforeDelete = interviewRepository.findAll().size();

        // Delete the interview
        restInterviewMockMvc.perform(delete("/api/interviews/{id}", interview.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Interview> interviewList = interviewRepository.findAll();
        assertThat(interviewList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
