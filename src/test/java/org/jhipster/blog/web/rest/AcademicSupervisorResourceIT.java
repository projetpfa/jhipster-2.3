package org.jhipster.blog.web.rest;

import org.jhipster.blog.EnisIntershipApp;
import org.jhipster.blog.domain.AcademicSupervisor;
import org.jhipster.blog.repository.AcademicSupervisorRepository;
import org.jhipster.blog.service.AcademicSupervisorService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AcademicSupervisorResource} REST controller.
 */
@SpringBootTest(classes = EnisIntershipApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class AcademicSupervisorResourceIT {

    private static final byte[] DEFAULT_PHOTO = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_PHOTO = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_PHOTO_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_PHOTO_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final Integer DEFAULT_POSTAL_CODE = 1;
    private static final Integer UPDATED_POSTAL_CODE = 2;

    private static final Integer DEFAULT_PHONE_NUMBER = 1;
    private static final Integer UPDATED_PHONE_NUMBER = 2;

    private static final String DEFAULT_GRADE = "AAAAAAAAAA";
    private static final String UPDATED_GRADE = "BBBBBBBBBB";

    @Autowired
    private AcademicSupervisorRepository academicSupervisorRepository;

    @Autowired
    private AcademicSupervisorService academicSupervisorService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAcademicSupervisorMockMvc;

    private AcademicSupervisor academicSupervisor;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AcademicSupervisor createEntity(EntityManager em) {
        AcademicSupervisor academicSupervisor = new AcademicSupervisor()
            .photo(DEFAULT_PHOTO)
            .photoContentType(DEFAULT_PHOTO_CONTENT_TYPE)
            .city(DEFAULT_CITY)
            .country(DEFAULT_COUNTRY)
            .postalCode(DEFAULT_POSTAL_CODE)
            .phoneNumber(DEFAULT_PHONE_NUMBER)
            .grade(DEFAULT_GRADE);
        return academicSupervisor;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AcademicSupervisor createUpdatedEntity(EntityManager em) {
        AcademicSupervisor academicSupervisor = new AcademicSupervisor()
            .photo(UPDATED_PHOTO)
            .photoContentType(UPDATED_PHOTO_CONTENT_TYPE)
            .city(UPDATED_CITY)
            .country(UPDATED_COUNTRY)
            .postalCode(UPDATED_POSTAL_CODE)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .grade(UPDATED_GRADE);
        return academicSupervisor;
    }

    @BeforeEach
    public void initTest() {
        academicSupervisor = createEntity(em);
    }

    @Test
    @Transactional
    public void createAcademicSupervisor() throws Exception {
        int databaseSizeBeforeCreate = academicSupervisorRepository.findAll().size();

        // Create the AcademicSupervisor
        restAcademicSupervisorMockMvc.perform(post("/api/academic-supervisors")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(academicSupervisor)))
            .andExpect(status().isCreated());

        // Validate the AcademicSupervisor in the database
        List<AcademicSupervisor> academicSupervisorList = academicSupervisorRepository.findAll();
        assertThat(academicSupervisorList).hasSize(databaseSizeBeforeCreate + 1);
        AcademicSupervisor testAcademicSupervisor = academicSupervisorList.get(academicSupervisorList.size() - 1);
        assertThat(testAcademicSupervisor.getPhoto()).isEqualTo(DEFAULT_PHOTO);
        assertThat(testAcademicSupervisor.getPhotoContentType()).isEqualTo(DEFAULT_PHOTO_CONTENT_TYPE);
        assertThat(testAcademicSupervisor.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testAcademicSupervisor.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testAcademicSupervisor.getPostalCode()).isEqualTo(DEFAULT_POSTAL_CODE);
        assertThat(testAcademicSupervisor.getPhoneNumber()).isEqualTo(DEFAULT_PHONE_NUMBER);
        assertThat(testAcademicSupervisor.getGrade()).isEqualTo(DEFAULT_GRADE);
    }

    @Test
    @Transactional
    public void createAcademicSupervisorWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = academicSupervisorRepository.findAll().size();

        // Create the AcademicSupervisor with an existing ID
        academicSupervisor.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAcademicSupervisorMockMvc.perform(post("/api/academic-supervisors")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(academicSupervisor)))
            .andExpect(status().isBadRequest());

        // Validate the AcademicSupervisor in the database
        List<AcademicSupervisor> academicSupervisorList = academicSupervisorRepository.findAll();
        assertThat(academicSupervisorList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCityIsRequired() throws Exception {
        int databaseSizeBeforeTest = academicSupervisorRepository.findAll().size();
        // set the field null
        academicSupervisor.setCity(null);

        // Create the AcademicSupervisor, which fails.

        restAcademicSupervisorMockMvc.perform(post("/api/academic-supervisors")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(academicSupervisor)))
            .andExpect(status().isBadRequest());

        List<AcademicSupervisor> academicSupervisorList = academicSupervisorRepository.findAll();
        assertThat(academicSupervisorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCountryIsRequired() throws Exception {
        int databaseSizeBeforeTest = academicSupervisorRepository.findAll().size();
        // set the field null
        academicSupervisor.setCountry(null);

        // Create the AcademicSupervisor, which fails.

        restAcademicSupervisorMockMvc.perform(post("/api/academic-supervisors")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(academicSupervisor)))
            .andExpect(status().isBadRequest());

        List<AcademicSupervisor> academicSupervisorList = academicSupervisorRepository.findAll();
        assertThat(academicSupervisorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPostalCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = academicSupervisorRepository.findAll().size();
        // set the field null
        academicSupervisor.setPostalCode(null);

        // Create the AcademicSupervisor, which fails.

        restAcademicSupervisorMockMvc.perform(post("/api/academic-supervisors")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(academicSupervisor)))
            .andExpect(status().isBadRequest());

        List<AcademicSupervisor> academicSupervisorList = academicSupervisorRepository.findAll();
        assertThat(academicSupervisorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPhoneNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = academicSupervisorRepository.findAll().size();
        // set the field null
        academicSupervisor.setPhoneNumber(null);

        // Create the AcademicSupervisor, which fails.

        restAcademicSupervisorMockMvc.perform(post("/api/academic-supervisors")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(academicSupervisor)))
            .andExpect(status().isBadRequest());

        List<AcademicSupervisor> academicSupervisorList = academicSupervisorRepository.findAll();
        assertThat(academicSupervisorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAcademicSupervisors() throws Exception {
        // Initialize the database
        academicSupervisorRepository.saveAndFlush(academicSupervisor);

        // Get all the academicSupervisorList
        restAcademicSupervisorMockMvc.perform(get("/api/academic-supervisors?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(academicSupervisor.getId().intValue())))
            .andExpect(jsonPath("$.[*].photoContentType").value(hasItem(DEFAULT_PHOTO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].photo").value(hasItem(Base64Utils.encodeToString(DEFAULT_PHOTO))))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY)))
            .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE)))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER)))
            .andExpect(jsonPath("$.[*].grade").value(hasItem(DEFAULT_GRADE)));
    }
    
    @Test
    @Transactional
    public void getAcademicSupervisor() throws Exception {
        // Initialize the database
        academicSupervisorRepository.saveAndFlush(academicSupervisor);

        // Get the academicSupervisor
        restAcademicSupervisorMockMvc.perform(get("/api/academic-supervisors/{id}", academicSupervisor.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(academicSupervisor.getId().intValue()))
            .andExpect(jsonPath("$.photoContentType").value(DEFAULT_PHOTO_CONTENT_TYPE))
            .andExpect(jsonPath("$.photo").value(Base64Utils.encodeToString(DEFAULT_PHOTO)))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY))
            .andExpect(jsonPath("$.postalCode").value(DEFAULT_POSTAL_CODE))
            .andExpect(jsonPath("$.phoneNumber").value(DEFAULT_PHONE_NUMBER))
            .andExpect(jsonPath("$.grade").value(DEFAULT_GRADE));
    }

    @Test
    @Transactional
    public void getNonExistingAcademicSupervisor() throws Exception {
        // Get the academicSupervisor
        restAcademicSupervisorMockMvc.perform(get("/api/academic-supervisors/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAcademicSupervisor() throws Exception {
        // Initialize the database
        academicSupervisorService.save(academicSupervisor);

        int databaseSizeBeforeUpdate = academicSupervisorRepository.findAll().size();

        // Update the academicSupervisor
        AcademicSupervisor updatedAcademicSupervisor = academicSupervisorRepository.findById(academicSupervisor.getId()).get();
        // Disconnect from session so that the updates on updatedAcademicSupervisor are not directly saved in db
        em.detach(updatedAcademicSupervisor);
        updatedAcademicSupervisor
            .photo(UPDATED_PHOTO)
            .photoContentType(UPDATED_PHOTO_CONTENT_TYPE)
            .city(UPDATED_CITY)
            .country(UPDATED_COUNTRY)
            .postalCode(UPDATED_POSTAL_CODE)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .grade(UPDATED_GRADE);

        restAcademicSupervisorMockMvc.perform(put("/api/academic-supervisors")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAcademicSupervisor)))
            .andExpect(status().isOk());

        // Validate the AcademicSupervisor in the database
        List<AcademicSupervisor> academicSupervisorList = academicSupervisorRepository.findAll();
        assertThat(academicSupervisorList).hasSize(databaseSizeBeforeUpdate);
        AcademicSupervisor testAcademicSupervisor = academicSupervisorList.get(academicSupervisorList.size() - 1);
        assertThat(testAcademicSupervisor.getPhoto()).isEqualTo(UPDATED_PHOTO);
        assertThat(testAcademicSupervisor.getPhotoContentType()).isEqualTo(UPDATED_PHOTO_CONTENT_TYPE);
        assertThat(testAcademicSupervisor.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testAcademicSupervisor.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testAcademicSupervisor.getPostalCode()).isEqualTo(UPDATED_POSTAL_CODE);
        assertThat(testAcademicSupervisor.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
        assertThat(testAcademicSupervisor.getGrade()).isEqualTo(UPDATED_GRADE);
    }

    @Test
    @Transactional
    public void updateNonExistingAcademicSupervisor() throws Exception {
        int databaseSizeBeforeUpdate = academicSupervisorRepository.findAll().size();

        // Create the AcademicSupervisor

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAcademicSupervisorMockMvc.perform(put("/api/academic-supervisors")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(academicSupervisor)))
            .andExpect(status().isBadRequest());

        // Validate the AcademicSupervisor in the database
        List<AcademicSupervisor> academicSupervisorList = academicSupervisorRepository.findAll();
        assertThat(academicSupervisorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAcademicSupervisor() throws Exception {
        // Initialize the database
        academicSupervisorService.save(academicSupervisor);

        int databaseSizeBeforeDelete = academicSupervisorRepository.findAll().size();

        // Delete the academicSupervisor
        restAcademicSupervisorMockMvc.perform(delete("/api/academic-supervisors/{id}", academicSupervisor.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AcademicSupervisor> academicSupervisorList = academicSupervisorRepository.findAll();
        assertThat(academicSupervisorList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
