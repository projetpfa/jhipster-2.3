package org.jhipster.blog.web.rest;

import org.jhipster.blog.EnisIntershipApp;
import org.jhipster.blog.domain.Deadline;
import org.jhipster.blog.repository.DeadlineRepository;
import org.jhipster.blog.service.DeadlineService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.jhipster.blog.domain.enumeration.DEADLINES;
/**
 * Integration tests for the {@link DeadlineResource} REST controller.
 */
@SpringBootTest(classes = EnisIntershipApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class DeadlineResourceIT {

    private static final Instant DEFAULT_START = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_START = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_END = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_END = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final DEADLINES DEFAULT_TYPE = DEADLINES.DEADLINEINTERVIEW;
    private static final DEADLINES UPDATED_TYPE = DEADLINES.DEADLINEAFFECTATION;

    @Autowired
    private DeadlineRepository deadlineRepository;

    @Autowired
    private DeadlineService deadlineService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDeadlineMockMvc;

    private Deadline deadline;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Deadline createEntity(EntityManager em) {
        Deadline deadline = new Deadline()
            .start(DEFAULT_START)
            .end(DEFAULT_END)
            .type(DEFAULT_TYPE);
        return deadline;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Deadline createUpdatedEntity(EntityManager em) {
        Deadline deadline = new Deadline()
            .start(UPDATED_START)
            .end(UPDATED_END)
            .type(UPDATED_TYPE);
        return deadline;
    }

    @BeforeEach
    public void initTest() {
        deadline = createEntity(em);
    }

    @Test
    @Transactional
    public void createDeadline() throws Exception {
        int databaseSizeBeforeCreate = deadlineRepository.findAll().size();

        // Create the Deadline
        restDeadlineMockMvc.perform(post("/api/deadlines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(deadline)))
            .andExpect(status().isCreated());

        // Validate the Deadline in the database
        List<Deadline> deadlineList = deadlineRepository.findAll();
        assertThat(deadlineList).hasSize(databaseSizeBeforeCreate + 1);
        Deadline testDeadline = deadlineList.get(deadlineList.size() - 1);
        assertThat(testDeadline.getStart()).isEqualTo(DEFAULT_START);
        assertThat(testDeadline.getEnd()).isEqualTo(DEFAULT_END);
        assertThat(testDeadline.getType()).isEqualTo(DEFAULT_TYPE);
    }

    @Test
    @Transactional
    public void createDeadlineWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = deadlineRepository.findAll().size();

        // Create the Deadline with an existing ID
        deadline.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDeadlineMockMvc.perform(post("/api/deadlines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(deadline)))
            .andExpect(status().isBadRequest());

        // Validate the Deadline in the database
        List<Deadline> deadlineList = deadlineRepository.findAll();
        assertThat(deadlineList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkStartIsRequired() throws Exception {
        int databaseSizeBeforeTest = deadlineRepository.findAll().size();
        // set the field null
        deadline.setStart(null);

        // Create the Deadline, which fails.

        restDeadlineMockMvc.perform(post("/api/deadlines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(deadline)))
            .andExpect(status().isBadRequest());

        List<Deadline> deadlineList = deadlineRepository.findAll();
        assertThat(deadlineList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEndIsRequired() throws Exception {
        int databaseSizeBeforeTest = deadlineRepository.findAll().size();
        // set the field null
        deadline.setEnd(null);

        // Create the Deadline, which fails.

        restDeadlineMockMvc.perform(post("/api/deadlines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(deadline)))
            .andExpect(status().isBadRequest());

        List<Deadline> deadlineList = deadlineRepository.findAll();
        assertThat(deadlineList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = deadlineRepository.findAll().size();
        // set the field null
        deadline.setType(null);

        // Create the Deadline, which fails.

        restDeadlineMockMvc.perform(post("/api/deadlines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(deadline)))
            .andExpect(status().isBadRequest());

        List<Deadline> deadlineList = deadlineRepository.findAll();
        assertThat(deadlineList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDeadlines() throws Exception {
        // Initialize the database
        deadlineRepository.saveAndFlush(deadline);

        // Get all the deadlineList
        restDeadlineMockMvc.perform(get("/api/deadlines?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(deadline.getId().intValue())))
            .andExpect(jsonPath("$.[*].start").value(hasItem(DEFAULT_START.toString())))
            .andExpect(jsonPath("$.[*].end").value(hasItem(DEFAULT_END.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }
    
    @Test
    @Transactional
    public void getDeadline() throws Exception {
        // Initialize the database
        deadlineRepository.saveAndFlush(deadline);

        // Get the deadline
        restDeadlineMockMvc.perform(get("/api/deadlines/{id}", deadline.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(deadline.getId().intValue()))
            .andExpect(jsonPath("$.start").value(DEFAULT_START.toString()))
            .andExpect(jsonPath("$.end").value(DEFAULT_END.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDeadline() throws Exception {
        // Get the deadline
        restDeadlineMockMvc.perform(get("/api/deadlines/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDeadline() throws Exception {
        // Initialize the database
        deadlineService.save(deadline);

        int databaseSizeBeforeUpdate = deadlineRepository.findAll().size();

        // Update the deadline
        Deadline updatedDeadline = deadlineRepository.findById(deadline.getId()).get();
        // Disconnect from session so that the updates on updatedDeadline are not directly saved in db
        em.detach(updatedDeadline);
        updatedDeadline
            .start(UPDATED_START)
            .end(UPDATED_END)
            .type(UPDATED_TYPE);

        restDeadlineMockMvc.perform(put("/api/deadlines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedDeadline)))
            .andExpect(status().isOk());

        // Validate the Deadline in the database
        List<Deadline> deadlineList = deadlineRepository.findAll();
        assertThat(deadlineList).hasSize(databaseSizeBeforeUpdate);
        Deadline testDeadline = deadlineList.get(deadlineList.size() - 1);
        assertThat(testDeadline.getStart()).isEqualTo(UPDATED_START);
        assertThat(testDeadline.getEnd()).isEqualTo(UPDATED_END);
        assertThat(testDeadline.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingDeadline() throws Exception {
        int databaseSizeBeforeUpdate = deadlineRepository.findAll().size();

        // Create the Deadline

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDeadlineMockMvc.perform(put("/api/deadlines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(deadline)))
            .andExpect(status().isBadRequest());

        // Validate the Deadline in the database
        List<Deadline> deadlineList = deadlineRepository.findAll();
        assertThat(deadlineList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDeadline() throws Exception {
        // Initialize the database
        deadlineService.save(deadline);

        int databaseSizeBeforeDelete = deadlineRepository.findAll().size();

        // Delete the deadline
        restDeadlineMockMvc.perform(delete("/api/deadlines/{id}", deadline.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Deadline> deadlineList = deadlineRepository.findAll();
        assertThat(deadlineList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
