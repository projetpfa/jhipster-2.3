package org.jhipster.blog.web.rest;

import org.jhipster.blog.EnisIntershipApp;
import org.jhipster.blog.domain.CompanyEvaluation;
import org.jhipster.blog.repository.CompanyEvaluationRepository;
import org.jhipster.blog.service.CompanyEvaluationService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CompanyEvaluationResource} REST controller.
 */
@SpringBootTest(classes = EnisIntershipApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class CompanyEvaluationResourceIT {

    private static final Integer DEFAULT_SUPERVISION = 1;
    private static final Integer UPDATED_SUPERVISION = 2;

    private static final Integer DEFAULT_ACCEUIL = 1;
    private static final Integer UPDATED_ACCEUIL = 2;

    @Autowired
    private CompanyEvaluationRepository companyEvaluationRepository;

    @Autowired
    private CompanyEvaluationService companyEvaluationService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCompanyEvaluationMockMvc;

    private CompanyEvaluation companyEvaluation;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompanyEvaluation createEntity(EntityManager em) {
        CompanyEvaluation companyEvaluation = new CompanyEvaluation()
            .supervision(DEFAULT_SUPERVISION)
            .acceuil(DEFAULT_ACCEUIL);
        return companyEvaluation;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompanyEvaluation createUpdatedEntity(EntityManager em) {
        CompanyEvaluation companyEvaluation = new CompanyEvaluation()
            .supervision(UPDATED_SUPERVISION)
            .acceuil(UPDATED_ACCEUIL);
        return companyEvaluation;
    }

    @BeforeEach
    public void initTest() {
        companyEvaluation = createEntity(em);
    }

    @Test
    @Transactional
    public void createCompanyEvaluation() throws Exception {
        int databaseSizeBeforeCreate = companyEvaluationRepository.findAll().size();

        // Create the CompanyEvaluation
        restCompanyEvaluationMockMvc.perform(post("/api/company-evaluations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyEvaluation)))
            .andExpect(status().isCreated());

        // Validate the CompanyEvaluation in the database
        List<CompanyEvaluation> companyEvaluationList = companyEvaluationRepository.findAll();
        assertThat(companyEvaluationList).hasSize(databaseSizeBeforeCreate + 1);
        CompanyEvaluation testCompanyEvaluation = companyEvaluationList.get(companyEvaluationList.size() - 1);
        assertThat(testCompanyEvaluation.getSupervision()).isEqualTo(DEFAULT_SUPERVISION);
        assertThat(testCompanyEvaluation.getAcceuil()).isEqualTo(DEFAULT_ACCEUIL);
    }

    @Test
    @Transactional
    public void createCompanyEvaluationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = companyEvaluationRepository.findAll().size();

        // Create the CompanyEvaluation with an existing ID
        companyEvaluation.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCompanyEvaluationMockMvc.perform(post("/api/company-evaluations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyEvaluation)))
            .andExpect(status().isBadRequest());

        // Validate the CompanyEvaluation in the database
        List<CompanyEvaluation> companyEvaluationList = companyEvaluationRepository.findAll();
        assertThat(companyEvaluationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkSupervisionIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyEvaluationRepository.findAll().size();
        // set the field null
        companyEvaluation.setSupervision(null);

        // Create the CompanyEvaluation, which fails.

        restCompanyEvaluationMockMvc.perform(post("/api/company-evaluations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyEvaluation)))
            .andExpect(status().isBadRequest());

        List<CompanyEvaluation> companyEvaluationList = companyEvaluationRepository.findAll();
        assertThat(companyEvaluationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAcceuilIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyEvaluationRepository.findAll().size();
        // set the field null
        companyEvaluation.setAcceuil(null);

        // Create the CompanyEvaluation, which fails.

        restCompanyEvaluationMockMvc.perform(post("/api/company-evaluations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyEvaluation)))
            .andExpect(status().isBadRequest());

        List<CompanyEvaluation> companyEvaluationList = companyEvaluationRepository.findAll();
        assertThat(companyEvaluationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCompanyEvaluations() throws Exception {
        // Initialize the database
        companyEvaluationRepository.saveAndFlush(companyEvaluation);

        // Get all the companyEvaluationList
        restCompanyEvaluationMockMvc.perform(get("/api/company-evaluations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(companyEvaluation.getId().intValue())))
            .andExpect(jsonPath("$.[*].supervision").value(hasItem(DEFAULT_SUPERVISION)))
            .andExpect(jsonPath("$.[*].acceuil").value(hasItem(DEFAULT_ACCEUIL)));
    }
    
    @Test
    @Transactional
    public void getCompanyEvaluation() throws Exception {
        // Initialize the database
        companyEvaluationRepository.saveAndFlush(companyEvaluation);

        // Get the companyEvaluation
        restCompanyEvaluationMockMvc.perform(get("/api/company-evaluations/{id}", companyEvaluation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(companyEvaluation.getId().intValue()))
            .andExpect(jsonPath("$.supervision").value(DEFAULT_SUPERVISION))
            .andExpect(jsonPath("$.acceuil").value(DEFAULT_ACCEUIL));
    }

    @Test
    @Transactional
    public void getNonExistingCompanyEvaluation() throws Exception {
        // Get the companyEvaluation
        restCompanyEvaluationMockMvc.perform(get("/api/company-evaluations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCompanyEvaluation() throws Exception {
        // Initialize the database
        companyEvaluationService.save(companyEvaluation);

        int databaseSizeBeforeUpdate = companyEvaluationRepository.findAll().size();

        // Update the companyEvaluation
        CompanyEvaluation updatedCompanyEvaluation = companyEvaluationRepository.findById(companyEvaluation.getId()).get();
        // Disconnect from session so that the updates on updatedCompanyEvaluation are not directly saved in db
        em.detach(updatedCompanyEvaluation);
        updatedCompanyEvaluation
            .supervision(UPDATED_SUPERVISION)
            .acceuil(UPDATED_ACCEUIL);

        restCompanyEvaluationMockMvc.perform(put("/api/company-evaluations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCompanyEvaluation)))
            .andExpect(status().isOk());

        // Validate the CompanyEvaluation in the database
        List<CompanyEvaluation> companyEvaluationList = companyEvaluationRepository.findAll();
        assertThat(companyEvaluationList).hasSize(databaseSizeBeforeUpdate);
        CompanyEvaluation testCompanyEvaluation = companyEvaluationList.get(companyEvaluationList.size() - 1);
        assertThat(testCompanyEvaluation.getSupervision()).isEqualTo(UPDATED_SUPERVISION);
        assertThat(testCompanyEvaluation.getAcceuil()).isEqualTo(UPDATED_ACCEUIL);
    }

    @Test
    @Transactional
    public void updateNonExistingCompanyEvaluation() throws Exception {
        int databaseSizeBeforeUpdate = companyEvaluationRepository.findAll().size();

        // Create the CompanyEvaluation

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCompanyEvaluationMockMvc.perform(put("/api/company-evaluations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyEvaluation)))
            .andExpect(status().isBadRequest());

        // Validate the CompanyEvaluation in the database
        List<CompanyEvaluation> companyEvaluationList = companyEvaluationRepository.findAll();
        assertThat(companyEvaluationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCompanyEvaluation() throws Exception {
        // Initialize the database
        companyEvaluationService.save(companyEvaluation);

        int databaseSizeBeforeDelete = companyEvaluationRepository.findAll().size();

        // Delete the companyEvaluation
        restCompanyEvaluationMockMvc.perform(delete("/api/company-evaluations/{id}", companyEvaluation.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CompanyEvaluation> companyEvaluationList = companyEvaluationRepository.findAll();
        assertThat(companyEvaluationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
