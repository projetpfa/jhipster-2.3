package org.jhipster.blog.web.rest;

import org.jhipster.blog.EnisIntershipApp;
import org.jhipster.blog.domain.StudentEvaluation;
import org.jhipster.blog.repository.StudentEvaluationRepository;
import org.jhipster.blog.service.StudentEvaluationService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link StudentEvaluationResource} REST controller.
 */
@SpringBootTest(classes = EnisIntershipApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class StudentEvaluationResourceIT {

    private static final Integer DEFAULT_COMMUNICATION = 1;
    private static final Integer UPDATED_COMMUNICATION = 2;

    private static final Integer DEFAULT_TECHNICAL = 1;
    private static final Integer UPDATED_TECHNICAL = 2;

    @Autowired
    private StudentEvaluationRepository studentEvaluationRepository;

    @Autowired
    private StudentEvaluationService studentEvaluationService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restStudentEvaluationMockMvc;

    private StudentEvaluation studentEvaluation;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static StudentEvaluation createEntity(EntityManager em) {
        StudentEvaluation studentEvaluation = new StudentEvaluation()
            .communication(DEFAULT_COMMUNICATION)
            .technical(DEFAULT_TECHNICAL);
        return studentEvaluation;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static StudentEvaluation createUpdatedEntity(EntityManager em) {
        StudentEvaluation studentEvaluation = new StudentEvaluation()
            .communication(UPDATED_COMMUNICATION)
            .technical(UPDATED_TECHNICAL);
        return studentEvaluation;
    }

    @BeforeEach
    public void initTest() {
        studentEvaluation = createEntity(em);
    }

    @Test
    @Transactional
    public void createStudentEvaluation() throws Exception {
        int databaseSizeBeforeCreate = studentEvaluationRepository.findAll().size();

        // Create the StudentEvaluation
        restStudentEvaluationMockMvc.perform(post("/api/student-evaluations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(studentEvaluation)))
            .andExpect(status().isCreated());

        // Validate the StudentEvaluation in the database
        List<StudentEvaluation> studentEvaluationList = studentEvaluationRepository.findAll();
        assertThat(studentEvaluationList).hasSize(databaseSizeBeforeCreate + 1);
        StudentEvaluation testStudentEvaluation = studentEvaluationList.get(studentEvaluationList.size() - 1);
        assertThat(testStudentEvaluation.getCommunication()).isEqualTo(DEFAULT_COMMUNICATION);
        assertThat(testStudentEvaluation.getTechnical()).isEqualTo(DEFAULT_TECHNICAL);
    }

    @Test
    @Transactional
    public void createStudentEvaluationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = studentEvaluationRepository.findAll().size();

        // Create the StudentEvaluation with an existing ID
        studentEvaluation.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restStudentEvaluationMockMvc.perform(post("/api/student-evaluations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(studentEvaluation)))
            .andExpect(status().isBadRequest());

        // Validate the StudentEvaluation in the database
        List<StudentEvaluation> studentEvaluationList = studentEvaluationRepository.findAll();
        assertThat(studentEvaluationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCommunicationIsRequired() throws Exception {
        int databaseSizeBeforeTest = studentEvaluationRepository.findAll().size();
        // set the field null
        studentEvaluation.setCommunication(null);

        // Create the StudentEvaluation, which fails.

        restStudentEvaluationMockMvc.perform(post("/api/student-evaluations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(studentEvaluation)))
            .andExpect(status().isBadRequest());

        List<StudentEvaluation> studentEvaluationList = studentEvaluationRepository.findAll();
        assertThat(studentEvaluationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTechnicalIsRequired() throws Exception {
        int databaseSizeBeforeTest = studentEvaluationRepository.findAll().size();
        // set the field null
        studentEvaluation.setTechnical(null);

        // Create the StudentEvaluation, which fails.

        restStudentEvaluationMockMvc.perform(post("/api/student-evaluations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(studentEvaluation)))
            .andExpect(status().isBadRequest());

        List<StudentEvaluation> studentEvaluationList = studentEvaluationRepository.findAll();
        assertThat(studentEvaluationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllStudentEvaluations() throws Exception {
        // Initialize the database
        studentEvaluationRepository.saveAndFlush(studentEvaluation);

        // Get all the studentEvaluationList
        restStudentEvaluationMockMvc.perform(get("/api/student-evaluations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(studentEvaluation.getId().intValue())))
            .andExpect(jsonPath("$.[*].communication").value(hasItem(DEFAULT_COMMUNICATION)))
            .andExpect(jsonPath("$.[*].technical").value(hasItem(DEFAULT_TECHNICAL)));
    }
    
    @Test
    @Transactional
    public void getStudentEvaluation() throws Exception {
        // Initialize the database
        studentEvaluationRepository.saveAndFlush(studentEvaluation);

        // Get the studentEvaluation
        restStudentEvaluationMockMvc.perform(get("/api/student-evaluations/{id}", studentEvaluation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(studentEvaluation.getId().intValue()))
            .andExpect(jsonPath("$.communication").value(DEFAULT_COMMUNICATION))
            .andExpect(jsonPath("$.technical").value(DEFAULT_TECHNICAL));
    }

    @Test
    @Transactional
    public void getNonExistingStudentEvaluation() throws Exception {
        // Get the studentEvaluation
        restStudentEvaluationMockMvc.perform(get("/api/student-evaluations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateStudentEvaluation() throws Exception {
        // Initialize the database
        studentEvaluationService.save(studentEvaluation);

        int databaseSizeBeforeUpdate = studentEvaluationRepository.findAll().size();

        // Update the studentEvaluation
        StudentEvaluation updatedStudentEvaluation = studentEvaluationRepository.findById(studentEvaluation.getId()).get();
        // Disconnect from session so that the updates on updatedStudentEvaluation are not directly saved in db
        em.detach(updatedStudentEvaluation);
        updatedStudentEvaluation
            .communication(UPDATED_COMMUNICATION)
            .technical(UPDATED_TECHNICAL);

        restStudentEvaluationMockMvc.perform(put("/api/student-evaluations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedStudentEvaluation)))
            .andExpect(status().isOk());

        // Validate the StudentEvaluation in the database
        List<StudentEvaluation> studentEvaluationList = studentEvaluationRepository.findAll();
        assertThat(studentEvaluationList).hasSize(databaseSizeBeforeUpdate);
        StudentEvaluation testStudentEvaluation = studentEvaluationList.get(studentEvaluationList.size() - 1);
        assertThat(testStudentEvaluation.getCommunication()).isEqualTo(UPDATED_COMMUNICATION);
        assertThat(testStudentEvaluation.getTechnical()).isEqualTo(UPDATED_TECHNICAL);
    }

    @Test
    @Transactional
    public void updateNonExistingStudentEvaluation() throws Exception {
        int databaseSizeBeforeUpdate = studentEvaluationRepository.findAll().size();

        // Create the StudentEvaluation

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restStudentEvaluationMockMvc.perform(put("/api/student-evaluations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(studentEvaluation)))
            .andExpect(status().isBadRequest());

        // Validate the StudentEvaluation in the database
        List<StudentEvaluation> studentEvaluationList = studentEvaluationRepository.findAll();
        assertThat(studentEvaluationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteStudentEvaluation() throws Exception {
        // Initialize the database
        studentEvaluationService.save(studentEvaluation);

        int databaseSizeBeforeDelete = studentEvaluationRepository.findAll().size();

        // Delete the studentEvaluation
        restStudentEvaluationMockMvc.perform(delete("/api/student-evaluations/{id}", studentEvaluation.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<StudentEvaluation> studentEvaluationList = studentEvaluationRepository.findAll();
        assertThat(studentEvaluationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
