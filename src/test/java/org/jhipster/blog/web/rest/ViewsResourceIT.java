package org.jhipster.blog.web.rest;

import org.jhipster.blog.EnisIntershipApp;
import org.jhipster.blog.domain.Views;
import org.jhipster.blog.repository.ViewsRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ViewsResource} REST controller.
 */
@SpringBootTest(classes = EnisIntershipApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class ViewsResourceIT {

    @Autowired
    private ViewsRepository viewsRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restViewsMockMvc;

    private Views views;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Views createEntity(EntityManager em) {
        Views views = new Views();
        return views;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Views createUpdatedEntity(EntityManager em) {
        Views views = new Views();
        return views;
    }

    @BeforeEach
    public void initTest() {
        views = createEntity(em);
    }

    @Test
    @Transactional
    public void createViews() throws Exception {
        int databaseSizeBeforeCreate = viewsRepository.findAll().size();

        // Create the Views
        restViewsMockMvc.perform(post("/api/views")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(views)))
            .andExpect(status().isCreated());

        // Validate the Views in the database
        List<Views> viewsList = viewsRepository.findAll();
        assertThat(viewsList).hasSize(databaseSizeBeforeCreate + 1);
        Views testViews = viewsList.get(viewsList.size() - 1);
    }

    @Test
    @Transactional
    public void createViewsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = viewsRepository.findAll().size();

        // Create the Views with an existing ID
        views.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restViewsMockMvc.perform(post("/api/views")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(views)))
            .andExpect(status().isBadRequest());

        // Validate the Views in the database
        List<Views> viewsList = viewsRepository.findAll();
        assertThat(viewsList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllViews() throws Exception {
        // Initialize the database
        viewsRepository.saveAndFlush(views);

        // Get all the viewsList
        restViewsMockMvc.perform(get("/api/views?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(views.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getViews() throws Exception {
        // Initialize the database
        viewsRepository.saveAndFlush(views);

        // Get the views
        restViewsMockMvc.perform(get("/api/views/{id}", views.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(views.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingViews() throws Exception {
        // Get the views
        restViewsMockMvc.perform(get("/api/views/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateViews() throws Exception {
        // Initialize the database
        viewsRepository.saveAndFlush(views);

        int databaseSizeBeforeUpdate = viewsRepository.findAll().size();

        // Update the views
        Views updatedViews = viewsRepository.findById(views.getId()).get();
        // Disconnect from session so that the updates on updatedViews are not directly saved in db
        em.detach(updatedViews);

        restViewsMockMvc.perform(put("/api/views")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedViews)))
            .andExpect(status().isOk());

        // Validate the Views in the database
        List<Views> viewsList = viewsRepository.findAll();
        assertThat(viewsList).hasSize(databaseSizeBeforeUpdate);
        Views testViews = viewsList.get(viewsList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingViews() throws Exception {
        int databaseSizeBeforeUpdate = viewsRepository.findAll().size();

        // Create the Views

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restViewsMockMvc.perform(put("/api/views")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(views)))
            .andExpect(status().isBadRequest());

        // Validate the Views in the database
        List<Views> viewsList = viewsRepository.findAll();
        assertThat(viewsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteViews() throws Exception {
        // Initialize the database
        viewsRepository.saveAndFlush(views);

        int databaseSizeBeforeDelete = viewsRepository.findAll().size();

        // Delete the views
        restViewsMockMvc.perform(delete("/api/views/{id}", views.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Views> viewsList = viewsRepository.findAll();
        assertThat(viewsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
