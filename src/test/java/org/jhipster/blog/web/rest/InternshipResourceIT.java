package org.jhipster.blog.web.rest;

import org.jhipster.blog.EnisIntershipApp;
import org.jhipster.blog.domain.Internship;
import org.jhipster.blog.repository.InternshipRepository;
import org.jhipster.blog.service.InternshipService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.jhipster.blog.domain.enumeration.Level;
import org.jhipster.blog.domain.enumeration.Domain;
/**
 * Integration tests for the {@link InternshipResource} REST controller.
 */
@SpringBootTest(classes = EnisIntershipApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class InternshipResourceIT {

    private static final byte[] DEFAULT_PHOTO = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_PHOTO = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_PHOTO_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_PHOTO_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_ENTITLED = "AAAAAAAAAA";
    private static final String UPDATED_ENTITLED = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_DURATION = "AAAAAAAAAA";
    private static final String UPDATED_DURATION = "BBBBBBBBBB";

    private static final Level DEFAULT_LEVEL = Level.FIRSTYEAR;
    private static final Level UPDATED_LEVEL = Level.SECONDYEAR;

    private static final Domain DEFAULT_DOMAIN = Domain.ELECTRONIC;
    private static final Domain UPDATED_DOMAIN = Domain.BIOLOGY;

    private static final byte[] DEFAULT_BROCHURE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_BROCHURE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_BROCHURE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_BROCHURE_CONTENT_TYPE = "image/png";

    @Autowired
    private InternshipRepository internshipRepository;

    @Autowired
    private InternshipService internshipService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restInternshipMockMvc;

    private Internship internship;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Internship createEntity(EntityManager em) {
        Internship internship = new Internship()
            .photo(DEFAULT_PHOTO)
            .photoContentType(DEFAULT_PHOTO_CONTENT_TYPE)
            .entitled(DEFAULT_ENTITLED)
            .description(DEFAULT_DESCRIPTION)
            .duration(DEFAULT_DURATION)
            .level(DEFAULT_LEVEL)
            .domain(DEFAULT_DOMAIN)
            .brochure(DEFAULT_BROCHURE)
            .brochureContentType(DEFAULT_BROCHURE_CONTENT_TYPE);
        return internship;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Internship createUpdatedEntity(EntityManager em) {
        Internship internship = new Internship()
            .photo(UPDATED_PHOTO)
            .photoContentType(UPDATED_PHOTO_CONTENT_TYPE)
            .entitled(UPDATED_ENTITLED)
            .description(UPDATED_DESCRIPTION)
            .duration(UPDATED_DURATION)
            .level(UPDATED_LEVEL)
            .domain(UPDATED_DOMAIN)
            .brochure(UPDATED_BROCHURE)
            .brochureContentType(UPDATED_BROCHURE_CONTENT_TYPE);
        return internship;
    }

    @BeforeEach
    public void initTest() {
        internship = createEntity(em);
    }

    @Test
    @Transactional
    public void createInternship() throws Exception {
        int databaseSizeBeforeCreate = internshipRepository.findAll().size();

        // Create the Internship
        restInternshipMockMvc.perform(post("/api/internships")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(internship)))
            .andExpect(status().isCreated());

        // Validate the Internship in the database
        List<Internship> internshipList = internshipRepository.findAll();
        assertThat(internshipList).hasSize(databaseSizeBeforeCreate + 1);
        Internship testInternship = internshipList.get(internshipList.size() - 1);
        assertThat(testInternship.getPhoto()).isEqualTo(DEFAULT_PHOTO);
        assertThat(testInternship.getPhotoContentType()).isEqualTo(DEFAULT_PHOTO_CONTENT_TYPE);
        assertThat(testInternship.getEntitled()).isEqualTo(DEFAULT_ENTITLED);
        assertThat(testInternship.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testInternship.getDuration()).isEqualTo(DEFAULT_DURATION);
        assertThat(testInternship.getLevel()).isEqualTo(DEFAULT_LEVEL);
        assertThat(testInternship.getDomain()).isEqualTo(DEFAULT_DOMAIN);
        assertThat(testInternship.getBrochure()).isEqualTo(DEFAULT_BROCHURE);
        assertThat(testInternship.getBrochureContentType()).isEqualTo(DEFAULT_BROCHURE_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void createInternshipWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = internshipRepository.findAll().size();

        // Create the Internship with an existing ID
        internship.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInternshipMockMvc.perform(post("/api/internships")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(internship)))
            .andExpect(status().isBadRequest());

        // Validate the Internship in the database
        List<Internship> internshipList = internshipRepository.findAll();
        assertThat(internshipList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkEntitledIsRequired() throws Exception {
        int databaseSizeBeforeTest = internshipRepository.findAll().size();
        // set the field null
        internship.setEntitled(null);

        // Create the Internship, which fails.

        restInternshipMockMvc.perform(post("/api/internships")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(internship)))
            .andExpect(status().isBadRequest());

        List<Internship> internshipList = internshipRepository.findAll();
        assertThat(internshipList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = internshipRepository.findAll().size();
        // set the field null
        internship.setDescription(null);

        // Create the Internship, which fails.

        restInternshipMockMvc.perform(post("/api/internships")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(internship)))
            .andExpect(status().isBadRequest());

        List<Internship> internshipList = internshipRepository.findAll();
        assertThat(internshipList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDurationIsRequired() throws Exception {
        int databaseSizeBeforeTest = internshipRepository.findAll().size();
        // set the field null
        internship.setDuration(null);

        // Create the Internship, which fails.

        restInternshipMockMvc.perform(post("/api/internships")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(internship)))
            .andExpect(status().isBadRequest());

        List<Internship> internshipList = internshipRepository.findAll();
        assertThat(internshipList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLevelIsRequired() throws Exception {
        int databaseSizeBeforeTest = internshipRepository.findAll().size();
        // set the field null
        internship.setLevel(null);

        // Create the Internship, which fails.

        restInternshipMockMvc.perform(post("/api/internships")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(internship)))
            .andExpect(status().isBadRequest());

        List<Internship> internshipList = internshipRepository.findAll();
        assertThat(internshipList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDomainIsRequired() throws Exception {
        int databaseSizeBeforeTest = internshipRepository.findAll().size();
        // set the field null
        internship.setDomain(null);

        // Create the Internship, which fails.

        restInternshipMockMvc.perform(post("/api/internships")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(internship)))
            .andExpect(status().isBadRequest());

        List<Internship> internshipList = internshipRepository.findAll();
        assertThat(internshipList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllInternships() throws Exception {
        // Initialize the database
        internshipRepository.saveAndFlush(internship);

        // Get all the internshipList
        restInternshipMockMvc.perform(get("/api/internships?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(internship.getId().intValue())))
            .andExpect(jsonPath("$.[*].photoContentType").value(hasItem(DEFAULT_PHOTO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].photo").value(hasItem(Base64Utils.encodeToString(DEFAULT_PHOTO))))
            .andExpect(jsonPath("$.[*].entitled").value(hasItem(DEFAULT_ENTITLED)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].duration").value(hasItem(DEFAULT_DURATION)))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL.toString())))
            .andExpect(jsonPath("$.[*].domain").value(hasItem(DEFAULT_DOMAIN.toString())))
            .andExpect(jsonPath("$.[*].brochureContentType").value(hasItem(DEFAULT_BROCHURE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].brochure").value(hasItem(Base64Utils.encodeToString(DEFAULT_BROCHURE))));
    }
    
    @Test
    @Transactional
    public void getInternship() throws Exception {
        // Initialize the database
        internshipRepository.saveAndFlush(internship);

        // Get the internship
        restInternshipMockMvc.perform(get("/api/internships/{id}", internship.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(internship.getId().intValue()))
            .andExpect(jsonPath("$.photoContentType").value(DEFAULT_PHOTO_CONTENT_TYPE))
            .andExpect(jsonPath("$.photo").value(Base64Utils.encodeToString(DEFAULT_PHOTO)))
            .andExpect(jsonPath("$.entitled").value(DEFAULT_ENTITLED))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.duration").value(DEFAULT_DURATION))
            .andExpect(jsonPath("$.level").value(DEFAULT_LEVEL.toString()))
            .andExpect(jsonPath("$.domain").value(DEFAULT_DOMAIN.toString()))
            .andExpect(jsonPath("$.brochureContentType").value(DEFAULT_BROCHURE_CONTENT_TYPE))
            .andExpect(jsonPath("$.brochure").value(Base64Utils.encodeToString(DEFAULT_BROCHURE)));
    }

    @Test
    @Transactional
    public void getNonExistingInternship() throws Exception {
        // Get the internship
        restInternshipMockMvc.perform(get("/api/internships/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInternship() throws Exception {
        // Initialize the database
        internshipService.save(internship);

        int databaseSizeBeforeUpdate = internshipRepository.findAll().size();

        // Update the internship
        Internship updatedInternship = internshipRepository.findById(internship.getId()).get();
        // Disconnect from session so that the updates on updatedInternship are not directly saved in db
        em.detach(updatedInternship);
        updatedInternship
            .photo(UPDATED_PHOTO)
            .photoContentType(UPDATED_PHOTO_CONTENT_TYPE)
            .entitled(UPDATED_ENTITLED)
            .description(UPDATED_DESCRIPTION)
            .duration(UPDATED_DURATION)
            .level(UPDATED_LEVEL)
            .domain(UPDATED_DOMAIN)
            .brochure(UPDATED_BROCHURE)
            .brochureContentType(UPDATED_BROCHURE_CONTENT_TYPE);

        restInternshipMockMvc.perform(put("/api/internships")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedInternship)))
            .andExpect(status().isOk());

        // Validate the Internship in the database
        List<Internship> internshipList = internshipRepository.findAll();
        assertThat(internshipList).hasSize(databaseSizeBeforeUpdate);
        Internship testInternship = internshipList.get(internshipList.size() - 1);
        assertThat(testInternship.getPhoto()).isEqualTo(UPDATED_PHOTO);
        assertThat(testInternship.getPhotoContentType()).isEqualTo(UPDATED_PHOTO_CONTENT_TYPE);
        assertThat(testInternship.getEntitled()).isEqualTo(UPDATED_ENTITLED);
        assertThat(testInternship.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testInternship.getDuration()).isEqualTo(UPDATED_DURATION);
        assertThat(testInternship.getLevel()).isEqualTo(UPDATED_LEVEL);
        assertThat(testInternship.getDomain()).isEqualTo(UPDATED_DOMAIN);
        assertThat(testInternship.getBrochure()).isEqualTo(UPDATED_BROCHURE);
        assertThat(testInternship.getBrochureContentType()).isEqualTo(UPDATED_BROCHURE_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingInternship() throws Exception {
        int databaseSizeBeforeUpdate = internshipRepository.findAll().size();

        // Create the Internship

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInternshipMockMvc.perform(put("/api/internships")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(internship)))
            .andExpect(status().isBadRequest());

        // Validate the Internship in the database
        List<Internship> internshipList = internshipRepository.findAll();
        assertThat(internshipList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteInternship() throws Exception {
        // Initialize the database
        internshipService.save(internship);

        int databaseSizeBeforeDelete = internshipRepository.findAll().size();

        // Delete the internship
        restInternshipMockMvc.perform(delete("/api/internships/{id}", internship.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Internship> internshipList = internshipRepository.findAll();
        assertThat(internshipList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
