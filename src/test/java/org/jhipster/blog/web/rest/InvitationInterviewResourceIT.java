package org.jhipster.blog.web.rest;

import org.jhipster.blog.EnisIntershipApp;
import org.jhipster.blog.domain.InvitationInterview;
import org.jhipster.blog.repository.InvitationInterviewRepository;
import org.jhipster.blog.service.InvitationInterviewService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link InvitationInterviewResource} REST controller.
 */
@SpringBootTest(classes = EnisIntershipApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class InvitationInterviewResourceIT {

    private static final String DEFAULT_CONFIRMATION = "AAAAAAAAAA";
    private static final String UPDATED_CONFIRMATION = "BBBBBBBBBB";

    private static final Instant DEFAULT_DATE_INTERVIEW = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_INTERVIEW = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private InvitationInterviewRepository invitationInterviewRepository;

    @Autowired
    private InvitationInterviewService invitationInterviewService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restInvitationInterviewMockMvc;

    private InvitationInterview invitationInterview;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InvitationInterview createEntity(EntityManager em) {
        InvitationInterview invitationInterview = new InvitationInterview()
            .confirmation(DEFAULT_CONFIRMATION)
            .dateInterview(DEFAULT_DATE_INTERVIEW);
        return invitationInterview;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InvitationInterview createUpdatedEntity(EntityManager em) {
        InvitationInterview invitationInterview = new InvitationInterview()
            .confirmation(UPDATED_CONFIRMATION)
            .dateInterview(UPDATED_DATE_INTERVIEW);
        return invitationInterview;
    }

    @BeforeEach
    public void initTest() {
        invitationInterview = createEntity(em);
    }

    @Test
    @Transactional
    public void createInvitationInterview() throws Exception {
        int databaseSizeBeforeCreate = invitationInterviewRepository.findAll().size();

        // Create the InvitationInterview
        restInvitationInterviewMockMvc.perform(post("/api/invitation-interviews")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(invitationInterview)))
            .andExpect(status().isCreated());

        // Validate the InvitationInterview in the database
        List<InvitationInterview> invitationInterviewList = invitationInterviewRepository.findAll();
        assertThat(invitationInterviewList).hasSize(databaseSizeBeforeCreate + 1);
        InvitationInterview testInvitationInterview = invitationInterviewList.get(invitationInterviewList.size() - 1);
        assertThat(testInvitationInterview.getConfirmation()).isEqualTo(DEFAULT_CONFIRMATION);
        assertThat(testInvitationInterview.getDateInterview()).isEqualTo(DEFAULT_DATE_INTERVIEW);
    }

    @Test
    @Transactional
    public void createInvitationInterviewWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = invitationInterviewRepository.findAll().size();

        // Create the InvitationInterview with an existing ID
        invitationInterview.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInvitationInterviewMockMvc.perform(post("/api/invitation-interviews")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(invitationInterview)))
            .andExpect(status().isBadRequest());

        // Validate the InvitationInterview in the database
        List<InvitationInterview> invitationInterviewList = invitationInterviewRepository.findAll();
        assertThat(invitationInterviewList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDateInterviewIsRequired() throws Exception {
        int databaseSizeBeforeTest = invitationInterviewRepository.findAll().size();
        // set the field null
        invitationInterview.setDateInterview(null);

        // Create the InvitationInterview, which fails.

        restInvitationInterviewMockMvc.perform(post("/api/invitation-interviews")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(invitationInterview)))
            .andExpect(status().isBadRequest());

        List<InvitationInterview> invitationInterviewList = invitationInterviewRepository.findAll();
        assertThat(invitationInterviewList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllInvitationInterviews() throws Exception {
        // Initialize the database
        invitationInterviewRepository.saveAndFlush(invitationInterview);

        // Get all the invitationInterviewList
        restInvitationInterviewMockMvc.perform(get("/api/invitation-interviews?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(invitationInterview.getId().intValue())))
            .andExpect(jsonPath("$.[*].confirmation").value(hasItem(DEFAULT_CONFIRMATION)))
            .andExpect(jsonPath("$.[*].dateInterview").value(hasItem(DEFAULT_DATE_INTERVIEW.toString())));
    }
    
    @Test
    @Transactional
    public void getInvitationInterview() throws Exception {
        // Initialize the database
        invitationInterviewRepository.saveAndFlush(invitationInterview);

        // Get the invitationInterview
        restInvitationInterviewMockMvc.perform(get("/api/invitation-interviews/{id}", invitationInterview.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(invitationInterview.getId().intValue()))
            .andExpect(jsonPath("$.confirmation").value(DEFAULT_CONFIRMATION))
            .andExpect(jsonPath("$.dateInterview").value(DEFAULT_DATE_INTERVIEW.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingInvitationInterview() throws Exception {
        // Get the invitationInterview
        restInvitationInterviewMockMvc.perform(get("/api/invitation-interviews/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInvitationInterview() throws Exception {
        // Initialize the database
        invitationInterviewService.save(invitationInterview);

        int databaseSizeBeforeUpdate = invitationInterviewRepository.findAll().size();

        // Update the invitationInterview
        InvitationInterview updatedInvitationInterview = invitationInterviewRepository.findById(invitationInterview.getId()).get();
        // Disconnect from session so that the updates on updatedInvitationInterview are not directly saved in db
        em.detach(updatedInvitationInterview);
        updatedInvitationInterview
            .confirmation(UPDATED_CONFIRMATION)
            .dateInterview(UPDATED_DATE_INTERVIEW);

        restInvitationInterviewMockMvc.perform(put("/api/invitation-interviews")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedInvitationInterview)))
            .andExpect(status().isOk());

        // Validate the InvitationInterview in the database
        List<InvitationInterview> invitationInterviewList = invitationInterviewRepository.findAll();
        assertThat(invitationInterviewList).hasSize(databaseSizeBeforeUpdate);
        InvitationInterview testInvitationInterview = invitationInterviewList.get(invitationInterviewList.size() - 1);
        assertThat(testInvitationInterview.getConfirmation()).isEqualTo(UPDATED_CONFIRMATION);
        assertThat(testInvitationInterview.getDateInterview()).isEqualTo(UPDATED_DATE_INTERVIEW);
    }

    @Test
    @Transactional
    public void updateNonExistingInvitationInterview() throws Exception {
        int databaseSizeBeforeUpdate = invitationInterviewRepository.findAll().size();

        // Create the InvitationInterview

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInvitationInterviewMockMvc.perform(put("/api/invitation-interviews")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(invitationInterview)))
            .andExpect(status().isBadRequest());

        // Validate the InvitationInterview in the database
        List<InvitationInterview> invitationInterviewList = invitationInterviewRepository.findAll();
        assertThat(invitationInterviewList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteInvitationInterview() throws Exception {
        // Initialize the database
        invitationInterviewService.save(invitationInterview);

        int databaseSizeBeforeDelete = invitationInterviewRepository.findAll().size();

        // Delete the invitationInterview
        restInvitationInterviewMockMvc.perform(delete("/api/invitation-interviews/{id}", invitationInterview.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<InvitationInterview> invitationInterviewList = invitationInterviewRepository.findAll();
        assertThat(invitationInterviewList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
