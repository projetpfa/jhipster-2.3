import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EnisIntershipTestModule } from '../../../test.module';
import { InvitationInterviewDetailComponent } from 'app/entities/invitation-interview/invitation-interview-detail.component';
import { InvitationInterview } from 'app/shared/model/invitation-interview.model';

describe('Component Tests', () => {
  describe('InvitationInterview Management Detail Component', () => {
    let comp: InvitationInterviewDetailComponent;
    let fixture: ComponentFixture<InvitationInterviewDetailComponent>;
    const route = ({ data: of({ invitationInterview: new InvitationInterview(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EnisIntershipTestModule],
        declarations: [InvitationInterviewDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(InvitationInterviewDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(InvitationInterviewDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load invitationInterview on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.invitationInterview).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
