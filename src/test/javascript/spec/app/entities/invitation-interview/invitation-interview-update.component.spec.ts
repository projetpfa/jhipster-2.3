import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EnisIntershipTestModule } from '../../../test.module';
import { InvitationInterviewUpdateComponent } from 'app/entities/invitation-interview/invitation-interview-update.component';
import { InvitationInterviewService } from 'app/entities/invitation-interview/invitation-interview.service';
import { InvitationInterview } from 'app/shared/model/invitation-interview.model';

describe('Component Tests', () => {
  describe('InvitationInterview Management Update Component', () => {
    let comp: InvitationInterviewUpdateComponent;
    let fixture: ComponentFixture<InvitationInterviewUpdateComponent>;
    let service: InvitationInterviewService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EnisIntershipTestModule],
        declarations: [InvitationInterviewUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(InvitationInterviewUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(InvitationInterviewUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(InvitationInterviewService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new InvitationInterview(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new InvitationInterview();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
