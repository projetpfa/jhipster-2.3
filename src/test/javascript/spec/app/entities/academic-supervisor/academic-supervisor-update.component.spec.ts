import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EnisIntershipTestModule } from '../../../test.module';
import { AcademicSupervisorUpdateComponent } from 'app/entities/academic-supervisor/academic-supervisor-update.component';
import { AcademicSupervisorService } from 'app/entities/academic-supervisor/academic-supervisor.service';
import { AcademicSupervisor } from 'app/shared/model/academic-supervisor.model';

describe('Component Tests', () => {
  describe('AcademicSupervisor Management Update Component', () => {
    let comp: AcademicSupervisorUpdateComponent;
    let fixture: ComponentFixture<AcademicSupervisorUpdateComponent>;
    let service: AcademicSupervisorService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EnisIntershipTestModule],
        declarations: [AcademicSupervisorUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(AcademicSupervisorUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AcademicSupervisorUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AcademicSupervisorService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new AcademicSupervisor(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new AcademicSupervisor();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
