import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { EnisIntershipTestModule } from '../../../test.module';
import { AcademicSupervisorComponent } from 'app/entities/academic-supervisor/academic-supervisor.component';
import { AcademicSupervisorService } from 'app/entities/academic-supervisor/academic-supervisor.service';
import { AcademicSupervisor } from 'app/shared/model/academic-supervisor.model';

describe('Component Tests', () => {
  describe('AcademicSupervisor Management Component', () => {
    let comp: AcademicSupervisorComponent;
    let fixture: ComponentFixture<AcademicSupervisorComponent>;
    let service: AcademicSupervisorService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EnisIntershipTestModule],
        declarations: [AcademicSupervisorComponent]
      })
        .overrideTemplate(AcademicSupervisorComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AcademicSupervisorComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AcademicSupervisorService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new AcademicSupervisor(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.academicSupervisors && comp.academicSupervisors[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
