import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { JhiDataUtils } from 'ng-jhipster';

import { EnisIntershipTestModule } from '../../../test.module';
import { AcademicSupervisorDetailComponent } from 'app/entities/academic-supervisor/academic-supervisor-detail.component';
import { AcademicSupervisor } from 'app/shared/model/academic-supervisor.model';

describe('Component Tests', () => {
  describe('AcademicSupervisor Management Detail Component', () => {
    let comp: AcademicSupervisorDetailComponent;
    let fixture: ComponentFixture<AcademicSupervisorDetailComponent>;
    let dataUtils: JhiDataUtils;
    const route = ({ data: of({ academicSupervisor: new AcademicSupervisor(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EnisIntershipTestModule],
        declarations: [AcademicSupervisorDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(AcademicSupervisorDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AcademicSupervisorDetailComponent);
      comp = fixture.componentInstance;
      dataUtils = fixture.debugElement.injector.get(JhiDataUtils);
    });

    describe('OnInit', () => {
      it('Should load academicSupervisor on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.academicSupervisor).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });

    describe('byteSize', () => {
      it('Should call byteSize from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'byteSize');
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.byteSize(fakeBase64);

        // THEN
        expect(dataUtils.byteSize).toBeCalledWith(fakeBase64);
      });
    });

    describe('openFile', () => {
      it('Should call openFile from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'openFile');
        const fakeContentType = 'fake content type';
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.openFile(fakeContentType, fakeBase64);

        // THEN
        expect(dataUtils.openFile).toBeCalledWith(fakeContentType, fakeBase64);
      });
    });
  });
});
