import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EnisIntershipTestModule } from '../../../test.module';
import { CompanyEvaluationDetailComponent } from 'app/entities/company-evaluation/company-evaluation-detail.component';
import { CompanyEvaluation } from 'app/shared/model/company-evaluation.model';

describe('Component Tests', () => {
  describe('CompanyEvaluation Management Detail Component', () => {
    let comp: CompanyEvaluationDetailComponent;
    let fixture: ComponentFixture<CompanyEvaluationDetailComponent>;
    const route = ({ data: of({ companyEvaluation: new CompanyEvaluation(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EnisIntershipTestModule],
        declarations: [CompanyEvaluationDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(CompanyEvaluationDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CompanyEvaluationDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load companyEvaluation on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.companyEvaluation).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
