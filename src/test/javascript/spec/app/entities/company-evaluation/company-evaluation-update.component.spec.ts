import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EnisIntershipTestModule } from '../../../test.module';
import { CompanyEvaluationUpdateComponent } from 'app/entities/company-evaluation/company-evaluation-update.component';
import { CompanyEvaluationService } from 'app/entities/company-evaluation/company-evaluation.service';
import { CompanyEvaluation } from 'app/shared/model/company-evaluation.model';

describe('Component Tests', () => {
  describe('CompanyEvaluation Management Update Component', () => {
    let comp: CompanyEvaluationUpdateComponent;
    let fixture: ComponentFixture<CompanyEvaluationUpdateComponent>;
    let service: CompanyEvaluationService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EnisIntershipTestModule],
        declarations: [CompanyEvaluationUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(CompanyEvaluationUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CompanyEvaluationUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CompanyEvaluationService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CompanyEvaluation(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CompanyEvaluation();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
