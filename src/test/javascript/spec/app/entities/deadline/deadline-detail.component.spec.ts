import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EnisIntershipTestModule } from '../../../test.module';
import { DeadlineDetailComponent } from 'app/entities/deadline/deadline-detail.component';
import { Deadline } from 'app/shared/model/deadline.model';

describe('Component Tests', () => {
  describe('Deadline Management Detail Component', () => {
    let comp: DeadlineDetailComponent;
    let fixture: ComponentFixture<DeadlineDetailComponent>;
    const route = ({ data: of({ deadline: new Deadline(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EnisIntershipTestModule],
        declarations: [DeadlineDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(DeadlineDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DeadlineDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load deadline on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.deadline).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
