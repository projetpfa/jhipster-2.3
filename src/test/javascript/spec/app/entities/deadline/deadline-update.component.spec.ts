import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EnisIntershipTestModule } from '../../../test.module';
import { DeadlineUpdateComponent } from 'app/entities/deadline/deadline-update.component';
import { DeadlineService } from 'app/entities/deadline/deadline.service';
import { Deadline } from 'app/shared/model/deadline.model';

describe('Component Tests', () => {
  describe('Deadline Management Update Component', () => {
    let comp: DeadlineUpdateComponent;
    let fixture: ComponentFixture<DeadlineUpdateComponent>;
    let service: DeadlineService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EnisIntershipTestModule],
        declarations: [DeadlineUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(DeadlineUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DeadlineUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DeadlineService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Deadline(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Deadline();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
