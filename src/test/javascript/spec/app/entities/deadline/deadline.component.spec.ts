import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { EnisIntershipTestModule } from '../../../test.module';
import { DeadlineComponent } from 'app/entities/deadline/deadline.component';
import { DeadlineService } from 'app/entities/deadline/deadline.service';
import { Deadline } from 'app/shared/model/deadline.model';

describe('Component Tests', () => {
  describe('Deadline Management Component', () => {
    let comp: DeadlineComponent;
    let fixture: ComponentFixture<DeadlineComponent>;
    let service: DeadlineService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EnisIntershipTestModule],
        declarations: [DeadlineComponent]
      })
        .overrideTemplate(DeadlineComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DeadlineComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DeadlineService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Deadline(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.deadlines && comp.deadlines[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
