import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EnisIntershipTestModule } from '../../../test.module';
import { InternshipUpdateComponent } from 'app/entities/internship/internship-update.component';
import { InternshipService } from 'app/entities/internship/internship.service';
import { Internship } from 'app/shared/model/internship.model';

describe('Component Tests', () => {
  describe('Internship Management Update Component', () => {
    let comp: InternshipUpdateComponent;
    let fixture: ComponentFixture<InternshipUpdateComponent>;
    let service: InternshipService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EnisIntershipTestModule],
        declarations: [InternshipUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(InternshipUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(InternshipUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(InternshipService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Internship(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Internship();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
