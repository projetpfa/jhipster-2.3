import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { InternshipService } from 'app/entities/internship/internship.service';
import { IInternship, Internship } from 'app/shared/model/internship.model';
import { Level } from 'app/shared/model/enumerations/level.model';
import { Domain } from 'app/shared/model/enumerations/domain.model';

describe('Service Tests', () => {
  describe('Internship Service', () => {
    let injector: TestBed;
    let service: InternshipService;
    let httpMock: HttpTestingController;
    let elemDefault: IInternship;
    let expectedResult: IInternship | IInternship[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(InternshipService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new Internship(
        0,
        'image/png',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        Level.FIRSTYEAR,
        Domain.ELECTRONIC,
        'image/png',
        'AAAAAAA'
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Internship', () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new Internship()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Internship', () => {
        const returnedFromService = Object.assign(
          {
            photo: 'BBBBBB',
            entitled: 'BBBBBB',
            description: 'BBBBBB',
            duration: 'BBBBBB',
            level: 'BBBBBB',
            domain: 'BBBBBB',
            brochure: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Internship', () => {
        const returnedFromService = Object.assign(
          {
            photo: 'BBBBBB',
            entitled: 'BBBBBB',
            description: 'BBBBBB',
            duration: 'BBBBBB',
            level: 'BBBBBB',
            domain: 'BBBBBB',
            brochure: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Internship', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
