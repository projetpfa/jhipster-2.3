import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { DeadlineComponentsPage, DeadlineDeleteDialog, DeadlineUpdatePage } from './deadline.page-object';

const expect = chai.expect;

describe('Deadline e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let deadlineComponentsPage: DeadlineComponentsPage;
  let deadlineUpdatePage: DeadlineUpdatePage;
  let deadlineDeleteDialog: DeadlineDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Deadlines', async () => {
    await navBarPage.goToEntity('deadline');
    deadlineComponentsPage = new DeadlineComponentsPage();
    await browser.wait(ec.visibilityOf(deadlineComponentsPage.title), 5000);
    expect(await deadlineComponentsPage.getTitle()).to.eq('enisIntershipApp.deadline.home.title');
    await browser.wait(ec.or(ec.visibilityOf(deadlineComponentsPage.entities), ec.visibilityOf(deadlineComponentsPage.noResult)), 1000);
  });

  it('should load create Deadline page', async () => {
    await deadlineComponentsPage.clickOnCreateButton();
    deadlineUpdatePage = new DeadlineUpdatePage();
    expect(await deadlineUpdatePage.getPageTitle()).to.eq('enisIntershipApp.deadline.home.createOrEditLabel');
    await deadlineUpdatePage.cancel();
  });

  it('should create and save Deadlines', async () => {
    const nbButtonsBeforeCreate = await deadlineComponentsPage.countDeleteButtons();

    await deadlineComponentsPage.clickOnCreateButton();

    await promise.all([
      deadlineUpdatePage.setStartInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      deadlineUpdatePage.setEndInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      deadlineUpdatePage.typeSelectLastOption()
    ]);

    expect(await deadlineUpdatePage.getStartInput()).to.contain('2001-01-01T02:30', 'Expected start value to be equals to 2000-12-31');
    expect(await deadlineUpdatePage.getEndInput()).to.contain('2001-01-01T02:30', 'Expected end value to be equals to 2000-12-31');

    await deadlineUpdatePage.save();
    expect(await deadlineUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await deadlineComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Deadline', async () => {
    const nbButtonsBeforeDelete = await deadlineComponentsPage.countDeleteButtons();
    await deadlineComponentsPage.clickOnLastDeleteButton();

    deadlineDeleteDialog = new DeadlineDeleteDialog();
    expect(await deadlineDeleteDialog.getDialogTitle()).to.eq('enisIntershipApp.deadline.delete.question');
    await deadlineDeleteDialog.clickOnConfirmButton();

    expect(await deadlineComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
