import { element, by, ElementFinder } from 'protractor';

export class InterviewComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-interview div table .btn-danger'));
  title = element.all(by.css('jhi-interview div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class InterviewUpdatePage {
  pageTitle = element(by.id('jhi-interview-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  dateInput = element(by.id('field_date'));
  presenceSelect = element(by.id('field_presence'));
  stateSelect = element(by.id('field_state'));
  communicationInput = element(by.id('field_communication'));
  scoreCommunicationInput = element(by.id('field_scoreCommunication'));
  technicalInput = element(by.id('field_technical'));
  scoretTechnicalInput = element(by.id('field_scoretTechnical'));
  otherInput = element(by.id('field_other'));
  studFinalChoiceSelect = element(by.id('field_studFinalChoice'));

  invitationInterviewSelect = element(by.id('field_invitationInterview'));
  companySelect = element(by.id('field_company'));
  studentSelect = element(by.id('field_student'));
  internshipSelect = element(by.id('field_internship'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setDateInput(date: string): Promise<void> {
    await this.dateInput.sendKeys(date);
  }

  async getDateInput(): Promise<string> {
    return await this.dateInput.getAttribute('value');
  }

  async setPresenceSelect(presence: string): Promise<void> {
    await this.presenceSelect.sendKeys(presence);
  }

  async getPresenceSelect(): Promise<string> {
    return await this.presenceSelect.element(by.css('option:checked')).getText();
  }

  async presenceSelectLastOption(): Promise<void> {
    await this.presenceSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setStateSelect(state: string): Promise<void> {
    await this.stateSelect.sendKeys(state);
  }

  async getStateSelect(): Promise<string> {
    return await this.stateSelect.element(by.css('option:checked')).getText();
  }

  async stateSelectLastOption(): Promise<void> {
    await this.stateSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setCommunicationInput(communication: string): Promise<void> {
    await this.communicationInput.sendKeys(communication);
  }

  async getCommunicationInput(): Promise<string> {
    return await this.communicationInput.getAttribute('value');
  }

  async setScoreCommunicationInput(scoreCommunication: string): Promise<void> {
    await this.scoreCommunicationInput.sendKeys(scoreCommunication);
  }

  async getScoreCommunicationInput(): Promise<string> {
    return await this.scoreCommunicationInput.getAttribute('value');
  }

  async setTechnicalInput(technical: string): Promise<void> {
    await this.technicalInput.sendKeys(technical);
  }

  async getTechnicalInput(): Promise<string> {
    return await this.technicalInput.getAttribute('value');
  }

  async setScoretTechnicalInput(scoretTechnical: string): Promise<void> {
    await this.scoretTechnicalInput.sendKeys(scoretTechnical);
  }

  async getScoretTechnicalInput(): Promise<string> {
    return await this.scoretTechnicalInput.getAttribute('value');
  }

  async setOtherInput(other: string): Promise<void> {
    await this.otherInput.sendKeys(other);
  }

  async getOtherInput(): Promise<string> {
    return await this.otherInput.getAttribute('value');
  }

  async setStudFinalChoiceSelect(studFinalChoice: string): Promise<void> {
    await this.studFinalChoiceSelect.sendKeys(studFinalChoice);
  }

  async getStudFinalChoiceSelect(): Promise<string> {
    return await this.studFinalChoiceSelect.element(by.css('option:checked')).getText();
  }

  async studFinalChoiceSelectLastOption(): Promise<void> {
    await this.studFinalChoiceSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async invitationInterviewSelectLastOption(): Promise<void> {
    await this.invitationInterviewSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async invitationInterviewSelectOption(option: string): Promise<void> {
    await this.invitationInterviewSelect.sendKeys(option);
  }

  getInvitationInterviewSelect(): ElementFinder {
    return this.invitationInterviewSelect;
  }

  async getInvitationInterviewSelectedOption(): Promise<string> {
    return await this.invitationInterviewSelect.element(by.css('option:checked')).getText();
  }

  async companySelectLastOption(): Promise<void> {
    await this.companySelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async companySelectOption(option: string): Promise<void> {
    await this.companySelect.sendKeys(option);
  }

  getCompanySelect(): ElementFinder {
    return this.companySelect;
  }

  async getCompanySelectedOption(): Promise<string> {
    return await this.companySelect.element(by.css('option:checked')).getText();
  }

  async studentSelectLastOption(): Promise<void> {
    await this.studentSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async studentSelectOption(option: string): Promise<void> {
    await this.studentSelect.sendKeys(option);
  }

  getStudentSelect(): ElementFinder {
    return this.studentSelect;
  }

  async getStudentSelectedOption(): Promise<string> {
    return await this.studentSelect.element(by.css('option:checked')).getText();
  }

  async internshipSelectLastOption(): Promise<void> {
    await this.internshipSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async internshipSelectOption(option: string): Promise<void> {
    await this.internshipSelect.sendKeys(option);
  }

  getInternshipSelect(): ElementFinder {
    return this.internshipSelect;
  }

  async getInternshipSelectedOption(): Promise<string> {
    return await this.internshipSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class InterviewDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-interview-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-interview'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
