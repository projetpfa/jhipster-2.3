import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { InterviewComponentsPage, InterviewDeleteDialog, InterviewUpdatePage } from './interview.page-object';

const expect = chai.expect;

describe('Interview e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let interviewComponentsPage: InterviewComponentsPage;
  let interviewUpdatePage: InterviewUpdatePage;
  let interviewDeleteDialog: InterviewDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Interviews', async () => {
    await navBarPage.goToEntity('interview');
    interviewComponentsPage = new InterviewComponentsPage();
    await browser.wait(ec.visibilityOf(interviewComponentsPage.title), 5000);
    expect(await interviewComponentsPage.getTitle()).to.eq('enisIntershipApp.interview.home.title');
    await browser.wait(ec.or(ec.visibilityOf(interviewComponentsPage.entities), ec.visibilityOf(interviewComponentsPage.noResult)), 1000);
  });

  it('should load create Interview page', async () => {
    await interviewComponentsPage.clickOnCreateButton();
    interviewUpdatePage = new InterviewUpdatePage();
    expect(await interviewUpdatePage.getPageTitle()).to.eq('enisIntershipApp.interview.home.createOrEditLabel');
    await interviewUpdatePage.cancel();
  });

  it('should create and save Interviews', async () => {
    const nbButtonsBeforeCreate = await interviewComponentsPage.countDeleteButtons();

    await interviewComponentsPage.clickOnCreateButton();

    await promise.all([
      interviewUpdatePage.setDateInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      interviewUpdatePage.presenceSelectLastOption(),
      interviewUpdatePage.stateSelectLastOption(),
      interviewUpdatePage.setCommunicationInput('communication'),
      interviewUpdatePage.setScoreCommunicationInput('5'),
      interviewUpdatePage.setTechnicalInput('technical'),
      interviewUpdatePage.setScoretTechnicalInput('5'),
      interviewUpdatePage.setOtherInput('other'),
      interviewUpdatePage.studFinalChoiceSelectLastOption(),
      interviewUpdatePage.invitationInterviewSelectLastOption(),
      interviewUpdatePage.companySelectLastOption(),
      interviewUpdatePage.studentSelectLastOption(),
      interviewUpdatePage.internshipSelectLastOption()
    ]);

    expect(await interviewUpdatePage.getDateInput()).to.contain('2001-01-01T02:30', 'Expected date value to be equals to 2000-12-31');
    expect(await interviewUpdatePage.getCommunicationInput()).to.eq(
      'communication',
      'Expected Communication value to be equals to communication'
    );
    expect(await interviewUpdatePage.getScoreCommunicationInput()).to.eq('5', 'Expected scoreCommunication value to be equals to 5');
    expect(await interviewUpdatePage.getTechnicalInput()).to.eq('technical', 'Expected Technical value to be equals to technical');
    expect(await interviewUpdatePage.getScoretTechnicalInput()).to.eq('5', 'Expected scoretTechnical value to be equals to 5');
    expect(await interviewUpdatePage.getOtherInput()).to.eq('other', 'Expected Other value to be equals to other');

    await interviewUpdatePage.save();
    expect(await interviewUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await interviewComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Interview', async () => {
    const nbButtonsBeforeDelete = await interviewComponentsPage.countDeleteButtons();
    await interviewComponentsPage.clickOnLastDeleteButton();

    interviewDeleteDialog = new InterviewDeleteDialog();
    expect(await interviewDeleteDialog.getDialogTitle()).to.eq('enisIntershipApp.interview.delete.question');
    await interviewDeleteDialog.clickOnConfirmButton();

    expect(await interviewComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
