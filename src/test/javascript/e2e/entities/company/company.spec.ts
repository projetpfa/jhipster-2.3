import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { CompanyComponentsPage, CompanyDeleteDialog, CompanyUpdatePage } from './company.page-object';
import * as path from 'path';

const expect = chai.expect;

describe('Company e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let companyComponentsPage: CompanyComponentsPage;
  let companyUpdatePage: CompanyUpdatePage;
  let companyDeleteDialog: CompanyDeleteDialog;
  const fileNameToUpload = 'logo-jhipster.png';
  const fileToUpload = '../../../../../../src/main/webapp/content/images/' + fileNameToUpload;
  const absolutePath = path.resolve(__dirname, fileToUpload);

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Companies', async () => {
    await navBarPage.goToEntity('company');
    companyComponentsPage = new CompanyComponentsPage();
    await browser.wait(ec.visibilityOf(companyComponentsPage.title), 5000);
    expect(await companyComponentsPage.getTitle()).to.eq('enisIntershipApp.company.home.title');
    await browser.wait(ec.or(ec.visibilityOf(companyComponentsPage.entities), ec.visibilityOf(companyComponentsPage.noResult)), 1000);
  });

  it('should load create Company page', async () => {
    await companyComponentsPage.clickOnCreateButton();
    companyUpdatePage = new CompanyUpdatePage();
    expect(await companyUpdatePage.getPageTitle()).to.eq('enisIntershipApp.company.home.createOrEditLabel');
    await companyUpdatePage.cancel();
  });

  it('should create and save Companies', async () => {
    const nbButtonsBeforeCreate = await companyComponentsPage.countDeleteButtons();

    await companyComponentsPage.clickOnCreateButton();

    await promise.all([
      companyUpdatePage.setPhotoInput(absolutePath),
      companyUpdatePage.setCityInput('city'),
      companyUpdatePage.setCountryInput('country'),
      companyUpdatePage.setPostalCodeInput('5'),
      companyUpdatePage.setPhoneNumberInput('5'),
      companyUpdatePage.setNameCompanyInput('nameCompany'),
      companyUpdatePage.setDescriptionInput('description'),
      companyUpdatePage.setBrochureCompanyInput(absolutePath),
      companyUpdatePage.setWebsiteCompanyInput('websiteCompany'),
      companyUpdatePage.userSelectLastOption()
    ]);

    expect(await companyUpdatePage.getPhotoInput()).to.endsWith(
      fileNameToUpload,
      'Expected Photo value to be end with ' + fileNameToUpload
    );
    expect(await companyUpdatePage.getCityInput()).to.eq('city', 'Expected City value to be equals to city');
    expect(await companyUpdatePage.getCountryInput()).to.eq('country', 'Expected Country value to be equals to country');
    expect(await companyUpdatePage.getPostalCodeInput()).to.eq('5', 'Expected postalCode value to be equals to 5');
    expect(await companyUpdatePage.getPhoneNumberInput()).to.eq('5', 'Expected phoneNumber value to be equals to 5');
    expect(await companyUpdatePage.getNameCompanyInput()).to.eq('nameCompany', 'Expected NameCompany value to be equals to nameCompany');
    expect(await companyUpdatePage.getDescriptionInput()).to.eq('description', 'Expected Description value to be equals to description');
    expect(await companyUpdatePage.getBrochureCompanyInput()).to.endsWith(
      fileNameToUpload,
      'Expected BrochureCompany value to be end with ' + fileNameToUpload
    );
    expect(await companyUpdatePage.getWebsiteCompanyInput()).to.eq(
      'websiteCompany',
      'Expected WebsiteCompany value to be equals to websiteCompany'
    );

    await companyUpdatePage.save();
    expect(await companyUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await companyComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Company', async () => {
    const nbButtonsBeforeDelete = await companyComponentsPage.countDeleteButtons();
    await companyComponentsPage.clickOnLastDeleteButton();

    companyDeleteDialog = new CompanyDeleteDialog();
    expect(await companyDeleteDialog.getDialogTitle()).to.eq('enisIntershipApp.company.delete.question');
    await companyDeleteDialog.clickOnConfirmButton();

    expect(await companyComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
