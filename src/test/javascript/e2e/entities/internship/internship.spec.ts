import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { InternshipComponentsPage, InternshipDeleteDialog, InternshipUpdatePage } from './internship.page-object';
import * as path from 'path';

const expect = chai.expect;

describe('Internship e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let internshipComponentsPage: InternshipComponentsPage;
  let internshipUpdatePage: InternshipUpdatePage;
  let internshipDeleteDialog: InternshipDeleteDialog;
  const fileNameToUpload = 'logo-jhipster.png';
  const fileToUpload = '../../../../../../src/main/webapp/content/images/' + fileNameToUpload;
  const absolutePath = path.resolve(__dirname, fileToUpload);

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Internships', async () => {
    await navBarPage.goToEntity('internship');
    internshipComponentsPage = new InternshipComponentsPage();
    await browser.wait(ec.visibilityOf(internshipComponentsPage.title), 5000);
    expect(await internshipComponentsPage.getTitle()).to.eq('enisIntershipApp.internship.home.title');
    await browser.wait(ec.or(ec.visibilityOf(internshipComponentsPage.entities), ec.visibilityOf(internshipComponentsPage.noResult)), 1000);
  });

  it('should load create Internship page', async () => {
    await internshipComponentsPage.clickOnCreateButton();
    internshipUpdatePage = new InternshipUpdatePage();
    expect(await internshipUpdatePage.getPageTitle()).to.eq('enisIntershipApp.internship.home.createOrEditLabel');
    await internshipUpdatePage.cancel();
  });

  it('should create and save Internships', async () => {
    const nbButtonsBeforeCreate = await internshipComponentsPage.countDeleteButtons();

    await internshipComponentsPage.clickOnCreateButton();

    await promise.all([
      internshipUpdatePage.setPhotoInput(absolutePath),
      internshipUpdatePage.setEntitledInput('entitled'),
      internshipUpdatePage.setDescriptionInput('description'),
      internshipUpdatePage.setDurationInput('duration'),
      internshipUpdatePage.levelSelectLastOption(),
      internshipUpdatePage.domainSelectLastOption(),
      internshipUpdatePage.setBrochureInput(absolutePath),
      internshipUpdatePage.companySelectLastOption()
    ]);

    expect(await internshipUpdatePage.getPhotoInput()).to.endsWith(
      fileNameToUpload,
      'Expected Photo value to be end with ' + fileNameToUpload
    );
    expect(await internshipUpdatePage.getEntitledInput()).to.eq('entitled', 'Expected Entitled value to be equals to entitled');
    expect(await internshipUpdatePage.getDescriptionInput()).to.eq('description', 'Expected Description value to be equals to description');
    expect(await internshipUpdatePage.getDurationInput()).to.eq('duration', 'Expected Duration value to be equals to duration');
    expect(await internshipUpdatePage.getBrochureInput()).to.endsWith(
      fileNameToUpload,
      'Expected Brochure value to be end with ' + fileNameToUpload
    );

    await internshipUpdatePage.save();
    expect(await internshipUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await internshipComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Internship', async () => {
    const nbButtonsBeforeDelete = await internshipComponentsPage.countDeleteButtons();
    await internshipComponentsPage.clickOnLastDeleteButton();

    internshipDeleteDialog = new InternshipDeleteDialog();
    expect(await internshipDeleteDialog.getDialogTitle()).to.eq('enisIntershipApp.internship.delete.question');
    await internshipDeleteDialog.clickOnConfirmButton();

    expect(await internshipComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
