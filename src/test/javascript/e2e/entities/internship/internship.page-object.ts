import { element, by, ElementFinder } from 'protractor';

export class InternshipComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-internship div table .btn-danger'));
  title = element.all(by.css('jhi-internship div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class InternshipUpdatePage {
  pageTitle = element(by.id('jhi-internship-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  photoInput = element(by.id('file_photo'));
  entitledInput = element(by.id('field_entitled'));
  descriptionInput = element(by.id('field_description'));
  durationInput = element(by.id('field_duration'));
  levelSelect = element(by.id('field_level'));
  domainSelect = element(by.id('field_domain'));
  brochureInput = element(by.id('file_brochure'));

  companySelect = element(by.id('field_company'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setPhotoInput(photo: string): Promise<void> {
    await this.photoInput.sendKeys(photo);
  }

  async getPhotoInput(): Promise<string> {
    return await this.photoInput.getAttribute('value');
  }

  async setEntitledInput(entitled: string): Promise<void> {
    await this.entitledInput.sendKeys(entitled);
  }

  async getEntitledInput(): Promise<string> {
    return await this.entitledInput.getAttribute('value');
  }

  async setDescriptionInput(description: string): Promise<void> {
    await this.descriptionInput.sendKeys(description);
  }

  async getDescriptionInput(): Promise<string> {
    return await this.descriptionInput.getAttribute('value');
  }

  async setDurationInput(duration: string): Promise<void> {
    await this.durationInput.sendKeys(duration);
  }

  async getDurationInput(): Promise<string> {
    return await this.durationInput.getAttribute('value');
  }

  async setLevelSelect(level: string): Promise<void> {
    await this.levelSelect.sendKeys(level);
  }

  async getLevelSelect(): Promise<string> {
    return await this.levelSelect.element(by.css('option:checked')).getText();
  }

  async levelSelectLastOption(): Promise<void> {
    await this.levelSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setDomainSelect(domain: string): Promise<void> {
    await this.domainSelect.sendKeys(domain);
  }

  async getDomainSelect(): Promise<string> {
    return await this.domainSelect.element(by.css('option:checked')).getText();
  }

  async domainSelectLastOption(): Promise<void> {
    await this.domainSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setBrochureInput(brochure: string): Promise<void> {
    await this.brochureInput.sendKeys(brochure);
  }

  async getBrochureInput(): Promise<string> {
    return await this.brochureInput.getAttribute('value');
  }

  async companySelectLastOption(): Promise<void> {
    await this.companySelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async companySelectOption(option: string): Promise<void> {
    await this.companySelect.sendKeys(option);
  }

  getCompanySelect(): ElementFinder {
    return this.companySelect;
  }

  async getCompanySelectedOption(): Promise<string> {
    return await this.companySelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class InternshipDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-internship-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-internship'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
