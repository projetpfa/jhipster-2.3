import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  InvitationInterviewComponentsPage,
  InvitationInterviewDeleteDialog,
  InvitationInterviewUpdatePage
} from './invitation-interview.page-object';

const expect = chai.expect;

describe('InvitationInterview e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let invitationInterviewComponentsPage: InvitationInterviewComponentsPage;
  let invitationInterviewUpdatePage: InvitationInterviewUpdatePage;
  let invitationInterviewDeleteDialog: InvitationInterviewDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load InvitationInterviews', async () => {
    await navBarPage.goToEntity('invitation-interview');
    invitationInterviewComponentsPage = new InvitationInterviewComponentsPage();
    await browser.wait(ec.visibilityOf(invitationInterviewComponentsPage.title), 5000);
    expect(await invitationInterviewComponentsPage.getTitle()).to.eq('enisIntershipApp.invitationInterview.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(invitationInterviewComponentsPage.entities), ec.visibilityOf(invitationInterviewComponentsPage.noResult)),
      1000
    );
  });

  it('should load create InvitationInterview page', async () => {
    await invitationInterviewComponentsPage.clickOnCreateButton();
    invitationInterviewUpdatePage = new InvitationInterviewUpdatePage();
    expect(await invitationInterviewUpdatePage.getPageTitle()).to.eq('enisIntershipApp.invitationInterview.home.createOrEditLabel');
    await invitationInterviewUpdatePage.cancel();
  });

  it('should create and save InvitationInterviews', async () => {
    const nbButtonsBeforeCreate = await invitationInterviewComponentsPage.countDeleteButtons();

    await invitationInterviewComponentsPage.clickOnCreateButton();

    await promise.all([
      invitationInterviewUpdatePage.setConfirmationInput('confirmation'),
      invitationInterviewUpdatePage.setDateInterviewInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      invitationInterviewUpdatePage.applicationSelectLastOption(),
      invitationInterviewUpdatePage.companySelectLastOption()
    ]);

    expect(await invitationInterviewUpdatePage.getConfirmationInput()).to.eq(
      'confirmation',
      'Expected Confirmation value to be equals to confirmation'
    );
    expect(await invitationInterviewUpdatePage.getDateInterviewInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateInterview value to be equals to 2000-12-31'
    );

    await invitationInterviewUpdatePage.save();
    expect(await invitationInterviewUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await invitationInterviewComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last InvitationInterview', async () => {
    const nbButtonsBeforeDelete = await invitationInterviewComponentsPage.countDeleteButtons();
    await invitationInterviewComponentsPage.clickOnLastDeleteButton();

    invitationInterviewDeleteDialog = new InvitationInterviewDeleteDialog();
    expect(await invitationInterviewDeleteDialog.getDialogTitle()).to.eq('enisIntershipApp.invitationInterview.delete.question');
    await invitationInterviewDeleteDialog.clickOnConfirmButton();

    expect(await invitationInterviewComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
