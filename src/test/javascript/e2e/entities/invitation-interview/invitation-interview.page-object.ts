import { element, by, ElementFinder } from 'protractor';

export class InvitationInterviewComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-invitation-interview div table .btn-danger'));
  title = element.all(by.css('jhi-invitation-interview div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class InvitationInterviewUpdatePage {
  pageTitle = element(by.id('jhi-invitation-interview-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  confirmationInput = element(by.id('field_confirmation'));
  dateInterviewInput = element(by.id('field_dateInterview'));

  applicationSelect = element(by.id('field_application'));
  companySelect = element(by.id('field_company'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setConfirmationInput(confirmation: string): Promise<void> {
    await this.confirmationInput.sendKeys(confirmation);
  }

  async getConfirmationInput(): Promise<string> {
    return await this.confirmationInput.getAttribute('value');
  }

  async setDateInterviewInput(dateInterview: string): Promise<void> {
    await this.dateInterviewInput.sendKeys(dateInterview);
  }

  async getDateInterviewInput(): Promise<string> {
    return await this.dateInterviewInput.getAttribute('value');
  }

  async applicationSelectLastOption(): Promise<void> {
    await this.applicationSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async applicationSelectOption(option: string): Promise<void> {
    await this.applicationSelect.sendKeys(option);
  }

  getApplicationSelect(): ElementFinder {
    return this.applicationSelect;
  }

  async getApplicationSelectedOption(): Promise<string> {
    return await this.applicationSelect.element(by.css('option:checked')).getText();
  }

  async companySelectLastOption(): Promise<void> {
    await this.companySelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async companySelectOption(option: string): Promise<void> {
    await this.companySelect.sendKeys(option);
  }

  getCompanySelect(): ElementFinder {
    return this.companySelect;
  }

  async getCompanySelectedOption(): Promise<string> {
    return await this.companySelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class InvitationInterviewDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-invitationInterview-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-invitationInterview'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
