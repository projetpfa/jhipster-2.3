import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ApplicationComponentsPage, ApplicationDeleteDialog, ApplicationUpdatePage } from './application.page-object';
import * as path from 'path';

const expect = chai.expect;

describe('Application e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let applicationComponentsPage: ApplicationComponentsPage;
  let applicationUpdatePage: ApplicationUpdatePage;
  let applicationDeleteDialog: ApplicationDeleteDialog;
  const fileNameToUpload = 'logo-jhipster.png';
  const fileToUpload = '../../../../../../src/main/webapp/content/images/' + fileNameToUpload;
  const absolutePath = path.resolve(__dirname, fileToUpload);

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Applications', async () => {
    await navBarPage.goToEntity('application');
    applicationComponentsPage = new ApplicationComponentsPage();
    await browser.wait(ec.visibilityOf(applicationComponentsPage.title), 5000);
    expect(await applicationComponentsPage.getTitle()).to.eq('enisIntershipApp.application.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(applicationComponentsPage.entities), ec.visibilityOf(applicationComponentsPage.noResult)),
      1000
    );
  });

  it('should load create Application page', async () => {
    await applicationComponentsPage.clickOnCreateButton();
    applicationUpdatePage = new ApplicationUpdatePage();
    expect(await applicationUpdatePage.getPageTitle()).to.eq('enisIntershipApp.application.home.createOrEditLabel');
    await applicationUpdatePage.cancel();
  });

  it('should create and save Applications', async () => {
    const nbButtonsBeforeCreate = await applicationComponentsPage.countDeleteButtons();

    await applicationComponentsPage.clickOnCreateButton();

    await promise.all([
      applicationUpdatePage.setCvInput(absolutePath),
      applicationUpdatePage.setFirstDisponibilityInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      applicationUpdatePage.setSecondDisponibilityInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      applicationUpdatePage.setThirdDisponibilityInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      applicationUpdatePage.studentSelectLastOption(),
      applicationUpdatePage.internshipSelectLastOption()
    ]);

    expect(await applicationUpdatePage.getCvInput()).to.endsWith(fileNameToUpload, 'Expected Cv value to be end with ' + fileNameToUpload);
    expect(await applicationUpdatePage.getFirstDisponibilityInput()).to.contain(
      '2001-01-01T02:30',
      'Expected firstDisponibility value to be equals to 2000-12-31'
    );
    expect(await applicationUpdatePage.getSecondDisponibilityInput()).to.contain(
      '2001-01-01T02:30',
      'Expected secondDisponibility value to be equals to 2000-12-31'
    );
    expect(await applicationUpdatePage.getThirdDisponibilityInput()).to.contain(
      '2001-01-01T02:30',
      'Expected thirdDisponibility value to be equals to 2000-12-31'
    );

    await applicationUpdatePage.save();
    expect(await applicationUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await applicationComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Application', async () => {
    const nbButtonsBeforeDelete = await applicationComponentsPage.countDeleteButtons();
    await applicationComponentsPage.clickOnLastDeleteButton();

    applicationDeleteDialog = new ApplicationDeleteDialog();
    expect(await applicationDeleteDialog.getDialogTitle()).to.eq('enisIntershipApp.application.delete.question');
    await applicationDeleteDialog.clickOnConfirmButton();

    expect(await applicationComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
