import { element, by, ElementFinder } from 'protractor';

export class ApplicationComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-application div table .btn-danger'));
  title = element.all(by.css('jhi-application div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class ApplicationUpdatePage {
  pageTitle = element(by.id('jhi-application-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  cvInput = element(by.id('file_cv'));
  firstDisponibilityInput = element(by.id('field_firstDisponibility'));
  secondDisponibilityInput = element(by.id('field_secondDisponibility'));
  thirdDisponibilityInput = element(by.id('field_thirdDisponibility'));

  studentSelect = element(by.id('field_student'));
  internshipSelect = element(by.id('field_internship'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setCvInput(cv: string): Promise<void> {
    await this.cvInput.sendKeys(cv);
  }

  async getCvInput(): Promise<string> {
    return await this.cvInput.getAttribute('value');
  }

  async setFirstDisponibilityInput(firstDisponibility: string): Promise<void> {
    await this.firstDisponibilityInput.sendKeys(firstDisponibility);
  }

  async getFirstDisponibilityInput(): Promise<string> {
    return await this.firstDisponibilityInput.getAttribute('value');
  }

  async setSecondDisponibilityInput(secondDisponibility: string): Promise<void> {
    await this.secondDisponibilityInput.sendKeys(secondDisponibility);
  }

  async getSecondDisponibilityInput(): Promise<string> {
    return await this.secondDisponibilityInput.getAttribute('value');
  }

  async setThirdDisponibilityInput(thirdDisponibility: string): Promise<void> {
    await this.thirdDisponibilityInput.sendKeys(thirdDisponibility);
  }

  async getThirdDisponibilityInput(): Promise<string> {
    return await this.thirdDisponibilityInput.getAttribute('value');
  }

  async studentSelectLastOption(): Promise<void> {
    await this.studentSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async studentSelectOption(option: string): Promise<void> {
    await this.studentSelect.sendKeys(option);
  }

  getStudentSelect(): ElementFinder {
    return this.studentSelect;
  }

  async getStudentSelectedOption(): Promise<string> {
    return await this.studentSelect.element(by.css('option:checked')).getText();
  }

  async internshipSelectLastOption(): Promise<void> {
    await this.internshipSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async internshipSelectOption(option: string): Promise<void> {
    await this.internshipSelect.sendKeys(option);
  }

  getInternshipSelect(): ElementFinder {
    return this.internshipSelect;
  }

  async getInternshipSelectedOption(): Promise<string> {
    return await this.internshipSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ApplicationDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-application-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-application'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
