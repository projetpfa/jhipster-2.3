import { element, by, ElementFinder } from 'protractor';

export class AffectationComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-affectation div table .btn-danger'));
  title = element.all(by.css('jhi-affectation div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class AffectationUpdatePage {
  pageTitle = element(by.id('jhi-affectation-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  dateAffectationInput = element(by.id('field_dateAffectation'));
  startDateInput = element(by.id('field_startDate'));
  endDateInput = element(by.id('field_endDate'));

  studentSelect = element(by.id('field_student'));
  internshipSelect = element(by.id('field_internship'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setDateAffectationInput(dateAffectation: string): Promise<void> {
    await this.dateAffectationInput.sendKeys(dateAffectation);
  }

  async getDateAffectationInput(): Promise<string> {
    return await this.dateAffectationInput.getAttribute('value');
  }

  async setStartDateInput(startDate: string): Promise<void> {
    await this.startDateInput.sendKeys(startDate);
  }

  async getStartDateInput(): Promise<string> {
    return await this.startDateInput.getAttribute('value');
  }

  async setEndDateInput(endDate: string): Promise<void> {
    await this.endDateInput.sendKeys(endDate);
  }

  async getEndDateInput(): Promise<string> {
    return await this.endDateInput.getAttribute('value');
  }

  async studentSelectLastOption(): Promise<void> {
    await this.studentSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async studentSelectOption(option: string): Promise<void> {
    await this.studentSelect.sendKeys(option);
  }

  getStudentSelect(): ElementFinder {
    return this.studentSelect;
  }

  async getStudentSelectedOption(): Promise<string> {
    return await this.studentSelect.element(by.css('option:checked')).getText();
  }

  async internshipSelectLastOption(): Promise<void> {
    await this.internshipSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async internshipSelectOption(option: string): Promise<void> {
    await this.internshipSelect.sendKeys(option);
  }

  getInternshipSelect(): ElementFinder {
    return this.internshipSelect;
  }

  async getInternshipSelectedOption(): Promise<string> {
    return await this.internshipSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class AffectationDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-affectation-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-affectation'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
