import { element, by, ElementFinder } from 'protractor';

export class CompanyEvaluationComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-company-evaluation div table .btn-danger'));
  title = element.all(by.css('jhi-company-evaluation div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class CompanyEvaluationUpdatePage {
  pageTitle = element(by.id('jhi-company-evaluation-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  supervisionInput = element(by.id('field_supervision'));
  acceuilInput = element(by.id('field_acceuil'));

  affectationSelect = element(by.id('field_affectation'));
  companySelect = element(by.id('field_company'));
  studentSelect = element(by.id('field_student'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setSupervisionInput(supervision: string): Promise<void> {
    await this.supervisionInput.sendKeys(supervision);
  }

  async getSupervisionInput(): Promise<string> {
    return await this.supervisionInput.getAttribute('value');
  }

  async setAcceuilInput(acceuil: string): Promise<void> {
    await this.acceuilInput.sendKeys(acceuil);
  }

  async getAcceuilInput(): Promise<string> {
    return await this.acceuilInput.getAttribute('value');
  }

  async affectationSelectLastOption(): Promise<void> {
    await this.affectationSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async affectationSelectOption(option: string): Promise<void> {
    await this.affectationSelect.sendKeys(option);
  }

  getAffectationSelect(): ElementFinder {
    return this.affectationSelect;
  }

  async getAffectationSelectedOption(): Promise<string> {
    return await this.affectationSelect.element(by.css('option:checked')).getText();
  }

  async companySelectLastOption(): Promise<void> {
    await this.companySelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async companySelectOption(option: string): Promise<void> {
    await this.companySelect.sendKeys(option);
  }

  getCompanySelect(): ElementFinder {
    return this.companySelect;
  }

  async getCompanySelectedOption(): Promise<string> {
    return await this.companySelect.element(by.css('option:checked')).getText();
  }

  async studentSelectLastOption(): Promise<void> {
    await this.studentSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async studentSelectOption(option: string): Promise<void> {
    await this.studentSelect.sendKeys(option);
  }

  getStudentSelect(): ElementFinder {
    return this.studentSelect;
  }

  async getStudentSelectedOption(): Promise<string> {
    return await this.studentSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class CompanyEvaluationDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-companyEvaluation-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-companyEvaluation'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
