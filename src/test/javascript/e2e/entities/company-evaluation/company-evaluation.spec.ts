import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  CompanyEvaluationComponentsPage,
  CompanyEvaluationDeleteDialog,
  CompanyEvaluationUpdatePage
} from './company-evaluation.page-object';

const expect = chai.expect;

describe('CompanyEvaluation e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let companyEvaluationComponentsPage: CompanyEvaluationComponentsPage;
  let companyEvaluationUpdatePage: CompanyEvaluationUpdatePage;
  let companyEvaluationDeleteDialog: CompanyEvaluationDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load CompanyEvaluations', async () => {
    await navBarPage.goToEntity('company-evaluation');
    companyEvaluationComponentsPage = new CompanyEvaluationComponentsPage();
    await browser.wait(ec.visibilityOf(companyEvaluationComponentsPage.title), 5000);
    expect(await companyEvaluationComponentsPage.getTitle()).to.eq('enisIntershipApp.companyEvaluation.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(companyEvaluationComponentsPage.entities), ec.visibilityOf(companyEvaluationComponentsPage.noResult)),
      1000
    );
  });

  it('should load create CompanyEvaluation page', async () => {
    await companyEvaluationComponentsPage.clickOnCreateButton();
    companyEvaluationUpdatePage = new CompanyEvaluationUpdatePage();
    expect(await companyEvaluationUpdatePage.getPageTitle()).to.eq('enisIntershipApp.companyEvaluation.home.createOrEditLabel');
    await companyEvaluationUpdatePage.cancel();
  });

  it('should create and save CompanyEvaluations', async () => {
    const nbButtonsBeforeCreate = await companyEvaluationComponentsPage.countDeleteButtons();

    await companyEvaluationComponentsPage.clickOnCreateButton();

    await promise.all([
      companyEvaluationUpdatePage.setSupervisionInput('5'),
      companyEvaluationUpdatePage.setAcceuilInput('5'),
      companyEvaluationUpdatePage.affectationSelectLastOption(),
      companyEvaluationUpdatePage.companySelectLastOption(),
      companyEvaluationUpdatePage.studentSelectLastOption()
    ]);

    expect(await companyEvaluationUpdatePage.getSupervisionInput()).to.eq('5', 'Expected supervision value to be equals to 5');
    expect(await companyEvaluationUpdatePage.getAcceuilInput()).to.eq('5', 'Expected acceuil value to be equals to 5');

    await companyEvaluationUpdatePage.save();
    expect(await companyEvaluationUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await companyEvaluationComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last CompanyEvaluation', async () => {
    const nbButtonsBeforeDelete = await companyEvaluationComponentsPage.countDeleteButtons();
    await companyEvaluationComponentsPage.clickOnLastDeleteButton();

    companyEvaluationDeleteDialog = new CompanyEvaluationDeleteDialog();
    expect(await companyEvaluationDeleteDialog.getDialogTitle()).to.eq('enisIntershipApp.companyEvaluation.delete.question');
    await companyEvaluationDeleteDialog.clickOnConfirmButton();

    expect(await companyEvaluationComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
