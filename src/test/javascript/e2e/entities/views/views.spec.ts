import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ViewsComponentsPage, ViewsDeleteDialog, ViewsUpdatePage } from './views.page-object';

const expect = chai.expect;

describe('Views e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let viewsComponentsPage: ViewsComponentsPage;
  let viewsUpdatePage: ViewsUpdatePage;
  let viewsDeleteDialog: ViewsDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Views', async () => {
    await navBarPage.goToEntity('views');
    viewsComponentsPage = new ViewsComponentsPage();
    await browser.wait(ec.visibilityOf(viewsComponentsPage.title), 5000);
    expect(await viewsComponentsPage.getTitle()).to.eq('enisIntershipApp.views.home.title');
    await browser.wait(ec.or(ec.visibilityOf(viewsComponentsPage.entities), ec.visibilityOf(viewsComponentsPage.noResult)), 1000);
  });

  it('should load create Views page', async () => {
    await viewsComponentsPage.clickOnCreateButton();
    viewsUpdatePage = new ViewsUpdatePage();
    expect(await viewsUpdatePage.getPageTitle()).to.eq('enisIntershipApp.views.home.createOrEditLabel');
    await viewsUpdatePage.cancel();
  });

  it('should create and save Views', async () => {
    const nbButtonsBeforeCreate = await viewsComponentsPage.countDeleteButtons();

    await viewsComponentsPage.clickOnCreateButton();

    await promise.all([]);

    await viewsUpdatePage.save();
    expect(await viewsUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await viewsComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Views', async () => {
    const nbButtonsBeforeDelete = await viewsComponentsPage.countDeleteButtons();
    await viewsComponentsPage.clickOnLastDeleteButton();

    viewsDeleteDialog = new ViewsDeleteDialog();
    expect(await viewsDeleteDialog.getDialogTitle()).to.eq('enisIntershipApp.views.delete.question');
    await viewsDeleteDialog.clickOnConfirmButton();

    expect(await viewsComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
