import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  AcademicSupervisorComponentsPage,
  AcademicSupervisorDeleteDialog,
  AcademicSupervisorUpdatePage
} from './academic-supervisor.page-object';
import * as path from 'path';

const expect = chai.expect;

describe('AcademicSupervisor e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let academicSupervisorComponentsPage: AcademicSupervisorComponentsPage;
  let academicSupervisorUpdatePage: AcademicSupervisorUpdatePage;
  let academicSupervisorDeleteDialog: AcademicSupervisorDeleteDialog;
  const fileNameToUpload = 'logo-jhipster.png';
  const fileToUpload = '../../../../../../src/main/webapp/content/images/' + fileNameToUpload;
  const absolutePath = path.resolve(__dirname, fileToUpload);

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load AcademicSupervisors', async () => {
    await navBarPage.goToEntity('academic-supervisor');
    academicSupervisorComponentsPage = new AcademicSupervisorComponentsPage();
    await browser.wait(ec.visibilityOf(academicSupervisorComponentsPage.title), 5000);
    expect(await academicSupervisorComponentsPage.getTitle()).to.eq('enisIntershipApp.academicSupervisor.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(academicSupervisorComponentsPage.entities), ec.visibilityOf(academicSupervisorComponentsPage.noResult)),
      1000
    );
  });

  it('should load create AcademicSupervisor page', async () => {
    await academicSupervisorComponentsPage.clickOnCreateButton();
    academicSupervisorUpdatePage = new AcademicSupervisorUpdatePage();
    expect(await academicSupervisorUpdatePage.getPageTitle()).to.eq('enisIntershipApp.academicSupervisor.home.createOrEditLabel');
    await academicSupervisorUpdatePage.cancel();
  });

  it('should create and save AcademicSupervisors', async () => {
    const nbButtonsBeforeCreate = await academicSupervisorComponentsPage.countDeleteButtons();

    await academicSupervisorComponentsPage.clickOnCreateButton();

    await promise.all([
      academicSupervisorUpdatePage.setPhotoInput(absolutePath),
      academicSupervisorUpdatePage.setCityInput('city'),
      academicSupervisorUpdatePage.setCountryInput('country'),
      academicSupervisorUpdatePage.setPostalCodeInput('5'),
      academicSupervisorUpdatePage.setPhoneNumberInput('5'),
      academicSupervisorUpdatePage.setGradeInput('grade'),
      academicSupervisorUpdatePage.userSelectLastOption(),
      academicSupervisorUpdatePage.departmentSelectLastOption()
    ]);

    expect(await academicSupervisorUpdatePage.getPhotoInput()).to.endsWith(
      fileNameToUpload,
      'Expected Photo value to be end with ' + fileNameToUpload
    );
    expect(await academicSupervisorUpdatePage.getCityInput()).to.eq('city', 'Expected City value to be equals to city');
    expect(await academicSupervisorUpdatePage.getCountryInput()).to.eq('country', 'Expected Country value to be equals to country');
    expect(await academicSupervisorUpdatePage.getPostalCodeInput()).to.eq('5', 'Expected postalCode value to be equals to 5');
    expect(await academicSupervisorUpdatePage.getPhoneNumberInput()).to.eq('5', 'Expected phoneNumber value to be equals to 5');
    expect(await academicSupervisorUpdatePage.getGradeInput()).to.eq('grade', 'Expected Grade value to be equals to grade');

    await academicSupervisorUpdatePage.save();
    expect(await academicSupervisorUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await academicSupervisorComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last AcademicSupervisor', async () => {
    const nbButtonsBeforeDelete = await academicSupervisorComponentsPage.countDeleteButtons();
    await academicSupervisorComponentsPage.clickOnLastDeleteButton();

    academicSupervisorDeleteDialog = new AcademicSupervisorDeleteDialog();
    expect(await academicSupervisorDeleteDialog.getDialogTitle()).to.eq('enisIntershipApp.academicSupervisor.delete.question');
    await academicSupervisorDeleteDialog.clickOnConfirmButton();

    expect(await academicSupervisorComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
